<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mprovince extends CI_Model {

	public function insertData() {
		$object = array(
				'province_name' => $this->input->post('province_name')
			);
		$query = $this->db->insert('province', $object);
		return $query;
	}

	public function getData(){
		$query = $this->db->get('province');
		return $query;
	}

	public function getId($id){
		$query = $this->db->get_where('province', array('id' => $id))->row();
		return $query;
	} 

	public function updateData($id){
		$this->db->set('province_name', $this->input->post('province_name'));
		$this->db->where('id', $id);
		return $this->db->update('province');
	}

	public function deleteData($id){
		$query = $this->db->delete('province', array('id' => $id));
		return $query;
	}
}
