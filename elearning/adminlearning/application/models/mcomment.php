<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mcomment extends CI_Model {

	public function getData(){ 
		return $this->db->get('comment');
	}
	
	public function updateData($id, $status){		
		$this->db->set('status', $status);
		$this->db->where('id', $id);
		return $this->db->update('comment');
	}
	
}
