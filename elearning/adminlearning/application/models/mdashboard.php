<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mdashboard extends CI_Model {

	public function countStudent(){
		return $this->db->query("SELECT COUNT(id) AS total FROM student");
	}

	public function countTeacher(){
		return $this->db->query("SELECT COUNT(id) AS total FROM teacher");;
	}

	public function countCategory(){
		return $this->db->query("SELECT COUNT(id) AS total FROM category");
	}

	public function countClasses(){
		return $this->db->query("SELECT COUNT(id) AS total FROM classes");
	}
	
}
