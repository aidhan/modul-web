<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mevaluation_teacher extends CI_Model {

	public function getData(){ 
		return $this->db->query("SELECT AVG(rate_module) AS module, AVG(rate_quiz) AS quiz, id, id_student, id_classes, rate_module, rate_quiz, timestamp FROM evaluation_teacher GROUP BY id_classes");
	}

	public function deleteAll(){
		return $this->db->query('DELETE FROM evaluation_teacher');
	}

	public function count(){
		return $this->db->query("SELECT COUNT(*) AS total FROM evaluation_teacher")->row();
	}
	
} 
