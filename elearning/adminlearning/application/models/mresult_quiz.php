<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mresult_quiz extends CI_Model {

	public function getData(){ 
		$query = $this->db->get('result_quiz');
		return $query;
	}

	public function getScore($id_classes, $id_student){
		$query = $this->db->query("SELECT SUM(score) AS final_score FROM result_quiz WHERE id_classes='$id_classes' AND id_student='$id_student'")->row();
		return $query;
	} 

	public function getIdQuiz($id_classes, $id_student){
		$query = $this->db->query("SELECT * FROM result_quiz WHERE id_classes='$id_classes' AND id_student='$id_student'")->result();
		return $query;
	} 

	public function deleteAll(){
		return $this->db->query('DELETE FROM result_quiz');
	}

	public function count(){
		return $this->db->query("SELECT COUNT(*) AS total FROM result_quiz")->row();
	}
	
}
