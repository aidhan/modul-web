<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mmessage extends CI_Model {

	public function getData(){
		return $this->db->get('message');
	}
	
	//MENAMPILKAN PERCAKAPAN

	public function chat($user_1, $user_2){
		return $this->db->query("SELECT * FROM `message` WHERE id_user_1 = '$user_1' AND id_user_2 = '$user_2' OR id_user_1 = '$user_2' AND id_user_2 = '$user_1'");
	}

	//KIRIM PESAN ATAU BALAS PESAN

	public function insertData($user_1, $user_2){
		$object = array(
			'id_user_1' => $user_1,
			'id_user_2' => $user_2,
			'message' => $this->input->post('message'),
			'status' => 0
		);

		return $this->db->insert('message', $object);
	}

	//MENAMPILKAN USER YANG PERNAH CHATTING (NAMA USER SAJA PADA SIDEBAR)

	public function showMessage($user_1){
		return $this->db->query("SELECT * FROM message WHERE id_user_1 = '$user_1' GROUP BY id_user_2");
	}

	//MENAMPILKAN BERAPA BANYAK PESAN YANG BELUM DIBACA PER USER (misalnya ERIS DSR (8 pesan))

	public function numberOfMessage($user_1, $user_2){
		return $this->db->query("SELECT COUNT(id_user_1) AS total FROM `message` WHERE id_user_1 = '$user_1' AND id_user_2 = '$user_2' AND status = 0")->row();
	}

	//UPDATE STATUS PESAN MENJADI TELAH TERBACA

	public function readMessageUpdate($user_1, $user_2){
		return $this->db->query("UPDATE message SET status = 1 WHERE id_user_1 = '$user_1' AND id_user_2 = '$user_2' AND status = 0");
	}

}
