<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mpost extends CI_Model {

	public function getData(){
		return $this->db->get('post');
	}
	
	public function updateData($id, $status){		
		$this->db->set('status', $status);
		$this->db->where('id', $id);
		return $this->db->update('post');
	}
}
