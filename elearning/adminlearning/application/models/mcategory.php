<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mcategory extends CI_Model {

	public function insertData() {
		$object = array(
				'category_name' => $this->input->post('category_name')
			);
		$query = $this->db->insert('category', $object);
		return $query;
	}
 
	public function getData(){
		$query = $this->db->get('category');
		return $query;
	}

	public function getId($id){
		$query = $this->db->get_where('category', array('id' => $id))->row();
		return $query;
	} 

	public function updateData($id){
		$this->db->set('category_name', $this->input->post('category_name'));
		$this->db->where('id', $id);
		return $this->db->update('category');
	}

	public function deleteData($id){
		$query = $this->db->delete('category', array('id' => $id));
		return $query;
	}
}
