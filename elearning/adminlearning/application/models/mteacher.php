<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mteacher extends CI_Model {

	public function insertData($img) {
		$object = array(
				'nidn' => $this->input->post('nidn'),
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'gender' => $this->input->post('gender'),
				'address' => $this->input->post('address'),
				'phone' => $this->input->post('phone'),
				'username' => $this->input->post('username'),
				'image' => $img
			);
		$query = $this->db->insert('teacher', $object);
		return $query;
	}

	public function getData(){
		$query = $this->db->get('teacher');
		return $query;
	} 

	public function getId($username){
		$query = $this->db->get_where('teacher', array('username' => $username))->row();
		return $query;
	} 

	public function getIdTeacher($id){
		$query = $this->db->get_where('teacher', array('id' => $id))->row();
		return $query;
	} 

	public function updateData($username){
		$object = array(
			'nidn' => $this->input->post('nidn'),
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'gender' => $this->input->post('gender'),
			'address' => $this->input->post('address'),
			'phone' => $this->input->post('phone')
		);

		$this->db->where('username', $username);
		return $this->db->update('teacher', $object);
	}

	public function updateProfilePhoto($username, $img){
		$object = array('image' => $img);
		$this->db->where('username', $username);
		return $this->db->update('teacher', $object);
	}

	public function deleteData($username){
		$query = $this->db->delete('teacher', array('username' => $username));
		return $query;
	}
}
