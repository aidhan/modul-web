<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mstudent extends CI_Model {

	public function insertData($img) {
		$object = array(
				'nim' => $this->input->post('nim'),
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'gender' => $this->input->post('gender'),
				'address' => $this->input->post('address'),
				'phone' => $this->input->post('phone'),
				'username' => $this->input->post('username'),
				'image' => $img
			);
		$query = $this->db->insert('student', $object);
		return $query;
	}

	public function getData(){
		$query = $this->db->get('student');
		return $query;
	} 

	public function getId($username){
		$query = $this->db->get_where('student', array('username' => $username))->row();
		return $query;
	} 

	public function getIdStudent($id){
		$query = $this->db->get_where('student', array('id' => $id))->row();
		return $query;
	} 

	public function getIdClasses($id_classes){
		$query = $this->db->query("
			SELECT 
				sc.id, 
				sc.id_student,
				s.nim,
				CONCAT(s.first_name, ' ', s.last_name) AS student_name,
				s.gender,
				s.address,
				s.phone,
				sc.id_classes,
				c.class_name,
				sc.status,
				sc.join_time,
				sc.leave_time
			FROM
				student_classes sc,
				student s,
				classes c
			WHERE 
				s.id = sc.id_student
			AND
				c.id = sc.id_classes
			AND 
				sc.id_classes = '$id_classes'			
			");
		return $query;
	} 

	public function updateData($username){
		$object = array(
			'nim' => $this->input->post('nim'),
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'gender' => $this->input->post('gender'),
			'address' => $this->input->post('address'),
			'phone' => $this->input->post('phone')
		);

		$this->db->where('username', $username);
		return $this->db->update('student', $object);
	}

	public function updateProfilePhoto($username, $img){
		$object = array('image' => $img);
		$this->db->where('username', $username);
		return $this->db->update('student', $object);
	}

	public function deleteData($username){
		$query = $this->db->delete('student', array('username' => $username));
		return $query;
	}
}
