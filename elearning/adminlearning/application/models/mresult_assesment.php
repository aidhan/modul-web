<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mresult_assesment extends CI_Model {

	public function getData(){ 
		$query = $this->db->get('result_assesment');
		return $query;
	}

	public function getScore($id){
		$query = $this->db->query("SELECT SUM(score) AS final_score FROM result_assesment WHERE id_assesment='$id'")->row();
		return $query;
	} 

	public function delete($id){
		return $this->db->query("DELETE FROM result_assesment WHERE id = '$id'");
	}

	public function count(){
		return $this->db->query("SELECT COUNT(*) AS total FROM result_assesment")->row();
	}
	
}
 