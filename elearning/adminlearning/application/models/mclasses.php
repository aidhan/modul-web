<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mclasses extends CI_Model {

	public function insertData($img) {
		$object = array(
				'class_code' => $this->input->post('class_code'),
				'class_name' => $this->input->post('class_name'),
				'id_teacher' => $this->input->post('teacher'),
				'id_province' => $this->input->post('province'),
				'id_category' => $this->input->post('category'),
				'description' => $this->input->post('description'),
				'image' => $img,
				'status' => $this->input->post('status')
			);
		$query = $this->db->insert('classes', $object);
		return $query;
	}

	public function getData(){
		$query = $this->db->get('classes');
		return $query;
	}

	public function getDataClass(){
		$query = $this->db->get('classes');
		return $query;
	}

	public function maxID(){		
		$this->db->select_max('class_code', 'max_class');	
		$result = $this->db->get('classes')->row();
		return $result;
	}

	public function generateCode(){
		$maxID = $this->maxID()->max_class;
		$split = explode("-", $maxID);
		$prefix = $split['0'];
		$code = $split['1'];			
		$increment_code = $code+1;
		$length = strlen($increment_code);		

		if(empty($prefix)){
			$prefix = 'CL';
		}

		if($length == 1){
			$generate_code = $prefix."-000".$increment_code;
		} else if($length == 2){
			$generate_code = $prefix."-00".$increment_code;
		} else if($length == 3){
			$generate_code = $prefix."-0".$increment_code;
		} else if($length == 4){
			$generate_code = $prefix."-".$increment_code;
		}

		return $generate_code;
	}

	public function getId($id){
		$query = $this->db->get_where('classes', array('id' => $id))->row();
		return $query;
	} 

	public function updateData($id){
		$object = array(
				'class_name' => $this->input->post('class_name'),
				'id_teacher' => $this->input->post('teacher'),
				'id_province' => $this->input->post('province'),
				'id_category' => $this->input->post('category'),
				'description' => $this->input->post('description'),
				'status' => $this->input->post('status')
			);
		$this->db->where('id', $id);
		return $this->db->update('classes', $object);
	}

	public function updateImage($id, $img){
		$object = array('image' => $img);
		$this->db->where('id', $id);
		return $this->db->update('classes', $object);
	}

	public function deleteData($id){
		$query = $this->db->delete('classes', array('id' => $id));
		return $query;
	}
}
