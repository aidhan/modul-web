<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mquiz extends CI_Model {

	public function insertData() {
		$object = array(
				'question' => $this->input->post('question'),
				'id_classes' => $this->input->post('id_classes')
			);
		$query = $this->db->insert('quiz', $object);
		return $query;
	}

	public function getData(){
		$query = $this->db->get('quiz');
		return $query;
	}

	public function getId($id){
		$query = $this->db->get_where('quiz', array('id' => $id))->row();
		return $query;
	} 

	public function updateData($id){
		$object = array(
				'question' => $this->input->post('question'),
				'id_classes' => $this->input->post('id_classes')
			);
		$this->db->where('id', $id);
		return $this->db->update('quiz', $object);
	}

	public function deleteData($id){
		$query = $this->db->delete('quiz', array('id' => $id));
		return $query;
	}
}
