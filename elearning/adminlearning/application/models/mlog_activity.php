<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mlog_activity extends CI_Model {

	public function getData(){
		return $this->db->get('log_activity');
	}

	public function getDataLimit(){
		return $this->db->query("SELECT * FROM log_activity ORDER BY timestamp DESC LIMIT 0,5");
	}

	public function deleteAll(){
		return $this->db->query('DELETE FROM log_activity');
	}

	public function insertData($action){
		$object = array(
				'username' => $this->session->userdata('username'),
				'action' => $action
			);
		$query = $this->db->insert('log_activity', $object);
		return $query;
	}

	public function count(){
		return $this->db->query("SELECT COUNT(*) AS total FROM log_activity")->row();
	}

}
