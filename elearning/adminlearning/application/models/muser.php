<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class muser extends CI_Model {

	public function randomChar($length, $count, $characters) {  
	    $symbols = array();
	    $code_verify = array();
	    $used_symbols = '';
	    $code = '';
	 
	    $symbols["lower_case"] = 'abcdefghijklmnopqrstuvwxyz';
	    $symbols["upper_case"] = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $symbols["numbers"] = '1234567890';
	    $symbols["special_symbols"] = '!?~@#-_+<>[]{}';
	 
	    $characters = explode(",", $characters); 

	    foreach ($characters as $key=>$value) {
	        $used_symbols .= $symbols[$value];
	    }

	    $symbols_length = strlen($used_symbols) - 1; 
	     
	    for ($p = 0; $p < $count; $p++) {
	        $code = '';
	        for ($i = 0; $i < $length; $i++) {
	            $n = rand(0, $symbols_length); 
	            $code .= $used_symbols[$n];
	        }
	        $code_verify[] = $code;
	    }
	     
	    return $code_verify; 
	}

	public function insertData() {
		$code_verify = $this->randomChar(30,1,"number,upper_case,lower_case");
		$object = array(
				'username' => $this->input->post('username'),
				'password' => md5($this->input->post('password')),
				'email' => $this->input->post('email'),
				'code_verify' => $code_verify[0],
				'verify' => $this->input->post('verify'),
				'level' => $this->input->post('level'),
				'status' => 0
			);
		$query = $this->db->insert('user', $object);
		return $query;
	}

	public function getData(){
		$query = $this->db->get('user');
		return $query;
	}

	public function getId($username){
		$query = $this->db->get_where('user', array('username' => $username))->row();
		return $query;
	}

	public function getUserOnline(){
		$query = $this->db->get_where('user', array('status' => 1))->result();
		return $query;
	}

	public function getDataAdmin(){
		$sql = "SELECT * FROM `user` WHERE level<=1";		
		return $this->db->query($sql);
	}

	public function getById($id){
		$query = $this->db->get_where('user', array('id' => $id))->row();
		return $query;
	}

	public function updateUser($username){
		$object = array(
			'username' => $this->input->post('username'),
			'password' => md5($this->input->post('password')),
			'email' => $this->input->post('email'),
			'verify' => $this->input->post('verify')
		);

		$this->db->where('username', $username);
		return $this->db->update('user', $object);
	}

	public function updateData($username){
		$object = array(
			'email' => $this->input->post('email'),
			'verify' => $this->input->post('verify')
		);

		$this->db->where('username', $username);
		return $this->db->update('user', $object);
	}

	public function setOnline($id, $status){
		$object = array(
			'status' => $status
		);

		$this->db->where('id', $id);
		return $this->db->update('user', $object);
	}

	public function updatePassword($username){
		$object = array(
			'password' => md5($this->input->post('password'))
		);

		$this->db->where('username', $username);
		return $this->db->update('user', $object);
	}

	public function deleteData($username){
		$query = $this->db->delete('user', array('username' => $username));
		return $query;
	}

	public function login($username, $password){
		$sql = "SELECT * FROM `user` WHERE username = ? AND password = ?";		
		return $this->db->query($sql, array($username, $password))->row();
	}
}
