<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class massesment extends CI_Model {

	public function insertData($img) {
		$object = array(
				'question' => $this->input->post('question'),
				'image' => $img,
				'id_classes' => $this->input->post('id_classes')
			);
		$query = $this->db->insert('assesment', $object);
		return $query;
	}

	public function getData(){
		$query = $this->db->get('assesment');
		return $query;
	} 

	public function getId($id){
		$query = $this->db->get_where('assesment', array('id' => $id))->row();
		return $query;
	} 

	public function updateData($id){
		$object = array(
				'question' => $this->input->post('question'),
				'id_classes' => $this->input->post('id_classes')
		);

		$this->db->where('id', $id);
		return $this->db->update('assesment', $object);
	}

	public function updateImage($id, $img){
		$object = array('image' => $img);
		$this->db->where('id', $id);
		return $this->db->update('assesment', $object);
	}

	public function deleteData($id){
		$query = $this->db->delete('assesment', array('id' => $id));
		return $query;
	}
}
