<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mmodule extends CI_Model {

	public function insertData($docs, $ext) {
		$object = array(
				'module_name' => $this->input->post('module_name'),
				'file_name' => $docs,
				'file_type' => $ext,
				'id_classes' => $this->input->post('id_classes')
			);
		$query = $this->db->insert('module', $object);
		return $query;
	}

	public function getData(){
		$query = $this->db->get('module');
		return $query;
	} 
 
	public function getId($id){
		$query = $this->db->get_where('module', array('id' => $id))->row();
		return $query;
	} 

	public function updateData($id){
		$object = array(
				'module_name' => $this->input->post('module_name'),
				'id_classes' => $this->input->post('id_classes')
		);

		$this->db->where('id', $id);
		return $this->db->update('module', $object);
	}

	public function updateDocs($id, $docs, $ext){
		$object = array(
				'file_name' => $docs,
				'file_type' => $ext
			);
		$this->db->where('id', $id);
		return $this->db->update('module', $object);
	}

	public function deleteData($id){
		$query = $this->db->delete('module', array('id' => $id));
		return $query;
	}
}
