<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class mevaluation_student extends CI_Model {

	public function getData(){ 
		$query = $this->db->get('evaluation_student');
		return $query;
	}

	public function deleteAll(){
		return $this->db->query('DELETE FROM evaluation_student');
	}

	public function count(){
		return $this->db->query("SELECT COUNT(*) AS total FROM evaluation_student")->row();
	}
	
}
