<!-- begin .app-main -->
<div class="app-main">
<!-- begin .main-heading -->
<header class="main-heading shadow-2dp">
  <!-- begin dashhead -->
  <div class="dashhead bg-white">
    <div class="dashhead-titles">
      <h1 class="dashhead-title"><?= $title_page ?></h1>
    </div>

    <div class="dashhead-toolbar">
      <div class="dashhead-toolbar-item">
        <a href="<?= base_url() ?>">Dashboard</a>
        / <?= $title_page ?>
      </div>
    </div>
  </div>
  <!-- END: dashhead -->
</header>
<!-- END: .main-heading -->

<!-- begin .main-content -->
<div class="main-content bg-clouds">

  <!-- begin .container-fluid -->
  <div class="container-fluid p-t-15">

    <?php if($this->session->flashdata('info')){ ?>

    <div class="alert alert-success"><?= $this->session->flashdata('info') ?></div>

    <?php } else if($this->session->flashdata('error')){ ?>

    <div class="alert alert-danger"><?= $this->session->flashdata('error') ?></div>

    <?php } ?>

    <div class="box">
      <header>
        <h4><i class="fa fa-lock"></i> Change Password</h4>
        <div class="box-tools">
          <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
          <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
          <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
        </div>
      </header>
      <div class="box-body">

        <?= form_open('change_password/update') ?>

        <div class="form-horizontal">

          <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" name="password" placeholder="New password">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Retype Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" name="retype_password" placeholder="Retype a new password">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
              <input type="submit" class="btn btn-rect btn-success" name="save" value="Save Change">
            </div>
          </div>

        </div>

        <?= form_close() ?>

      </div>
    </div>

  </div>
  <!-- END: .container-fluid -->
</div>
<!-- END: .main-content -->

