<!-- begin .app-container -->
<div class="app-container">
  <!-- begin .app-side -->
  <aside class="app-side">
    <!-- begin .side-content -->
    <div class="side-content">
      <!-- begin user-panel -->
      <div class="user-panel">
        <div class="user-image">
          <a href="#">
            <img class="img-circle" src="<?= base_url('assets/images/user.png') ?>" alt="John Doe">
          </a>
        </div>
        <div class="user-info">
          <h5><?= ucwords($this->session->userdata('username')); ?></h5>
              <p class="text-turquoise small dropdown-toggle bg-transparent">
                <i class="fa fa-fw fa-circle"></i> Online
              </p>
        </div>
      </div>
      <!-- END: user-panel -->
      
      <!-- begin .side-nav -->
      <nav class="side-nav">
        <!-- BEGIN: nav-content -->
        <ul class="metismenu nav nav-inverse nav-bordered nav-stacked" data-plugin="metismenu">

          <li class="nav-header">MAIN MENU</li>
          <li>
            <a class="<?= $title_page == 'Dashboard'? 'active' : '' ?>" href="<?= base_url() ?>">
              <span class="nav-icon"><i class="fa fa-fw fa-dashboard"></i></span>
              <span class="nav-title">Dashboard</span>
            </a>
          </li>

          <!-- BEGIN: Data Class -->
          <li class="<?= $title_page == 'Province' || $title_page == 'Category' || $title_page == 'Classes' || $title_page == 'Module' || $title_page == 'Assesment' || $title_page == 'Quiz' ? 'active' : '' ?>">
            <a href="javascript:;" aria-expanded="<?= $title_page == 'Province' || $title_page == 'Category' || $title_page == 'Classes' || $title_page == 'Module' || $title_page == 'Assesment' || $title_page == 'Quiz' ? 'true' : 'false' ?>">
              <span class="nav-icon">
                <i class="fa fa-fw fa-graduation-cap"></i>
              </span>
              <span class="nav-title">Data Class</span>
              <span class="nav-tools"><i class="fa fa-fw arrow"></i></span>
            </a>

            <ul class="nav nav-sub nav-stacked">
              <li><a href="<?= base_url('province') ?>" class="<?= $title_page == 'Province'? 'active' : '' ?>">Province</a></li>
              <li><a href="<?= base_url('category') ?>" class="<?= $title_page == 'Category'? 'active' : '' ?>">Category</a></li>
              <li><a href="<?= base_url('classes') ?>" class="<?= $title_page == 'Classes'? 'active' : '' ?>">Classes</a></li>
              <li><a href="<?= base_url('module') ?>" class="<?= $title_page == 'Module'? 'active' : '' ?>">Module</a></li>
              <li><a href="<?= base_url('assesment') ?>" class="<?= $title_page == 'Assesment'? 'active' : '' ?>">Assesment</a></li>
              <li><a href="<?= base_url('quiz') ?>" class="<?= $title_page == 'Quiz'? 'active' : '' ?>">Quiz</a></li>
            </ul>
          </li>
          <!-- END: Data Class -->

          <!-- BEGIN: Post -->
          <li class="<?= $title_page == 'Post' || $title_page == 'Comment' ? 'active' : '' ?>">
            <a href="javascript:;" aria-expanded="<?= $title_page == 'Post' || $title_page == 'Comment' ? 'true' : 'false' ?>">
              <span class="nav-icon">
                <i class="fa fa-fw fa-list-alt"></i>
              </span>
              <span class="nav-title">Data Post</span>
              <span class="nav-tools"><i class="fa fa-fw arrow"></i></span>
            </a>

            <ul class="nav nav-sub nav-stacked">
              <li><a href="<?= base_url('post') ?>" class="<?= $title_page == 'Post'? 'active' : '' ?>">Post</a></li>
              <li><a href="<?= base_url('comment') ?>" class="<?= $title_page == 'Comment'? 'active' : '' ?>">Comment</a></li>
            </ul>
          </li>
          <!-- END: Post -->

          <!-- BEGIN: Report -->
          <li class="<?= $title_page == 'Evaluation Student' || $title_page == 'Evaluation Teacher' || $title_page == 'Result Assesment' || $title_page == 'Result Quiz' ? 'active' : '' ?>">
            <a href="javascript:;" aria-expanded="<?= $title_page == 'Evaluation Student' || $title_page == 'Evaluation Student' || $title_page == 'Result Assesment' || $title_page == 'Result Quiz' ? 'true' : 'false' ?>">
              <span class="nav-icon">
                <i class="fa fa-fw fa-th-large"></i>
              </span>
              <span class="nav-title">Report</span>
              <span class="nav-tools"><i class="fa fa-fw arrow"></i></span>
            </a>

            <ul class="nav nav-sub nav-stacked">
              <li><a href="<?= base_url('evaluation_student') ?>" class="<?= $title_page == 'Evaluation Student'? 'active' : '' ?>">Evaluation Student</a></li>
              <li><a href="<?= base_url('evaluation_teacher') ?>" class="<?= $title_page == 'Evaluation Teacher'? 'active' : '' ?>">Evaluation Teacher</a></li>
              <li><a href="<?= base_url('result_assesment') ?>" class="<?= $title_page == 'Result Assesment'? 'active' : '' ?>">Result Assesment</a></li>
              <li><a href="<?= base_url('result_quiz') ?>" class="<?= $title_page == 'Result Quiz'? 'active' : '' ?>">Result Quiz</a></li>
            </ul>
          </li>
          <!-- END: Report -->

          <!-- BEGIN: Management User -->
          <li class="<?= $title_page == 'Teacher' || $title_page == 'Student' || $title_page == 'Administrator' ? 'active' : '' ?>">
            <a href="javascript:;" aria-expanded="<?= $title_page == 'Teacher' || $title_page == 'Student' || $title_page == 'Administrator'  ? 'true' : 'false' ?>">
              <span class="nav-icon">
                <i class="fa fa-fw fa-user-plus"></i>
              </span>
              <span class="nav-title">Management User</span>
              <span class="nav-tools"><i class="fa fa-fw arrow"></i></span>
            </a>

            <ul class="nav nav-sub nav-stacked">
              <li><a href="<?= base_url('teacher') ?>" class="<?= $title_page == 'Teacher'? 'active' : '' ?>">Teacher</a></li>
              <li><a href="<?= base_url('student') ?>" class="<?= $title_page == 'Student'? 'active' : '' ?>">Student</a></li>

              <?php if($this->session->userdata('level') == 0) { ?>
              
              <li><a href="<?= base_url('user') ?>" class="<?= $title_page == 'Administrator'? 'active' : '' ?>">Administrator</a></li>

              <?php } ?>
            </ul>
          </li>
          <!-- END: Management User -->

          <!-- BEGIN: Message -->
          <li>
            <a href="<?= base_url('message') ?>" class="<?= $title_page == 'Message'? 'active' : '' ?>">
              <span class="nav-icon">
                <i class="fa fa-fw fa-envelope"></i>
              </span>
              <span class="nav-title">Message</span>
            </a>
          </li>
          <!-- END: Message -->

          <!-- BEGIN: Log Activity -->
          
          <?php if($this->session->userdata('level') == 0) { ?>

          <li>
            <a href="<?= base_url('log_activity') ?>" class="<?= $title_page == 'Log Activity'? 'active' : '' ?>">
              <span class="nav-icon">
                <i class="fa fa-fw fa-retweet"></i>
              </span>
              <span class="nav-title">Log Activity</span>
            </a>
          </li>

          <?php } ?>
          
          <!-- END: Log Activity -->

        </ul>
        <!-- END: nav-content -->
      </nav>
      <!-- END: .side-nav -->
    </div>
    <!-- END: .side-content -->
  </aside>
  <!-- END: .app-side -->

  <!-- begin side-collapse-visible bar -->
  <div class="side-visible-line hidden-xs" data-side="collapse">
    <i class="fa fa-caret-left"></i>
  </div>
  <!-- begin side-collapse-visible bar -->