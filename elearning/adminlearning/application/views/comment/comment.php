<!-- begin .app-main -->
<div class="app-main">
<!-- begin .main-heading -->
<header class="main-heading shadow-2dp">
  <!-- begin dashhead -->
  <div class="dashhead bg-white">
    <div class="dashhead-titles">
      <h1 class="dashhead-title"><?= $title_page ?></h1>
    </div>

    <div class="dashhead-toolbar">
      <div class="dashhead-toolbar-item">
        <a href="<?= base_url() ?>">Dashboard</a>
        / <?= $title_page ?>
      </div>
    </div>
  </div>
  <!-- END: dashhead -->
</header>
<!-- END: .main-heading -->

<!-- begin .main-content -->
<div class="main-content bg-clouds">

  <!-- begin .container-fluid -->
  <div class="container-fluid p-t-15">

    <?php if($this->session->flashdata('info')){ ?>

    <div class="alert alert-success"><?= $this->session->flashdata('info') ?></div>

    <?php } ?>

    <div class="row">
      <div class="col-sm-12">
        <div class="box">
          <header>
            <h4><i class="fa fa-table"></i> Data <?= $title_page ?></h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>

          <div class="box-body">
            
            <div class="table-responsive">
              <table data-plugin="datatables" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Comment</th>
                    <th>Username</th>
                    <th>Status</th>
                    <th>Timestamp</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $no = 0; foreach($comment as $row){ $no++; ?>

                  <tr>
                    <td><?= $no ?></td>
                    <td><?php if(strlen($row->comment) > 50) { echo mb_substr($row->comment, 0, 50)."...."; } else { echo $row->comment; } ?></td>
                    <td><?= $row->username ?></td>
                    <td><?= $row->status == 1? 'Published' : 'Unpublished' ?></td>
                    <td><?= $row->timestamp ?></td>
                    <td>
                      <?= $row->status == 1? "<a href='comment/unpublish/". $row->id ."' class='btn btn-sm btn-rect btn-success' title='Unpublish'><i class='fa fa-globe'></i></a>" : "<a href='comment/publish/". $row->id ."' class='btn btn-sm btn-rect btn-danger' title='Publish'><i class='fa fa-minus-circle'></i></a>" ?>
                       
                        <button data-toggle="modal" data-target="#exampleModal<?= $no ?>" class="btn btn-sm btn-rect btn-info" title="Show Details"><i class="fa fa-eye"></i></button>
                    </td>
                  </tr>

                  <!-- Modal -->
                  <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal<?= $no ?>">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h3>Detail Comment ID <?= $row->id ?></h3>
                        </div>
                        <div class="modal-body">
                          <small class="text-muted" style="text-align: right">
                            <i class="fa fa-user"></i> <?= $row->username ?> 
                            <i class="fa fa-calendar"></i> <?= $row->timestamp ?> 
                          </small>
                          <p style="padding: 10px 0"><?= $row->comment ?></p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- END: .container-fluid -->
</div>
<!-- END: .main-content -->

