<!-- begin .app-main -->
<div class="app-main">
  <!-- begin .main-heading -->
  <header class="main-heading shadow-2dp">
    <!-- begin dashhead -->
    <div class="dashhead bg-white">
      <div class="dashhead-titles">
        <h1 class="dashhead-title"><?= $title_page ?></h1>
      </div>

      <div class="dashhead-toolbar">
        <div class="dashhead-toolbar-item">
          <a href="<?= base_url() ?>">Dashboard</a>
          / <?= $title_page ?>
        </div>
      </div>
    </div>
    <!-- END: dashhead -->
  </header>
  <!-- END: .main-heading -->

  <!-- begin .main-content -->
  <div class="main-content bg-clouds">

    <!-- begin .container-fluid -->
    <div class="container-fluid p-t-15">

      <?php if($this->session->flashdata('info')){ ?>

      <div class="alert alert-success alert-dismissible show" role="alert">
          <?= $this->session->flashdata('info'); ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>

      <?php } ?>

      <div class="row">
        <div class="col-sm-12">
          <div class="box">
            <header>
              <h4><i class="fa fa-table"></i> Data <?= $title_page ?></h4>
              <div class="box-tools">
                <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
                <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
              </div>
            </header>
            <div class="box-body">

              <div class="form-group">
                <a href="<?= base_url('module/add') ?>" class="btn btn-rect btn-success"><i class="fa fa-plus"></i> Add Module</a>
              </div>

              <div class="table-responsive">
                <table data-plugin="datatables" class="table table-striped table-bordered">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Module Name</th>
                      <th>File Name</th>
                      <th>File Type</th>
                      <th>Classes</th>
                      <th>Timestamp</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>

                    <?php 
                    $no = 0; 
                    foreach($module as $row){ 
                      $no++; 
                      $classes = $this->db->get_where('classes', array('id' => $row->id_classes))->row(); 
                    ?>
                    
                    <tr>
                      <td><?= $no; ?></td>
                      <td><?= $row->module_name; ?></td>
                      <td><?= $row->file_name; ?></td>
                      <td><?= $row->file_type; ?></td>
                      <td><?= $classes->class_name ?></td>
                      <td><?= $row->timestamp; ?></td>
                      <td>
                        <a href="<?= base_url("module/delete/$row->id") ?>" class="btn btn-rect btn-sm btn-danger" title="Delete" onclick="return confirm('Are you sure to delete data?')" ><i class="fa fa-trash"></i></a>
                        <a href="<?= base_url("module/edit/$row->id") ?>" class="btn btn-rect btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                        <a href="<?= base_url("module/details/$row->id") ?>" class="btn btn-rect btn-sm btn-info" title="View Module"><i class="fa fa-eye"></i></a>
                        <a href="<?= base_url("assets/docs/$row->file_name") ?>" class="btn btn-rect btn-sm btn-success" title="Download"><i class="fa fa-download"></i></a>

                      </td>
                    </tr>

                    <?php } ?>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- END: .container-fluid -->
  </div>
  <!-- END: .main-content -->

