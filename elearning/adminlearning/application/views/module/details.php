<!-- begin .app-main -->
<div class="app-main">
  <!-- begin .main-heading -->
  <header class="main-heading shadow-2dp">
    <!-- begin dashhead -->
    <div class="dashhead bg-white">
      <div class="dashhead-titles">
        <h1 class="dashhead-title"><?= $title_page ?></h1>
      </div>

      <div class="dashhead-toolbar">
        <div class="dashhead-toolbar-item">
          <a href="<?= base_url() ?>">Dashboard</a>
          / <a href="<?= base_url('module') ?>">Module</a>
          / <?= $title_page ?>
        </div>
      </div>
    </div>
    <!-- END: dashhead -->
  </header>
  <!-- END: .main-heading -->

  <!-- begin .main-content -->
  <div class="main-content bg-clouds">

    <!-- begin .container-fluid -->
    <div class="container-fluid p-t-15">

      <?php if($this->session->flashdata('info')){ ?>

      <div class="alert alert-success alert-dismissible show" role="alert">
          <?= $this->session->flashdata('info'); ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>

      <?php } ?>

      <div class="row" >
        <div class="col-sm-12">
          <div class="box">
            <header>
              <h4><i class="fa fa-file"></i> <?= $module->module_name ?></h4>
              <div class="box-tools">
                <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
                <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
              </div>
            </header>
            <div class="box-body">
              <iframe style="border: 1px solid silver; width: 100%; height: 670px" type="application/pdf" src="<?= base_url('assets/docs/'.$module->file_name) ?>"></iframe>  
            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- END: .container-fluid -->
  </div>
  <!-- END: .main-content -->

