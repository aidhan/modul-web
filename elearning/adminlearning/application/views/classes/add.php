<!-- begin .app-main -->
<div class="app-main">
  <!-- begin .main-heading -->
  <header class="main-heading shadow-2dp">
    <!-- begin dashhead -->
    <div class="dashhead bg-white">
      <div class="dashhead-titles">
        <h1 class="dashhead-title"><?= $title_page ?></h1>
      </div>

      <div class="dashhead-toolbar">
        <div class="dashhead-toolbar-item">
          <a href="<?= base_url() ?>">Dashboard</a>
          / <a href="<?= base_url('classes') ?>">Classes</a>
          / <?= $title_page ?>
        </div>
      </div>
    </div>
    <!-- END: dashhead -->
  </header>
  <!-- END: .main-heading -->

  <!-- begin .main-content -->
  <div class="main-content bg-clouds">

    <!-- begin .container-fluid -->
    <div class="container-fluid p-t-15">

      <div class="row">
        <div class="col-sm-12">
          <div class="box">
            <header>
              <h4><i class="fa fa-plus"></i> Add <?= $title_page ?></h4>
              <div class="box-tools">
                <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a> 
                <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
              </div>
            </header>

            <div class="box-body">

            <?= form_open_multipart('classes/insert') ?>

              <div class="form-horizontal">

                <div class="form-group">
                  <label class="col-sm-2 control-label">Class Code</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="class_code" value="<?= $generate_code ?>" readonly="">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Class Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="class_name" placeholder="Type a class name">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Teacher</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="teacher">
                      <option value="">- Select a Teacher -</option>

                      <?php foreach ($teacher as $row) { ?>

                      <option value="<?= $row->id ?>"><?= $row->first_name." ".$row->last_name ?></option>
                        
                      <?php } ?>

                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Province</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="province">
                      <option value="">- Select a Province -</option>

                      <?php foreach ($province as $row) { ?>

                      <option value="<?= $row->id ?>"><?= $row->province_name ?></option>
                        
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Category</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="category">
                      <option value="">- Select a Category -</option>

                      <?php foreach ($category as $row) { ?>

                      <option value="<?= $row->id ?>"><?= $row->category_name ?></option>
                        
                      <?php } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="description" placeholder="Tell about this class..."></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Image</label>
                  <div class="col-sm-10">
                    <input type="file" name="image" class="form-control">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10">
                    <div class="radio">
                      <label><input type="radio" name="status" value="1"> Active</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="status" value="0"> Inactive</label>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-10">
                    <input type="submit" name="save" class="btn btn-rect btn-success" value="Add Data">
                  </div>
                </div>

              </div>

            <?= form_close(); ?>


            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- END: .container-fluid -->
  </div>
  <!-- END: .main-content -->

