<!-- begin .app-main -->
<div class="app-main">
  <!-- begin .main-heading -->
  <header class="main-heading shadow-2dp">
    <!-- begin dashhead -->
    <div class="dashhead bg-white">
      <div class="dashhead-titles">
        <h1 class="dashhead-title"><?= $title_page ?></h1>
      </div>

      <div class="dashhead-toolbar">
        <div class="dashhead-toolbar-item">
          <a href="<?= base_url() ?>">Dashboard</a>
          / <a href="<?= base_url('classes') ?>">Classes</a>
          / <?= $title_page ?>
        </div>
      </div>
    </div>
    <!-- END: dashhead -->
  </header>
  <!-- END: .main-heading -->

  <!-- begin .main-content -->
  <div class="main-content bg-clouds">

    <!-- begin .container-fluid -->
    <div class="container-fluid p-t-15">

    <?php if($this->session->flashdata('info')){ ?>

    <div class="alert alert-success"><?= $this->session->flashdata('info'); ?></div>

    <?php } ?>

      <div class="row">
        <div class="col-sm-12">
          <div class="box">
            <header>
              <h4><i class="fa fa-edit"></i> <?= $title_page ?></h4>
              <div class="box-tools">
                <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
                <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
              </div>
            </header>

            <div class="box-body">

            <?= form_open('classes/update_data/'.$this->uri->segment(3)) ?>

              <div class="form-horizontal">

                <div class="form-group">
                  <label class="col-sm-2 control-label">Class Name</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="class_name" value="<?= $classes->class_name ?>">
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Teacher</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="teacher">
                      <option value="">- Select a Teacher -</option>

                      <?php foreach ($teacher as $row) { ?>

                      <?php if($classes->id_teacher == $row->id){ ?>

                      <option value="<?= $row->id ?>" selected=""><?= $row->first_name." ".$row->last_name ?></option>

                      <?php } else { ?>             

                      <option value="<?= $row->id ?>"><?= $row->first_name." ".$row->last_name ?></option>     
                        
                      <?php } } ?>

                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Province</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="province">
                      <option value="">- Select a Province -</option>

                      <?php foreach ($province as $row) { ?>

                      <?php if($classes->id_province == $row->id){ ?>

                      <option value="<?= $row->id ?>" selected=""><?= $row->province_name ?></option>

                      <?php } else { ?>                        

                      <option value="<?= $row->id ?>"><?= $row->province_name ?></option>
                      
                      <?php } } ?>
                    
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Category</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="category">
                      <option value="">- Select a Category -</option>

                      <?php foreach ($category as $row) { ?>

                      <?php if($classes->id_category == $row->id){ ?>

                      <option value="<?= $row->id ?>" selected=""><?= $row->category_name ?></option>

                      <?php } else { ?>

                      <option value="<?= $row->id ?>"><?= $row->category_name ?></option>
                        
                      <?php } } ?>

                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Description</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="description" placeholder="Tell about this class..."><?= $classes->description ?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Status</label>
                  <div class="col-sm-10">

                    <?php if($classes->status == 1){ ?>

                    <div class="radio">
                      <label><input type="radio" name="status" value="1" checked=""> Active</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="status" value="0"> Inactive</label>
                    </div>

                    <?php } else { ?>

                    <div class="radio">
                      <label><input type="radio" name="status" value="1"> Active</label>
                    </div>
                    <div class="radio">
                      <label><input type="radio" name="status" value="0" checked=""> Inactive</label>
                    </div>

                    <?php } ?>

                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-10">
                    <input type="submit" name="save" class="btn btn-rect btn-success" value="Save Change">
                  </div>
                </div>

              </div>

            <?= form_close(); ?>


            </div>
          </div>



          <div class="box">
            <header>
              <h4><i class="fa fa-image"></i> Change Profile Photo</h4>
              <div class="box-tools">
                <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
                <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
              </div>
            </header>
            <div class="box-body">  

              <?= form_open_multipart('classes/update_image/'.$this->uri->segment(3)) ?>

              <div class="form-horizontal">

                <input type="hidden" name="id" value="<?= $this->uri->segment(3) ?>">

                <div class="row">
                  <div class="col-md-2">
                    <img src="<?= base_url('assets/images/classes/'.$classes->image) ?>" class="img-responsive thumbnail">
                  </div>  
                  <div class="col-md-10">
                    <div class="form-group">
                      <label>Image Classes</label>
                      <input type="file" name="image">
                    </div>
                    <div class="form-group">
                      <div>
                        <input type="submit" class="btn btn-rect btn-success" name="save" value="Save Change">
                        <a href="<?= base_url('classes') ?>" class="btn btn-rect btn-danger">Cancel</a>
                      </div>
                    </div>
                  </div>  
                </div>
              </div>

              <?= form_close() ?>

            </div>
          </div>

        </div>
      </div>

    </div>
    <!-- END: .container-fluid -->
  </div>
  <!-- END: .main-content -->

