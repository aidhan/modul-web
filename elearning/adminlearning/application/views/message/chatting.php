<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="#">

    <title><?= $title_page ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('assets/vendor/bootstrap/css/bootstrap.min.css') ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=Playfair+Display:700,900" rel="stylesheet">

    <style>
      .scroll{
        width: 100%;
        padding: 10px;
        overflow-y: scroll;
        height: 450px;
        margin: 20px 0;
        
      }
    </style>

  </head>

  <body>

    <div class="container">      
      <h2><?= $title_page ?></h2>

      <div class="row">
        <div class="col-md-3">
          <div class="list-group">

            <?php foreach ($users as $users) { ?>

            <a href="<?= base_url('message/chat/'.$users->id_user_2) ?>" class="list-group-item list-group-item-action list-group-item-primary">
              <?php 
              echo $users->id_user_2;
              $number = $this->mmessage->numberOfMessage($users->id_user_2, $this->session->userdata('username'));
              if($this->uri->segment(3) == $users->id_user_2){
                $this->mmessage->readMessageUpdate($users->id_user_2, $this->session->userdata('username'));
              }
              if($number->total > 0){
              ?>

              <span class="badge badge-success"><?= $number->total ?></span>

              <?php } ?>

            </a>
              
            <?php } ?>

          </div>
          
        </div>
        <div class="col-md-9">
          <div class="scroll" id="bottom">

          <?php foreach ($chatting as $row) { ?>

            <?php if($row->id_user_1 == $this->session->userdata('username')){ ?>
            
            <div class="alert alert-success" align="right" style="float: right; width: 75%">
              <b>You</b>
              <p><?= $row->message ?></p>
              <sub><?= $row->timestamp ?></sub>
            </div>
            
            <?php } else { ?>

            <div class="alert alert-info" align="left" style="float:left; width: 75%">
              <b><?= $row->id_user_1 ?></b>
              <p><?= $row->message ?></p>
              <sub><?= $row->timestamp ?></sub>
            </div>
          <?php } } ?>
        
        </div>

          <?= form_open('message/sent/'.$this->uri->segment(3)); ?>

            <textarea name="message" class="form-control" placeholder="Type your message ...."></textarea><br>
            <input type="submit" name="send" class="btn btn-primary" value="Send">

          <?= form_close(); ?>
          
        </div>
      </div>

    </div>

    <script>
      window.onload=function () {
        var objDiv = document.getElementById("bottom");
        objDiv.scrollTop = objDiv.scrollHeight;
      }
    </script>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="<?= base_url('assets/vendor/bootstrap/js/bootstrap.min.js') ?>"></script>
  </body>
</html>