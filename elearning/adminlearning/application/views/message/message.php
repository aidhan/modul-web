<!-- begin .app-main -->
<div class="app-main">
<!-- begin .main-heading -->
<header class="main-heading shadow-2dp">
  <!-- begin dashhead -->
  <div class="dashhead bg-white">
    <div class="dashhead-titles">
      <h1 class="dashhead-title"><?= $title_page ?></h1>
    </div>

    <div class="dashhead-toolbar">
      <div class="dashhead-toolbar-item">
        <a href="<?= base_url() ?>">Dashboard</a>
        / <?= $title_page ?>
      </div>
    </div>
  </div>
  <!-- END: dashhead -->
</header>
<!-- END: .main-heading -->

<!-- begin .main-content -->
<div class="main-content bg-clouds">

  <!-- begin .container-fluid -->
  <div class="container-fluid p-t-15">

    <div class="row">
      <div class="col-sm-12">
        <div class="box">
          <header>
            <h4><i class="fa fa-table"></i> Data <?= $title_page ?></h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>

          <div class="box-body">
            
            <div class="table-responsive">
              <table data-plugin="datatables" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>User 1</th>
                    <th>User 2</th>
                    <th>Message</th>
                    <th>Timestamp</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $no = 0; foreach($message as $row){ $no++; ?>

                  <tr>
                    <td><?= $no ?></td>
                    <td><?= $row->id_user_1 ?></td>
                    <td><?= $row->id_user_2 ?></td>
                    <td><?php if(strlen($row->message) > 50) { echo mb_substr($row->message, 0, 50)."...."; } else { echo $row->message; } ?></td>
                    <td><?= $row->timestamp ?></td>
                  </tr>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- END: .container-fluid -->
</div>
<!-- END: .main-content -->

