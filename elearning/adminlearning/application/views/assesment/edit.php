<!-- begin .app-main -->
<div class="app-main">
  <!-- begin .main-heading -->
  <header class="main-heading shadow-2dp">
    <!-- begin dashhead -->
    <div class="dashhead bg-white">
      <div class="dashhead-titles">
        <h1 class="dashhead-title"><?= $title_page ?></h1>
      </div>

      <div class="dashhead-toolbar">
        <div class="dashhead-toolbar-item">
          <a href="<?= base_url() ?>">Dashboard</a>
          / <a href="<?= base_url('assesment') ?>">Assesment</a>
          / <?= $title_page ?>
        </div>
      </div>
    </div>
    <!-- END: dashhead -->
  </header>
  <!-- END: .main-heading -->

  <!-- begin .main-content -->
  <div class="main-content bg-clouds">

    <!-- begin .container-fluid -->
    <div class="container-fluid p-t-15">

      <?php if($this->session->flashdata('info')){ ?>

      <div class="alert alert-success alert-dismissible show" role="alert">
          <?= $this->session->flashdata('info'); ?>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
      </div>

      <?php } ?>

      <div class="row">
        <div class="col-sm-12">
          <div class="box">
            <header>
              <h4><i class="fa fa-edit"></i> Edit Data</h4>
              <div class="box-tools">
                <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a> 
                <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
              </div>
            </header>

            <div class="box-body">

            <?= form_open('assesment/update_data/'.$this->uri->segment(3)) ?>

              <div class="form-horizontal">

                <div class="form-group">
                  <label class="col-sm-2 control-label">Question</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="question"><?= $assesment->question ?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Classes</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="id_classes">
                      <option value="">- Select a Class -</option>

                      <?php foreach ($classes as $row) { 
                        if($assesment->id_classes == $row->id) { ?>

                      <option value="<?= $row->id ?>" selected><?= $row->class_name ?></option>

                      <?php } else { ?>

                      <option value="<?= $row->id ?>"><?= $row->class_name ?></option>
                        
                      <?php } } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-10">
                    <input type="submit" name="save" class="btn btn-rect btn-success" value="Save Change">
                  </div>
                </div>

              </div>

            <?= form_close(); ?>


            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col-sm-12">
          <div class="box">
            <header>
              <h4><i class="fa fa-image"></i> Change Image</h4>
              <div class="box-tools">
                <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a> 
                <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
              </div>
            </header>

            <div class="box-body">

            <?= form_open_multipart('assesment/update_image/'.$this->uri->segment(3)) ?>

              <div class="form-horizontal">

                <div class="row">
                  <div class="col-md-2">
                    <img src="<?= base_url('assets/images/assesment/assesment/'. $assesment->image) ?>" class="img-responsive thumbnail">
                  </div>
                  <div class="col-md-10">
                    <div class="form-group">
                      <label>Image</label>
                        <input type="file" name="image" class="form-control">
                    </div>

                    <div class="form-group">
                        <input type="submit" name="save" class="btn btn-rect btn-success" value="Save Change">
                    </div>    
                  </div>
                </div>

              </div>

            <?= form_close(); ?>


            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- END: .container-fluid -->
  </div>
  <!-- END: .main-content -->

