<!-- begin .app-main -->
<div class="app-main">
  <!-- begin .main-heading -->
  <header class="main-heading shadow-2dp">
    <!-- begin dashhead -->
    <div class="dashhead bg-white">
      <div class="dashhead-titles">
        <h1 class="dashhead-title"><?= $title_page ?></h1>
      </div>

      <div class="dashhead-toolbar">
      </div>
    </div>
    <!-- END: dashhead -->
  </header>
  <!-- END: .main-heading -->

  <!-- begin .main-content -->
  <div class="main-content bg-clouds">
    <!-- begin .container-fluid -->
    <div class="container-fluid p-t-15">

      <!-- begin statarea chart -->
      <div class="row">
        <div class="col-xs-6 col-sm-3">
          <div class="box p-a-0 bg-peter-river b-r-3">
            <div class="p-a-15">
              <i class="fa fa-fw fa-graduation-cap"></i>
              <span class="text-white">STUDENT</span>
              <div class="f-5 text-white">
                <span class="counterup">
                  <?= $count_student->total ?>   
                </span>
                <span class="h4">Student</span>
              </div>
            </div>
          </div>
        </div>

        <div class="col-xs-6 col-sm-3">
          <div class="box p-a-0 bg-nephritis b-r-3">
            <div class="p-a-15">
              <i class="fa fa-fw fa-user"></i>
              <span class="text-white">TEACHER</span>
              <div class="f-5 text-white">
                <span class="counterup">
                  <?= $count_teacher->total ?>   
                </span>
                <span class="h4">Teacher</span>
              </div>
            </div>
          </div>
        </div>
        <div class="clearfix visible-xs-block"></div>

        <div class="col-xs-6 col-sm-3">
          <div class="box p-a-0 bg-wisteria b-r-3">
            <div class="p-a-15">
              <i class="fa fa-fw fa-puzzle-piece"></i>
              <span class="text-white">CLASSES</span>
              <div class="f-5 text-white">
                <span class="counterup">
                  <?= $count_classes->total ?>   
                </span>
                <span class="h4">Class</span>
              </div>
            </div>
          </div>
        </div>
        <div class="col-xs-6 col-sm-3">
          <div class="box p-a-0 bg-sun-flower b-r-3">
            <div class="p-a-15">
              <i class="fa fa-fw fa-tags text-white"></i>
              <span class="text-white">CATEGORY</span>
              <div class="f-5 text-white">
                <span class="counterup">
                  <?= $count_category->total ?>   
                </span>
                <span class="h4">Category</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- END: statarea chart -->

      <!-- BEGIN: .row -->
      <div class="row">
        <div class="col-sm-7">
          <div class="box">
            <header class="bg-turquoise text-white">
              <h4><i class="fa fa-user"></i> User Online</h4>
            </header>
            <div class="box-body p-a-0 max-h-lg ps" style="margin:10px">
              <table class="table table-striped table-hover ps" data-plugin="datatables">
                <thead>
                  <tr>
                    <th>#</th>
                    <th>Username</th>
                    <th>E-mail</th>
                    <th>Verify</th>
                    <th>User Role</th>
                  </tr>
                </thead>
                <tbody>

                  <?php $no = 0; foreach($user_online as $row1){ $no++; ?>

                  <tr>
                    <td><?= $no ?></td>
                    <td><?= $row1->username ?></td>
                    <td><?= $row1->email ?></td>
                    <td><?= $row1->verify == 1? 'Yes' : 'No' ?></td>
                    <td>

                      <?php 
                      if($row1->level == 0)
                        echo "Super Admin";
                      else if($row1->level == 1)
                        echo "Admin";
                      else if($row1->level == 2)
                        echo "Teacher";
                      else if($row1->level == 3)
                        echo "Student";
                      ?>
                        
                    </td>
                  </tr>

                  <?php } ?>

                </tbody>
              </table>
            </div>
          </div>
        </div>

        <div class="col-sm-5">
          <div class="box shadow-2dp b-r-2">
            <header class="b-b bg-alizarin text-white">
              <h4><i class="fa fa-retweet"></i> Recent Activity</h4>
            </header>
            <div class="box-body">
              <ul class="members">


                <?php if($this->mlog_activity->count()->total == 0){ ?>

                <li class="member">
                  <div class="member-info">
                    <h4 class="member-name">No Recent Activity</h4>
                    <div class="member-skills"></div>
                  </div>
                </li>

                <?php } foreach($log_activity as $row2){ ?>

                <li class="member">
                  <div class="member-media">
                    <a class="member-media-link" href="#">
                      <img class="member-media-img" src="<?= base_url('assets/images/user.png') ?>" alt="Bret">
                    </a>
                  </div>
                  <div class="member-info">
                    <h4 class="member-name"><?= ucwords($row2->username) ?></h4>
                    <div class="member-skills"><?= $row2->action ?></div>
                  </div>
                </li>

                <?php 
                } 
                if($this->mlog_activity->count()->total > 0){
                  if($this->session->userdata('level') == 0){ ?>

                  <li class="member">
                    <div class="member-info">
                      <div class="member-skills">
                        <a href="<?= base_url('log_activity') ?>">Show All <i class="fa fa-angle-double-right"></i></a>
                      </div>
                    </div>
                  </li>

                <?php } } ?> 

              </ul>
            </div>
          </div>
        </div>

      </div>
      <!-- END: .row -->

    </div>
    <!-- END: .container-fluid -->

  </div>
  <!-- END: .main-content -->

