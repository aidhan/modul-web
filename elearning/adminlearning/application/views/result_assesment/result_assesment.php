<!-- begin .app-main -->
<div class="app-main">
<!-- begin .main-heading -->
<header class="main-heading shadow-2dp">
  <!-- begin dashhead -->
  <div class="dashhead bg-white">
    <div class="dashhead-titles">
      <h1 class="dashhead-title"><?= $title_page ?></h1>
    </div>

    <div class="dashhead-toolbar">
      <div class="dashhead-toolbar-item">
        <a href="<?= base_url() ?>">Dashboard</a>
        / <?= $title_page ?>
      </div>
    </div>
  </div>
  <!-- END: dashhead -->
</header>
<!-- END: .main-heading -->

<!-- begin .main-content -->
<div class="main-content bg-clouds">

  <!-- begin .container-fluid -->
  <div class="container-fluid p-t-15">

    <?php if($this->session->flashdata('info')){ ?>

    <div class="alert alert-success alert-dismissible show" role="alert">
        <?= $this->session->flashdata('info'); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <?php } ?>

    <div class="row">
      <div class="col-sm-12">
        <div class="box">
          <header>
            <h4><i class="fa fa-table"></i> Data <?= $title_page ?></h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>

          <div class="box-body">

            <?php if($this->mresult_assesment->count()->total > 0){ ?>
            
            <div class="form-group">
              <a href="<?= base_url('result_assesment/export') ?>" title="Details" class="btn btn-rect btn-success"><i class="fa fa-download"></i> Export to Excel</a>
              <a href="<?= base_url('result_assesment/delete_all') ?>" onclick="return confirm('Are you sure to delete all data?')" class="btn btn-rect btn-danger"><i class="fa fa-trash"></i> Delete All</a>
            </div>

            <?php } ?>
            
            <div class="table-responsive">
              <table data-plugin="datatables" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Student</th>
                    <th>Score</th>
                    <th>Class</th>
                    <th>Comment</th>
                    <th>Timestamp</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                  <?php 
                  $no = 0; 
                  foreach($result_assesment as $row){ 
                    $no++;
                    $classes = $this->db->get_where('classes', array('id' => $row->id_classes))->row(); 
                    $teacher = $this->db->get_where('teacher', array('id' => $classes->id_teacher))->row(); 
                    $student = $this->db->get_where('student', array('id' => $row->id_student))->row(); 
                    $assesment = $this->db->query("SELECT SUM(score) AS a_score FROM result_assesment WHERE id_student='$row->id_student' AND id_assesment='$row->id_assesment'")->row();  
                  ?>

                  <tr>
                    <td><?= $no ?></td>
                    <td><?= $student->first_name ." ". $student->last_name ?></td>
                    <td><?= $row->score ?></td>
                    <td><?= $classes->class_name ?></td>
                    <td><?= $row->evaluation ?></td>
                    <td><?= $row->timestamp ?></td>
                    <td>
                      <button data-toggle="modal" data-target="#exampleModal<?= $no ?>" class="btn btn-sm btn-rect btn-info" title="Details"><i class="fa fa-eye"></i> Show Details</button>
                    </td>
                  </tr>

                  <!-- Modal -->
                  <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal<?= $no ?>">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h3>Result Assesment ID <?= $row->id ?></h3>
                        </div>
                        <div class="modal-body">

                          <?php

                          $task_start = explode('.',$row->task_start);

                          if($task_start[1] == 'jpg' || $task_start[1] == 'jpeg' || $task_start[1] == 'png'){
                            $tag1 = 'img';
                          } else if($task_start[1] == 'mp4' || $task_start[1] == 'mkv'){
                            $tag1 = 'video';
                          }

                          $task_process = explode('.',$row->task_process);

                          if($task_process[1] == 'jpg' || $task_process[1] == 'jpeg' || $task_process[1] == 'png'){
                            $tag2 = 'img';
                          } else if($task_process[1] == 'mp4' || $task_process[1] == 'mkv'){
                            $tag2 = 'video';
                          }
                          $task_final = explode('.',$row->task_final);

                          if($task_final[1] == 'jpg' || $task_final[1] == 'jpeg' || $task_final[1] == 'png'){
                            $tag3 = 'img';
                          } else if($task_final[1] == 'mp4' || $task_final[1] == 'mkv'){
                            $tag3 = 'video';
                          }

                          ?>

                          <?php if($tag1 == 'img'){ ?>

                          <h4>Prepared</h4>
                          <img src="<?= base_url()."assets/images/assesment/task_start/".$row->task_start ?>" style="max-width: 100%">

                          <?php } else if($tag1 == 'video'){ ?>

                          <h4>Prepared</h4>
                          <video style="width: 100%; border: 1px solid silver" controls>                            
                            <source src="<?= base_url()."assets/images/assesment/task_start/".$row->task_start ?>" type="video/mp4">
                          </video>

                          <?php } if($tag2 == 'img'){ ?>

                          <h4>Proccess</h4>
                          <img src="<?= base_url()."assets/images/assesment/task_process/".$row->task_process ?>" style="max-width: 100%">

                          <?php } else if($tag2 == 'video'){ ?>

                          <h4>Proccess</h4>
                          <video style="width: 100%; border: 1px solid silver" controls>                          
                            <source src="<?= base_url()."assets/images/assesment/task_process/".$row->task_process ?>" type="video/mp4">
                          </video>

                          <?php } if($tag3 == 'img'){ ?>

                          <h4>Ending</h4>
                          <img src="<?= base_url()."assets/images/assesment/task_final/".$row->task_final ?>" style="max-width: 100%">
                          
                          <?php } else if($tag3 == 'video'){ ?>

                          <h4>Ending</h4>
                          <video style="width: 100%; border: 1px solid silver" controls>           
                            <source src="<?= base_url()."assets/images/assesment/task_start/".$row->task_start ?>" type="video/mp4">
                          </video>
                          
                          <?php } ?>

                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- END: .container-fluid -->
</div>
<!-- END: .main-content -->

