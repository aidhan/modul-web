<!-- begin .app-main -->
<div class="app-main">
<!-- begin .main-heading -->
<header class="main-heading shadow-2dp">
  <!-- begin dashhead -->
  <div class="dashhead bg-white">
    <div class="dashhead-titles">
      <h1 class="dashhead-title"><?= $title_page ?></h1>
    </div> 

    <div class="dashhead-toolbar">
      <div class="dashhead-toolbar-item">
        <a href="<?= base_url() ?>">Dashboard</a>
        / <?= $title_page ?>
      </div>
    </div>
  </div>
  <!-- END: dashhead -->
</header>
<!-- END: .main-heading -->

<!-- begin .main-content -->
<div class="main-content bg-clouds">

  <!-- begin .container-fluid -->
  <div class="container-fluid p-t-15">

    <?php if($this->session->flashdata('info')){ ?>

    <div class="alert alert-success alert-dismissible show" role="alert">
        <?= $this->session->flashdata('info'); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <?php } ?>

    <div class="row">
      <div class="col-sm-12">
        <div class="box">
          <header>
            <h4><i class="fa fa-table"></i> Data <?= $title_page ?></h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>

          <div class="box-body">

            <?php if($this->mevaluation_student->count()->total > 0){ ?>
            
            <div class="form-group">
              <a href="<?= base_url('evaluation_student/export') ?>" title="Details" class="btn btn-rect btn-success"><i class="fa fa-download"></i> Export to Excel</a>
              <a href="<?= base_url('evaluation_student/delete_all') ?>" onclick="return confirm('Are you sure to delete all data?')" class="btn btn-rect btn-danger"><i class="fa fa-trash"></i> Delete All</a>
            </div>

            <?php } ?>
            
            <div class="table-responsive">
              <table data-plugin="datatables" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Student</th>
                    <th>Assesment</th>
                    <th>Quiz</th>
                    <th>Teacher</th>
                    <th>Class</th>
                    <th>Timestamp</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody> 

                  <?php 
                  $no = 0; 
                  foreach($evaluation_student as $row){ 
                    $no++; 

                    $classes = $this->db->get_where('classes', array('id' => $row->id_classes))->row(); 
                    $teacher = $this->db->get_where('teacher', array('id' => $classes->id_teacher))->row(); 
                    $student = $this->db->get_where('student', array('id' => $row->id_student))->row(); 
                    $assesment = $this->db->query("SELECT SUM(score) AS a_score FROM result_assesment WHERE id_student='$row->id_student' AND id_assesment='$row->id_assesment'")->row(); 
                    $quiz = $this->db->query("SELECT SUM(score) AS q_score FROM result_quiz WHERE id_student='$row->id_student' AND id_classes='$row->id_classes'")->row(); 
                  ?>

                  <tr>
                    <td><?= $no ?></td>
                    <td><?= $student->first_name." ".$student->last_name ?></td>
                    <td><?= $assesment->a_score ?></td>
                    <td><?= $quiz->q_score ?></td>
                    <td><?= $teacher->first_name." ".$teacher->last_name ?></td>
                    <td><?= $classes->class_name ?></td>
                    <td><?= $row->timestamp ?></td>
                    <td>
                      <button data-toggle="modal" data-target="#exampleModal<?= $no ?>" class="btn btn-sm btn-rect btn-info" title="Details"><i class="fa fa-eye"></i> Show Details</button>
                    </td>
                  </tr>   

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- END: .container-fluid -->
</div>
<!-- END: .main-content -->

<?php $no_ = 0; foreach($evaluation_student as $row1){ $no_++; ?>

<!-- Modal -->
<div class="modal fade" tabindex="-1" role="dialog" id="exampleModal<?= $no_ ?>">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h3>Evaluation Student ID <?= $row1->id ?></h3>
      </div>
      <div class="modal-body">

        <h4>Result Quiz</h4>

        <?php 
        $numb_ = 0;
        $detail_quiz = $this->db->get_where('result_quiz', array('id_student' => $row1->id_student, 'id_classes' => $row1->id_classes))->result();
        $assesment = $this->db->query("SELECT SUM(score) AS a_score FROM result_assesment WHERE id_student='$row1->id_student' AND id_assesment='$row1->id_assesment'")->row();
        $quiz = $this->db->query("SELECT SUM(score) AS q_score FROM result_quiz WHERE id_student='$row1->id_student' AND id_classes='$row1->id_classes'")->row(); 
        ?> 
        <div class="table-responsive">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Question</th>
                <th>Answer</th>
                <th>Score</th>
              </tr>
            </thead>
            <tbody>

            <?php foreach($detail_quiz as $get_detail){ $numb_++; ?>
            <?php $question = $this->db->get_where('quiz', array('id_classes' => $get_detail->id_classes, 'id' => $get_detail->id_quiz))->row();  ?>
              <tr>
                <td><?= $numb_ ?></td>
                <td><?= $question->question ?></td>
                <td><?= $get_detail->answer ?></td>
                <td><?= $get_detail->score ?></td>
              </tr>
            <?php } ?>
            
              <tr>
                <th colspan="3">Total Score</th>
                <th><?= $quiz->q_score ?></th>
              </tr>
            </tbody>

          </table>
        </div>

        <h4>Result Assesment</h4>

        <div class="table-responsive">
          <table class="table table-bordered table-striped">
            <tr>
              <th>Score : <?= $assesment->a_score ?></td>
            </tr>
          </table>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<?php } ?>