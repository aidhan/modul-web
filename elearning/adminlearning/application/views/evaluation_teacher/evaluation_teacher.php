<!-- begin .app-main -->
<div class="app-main">
<!-- begin .main-heading -->
<header class="main-heading shadow-2dp">
  <!-- begin dashhead -->
  <div class="dashhead bg-white">
    <div class="dashhead-titles">
      <h1 class="dashhead-title"><?= $title_page ?></h1>
    </div>

    <div class="dashhead-toolbar">
      <div class="dashhead-toolbar-item">
        <a href="<?= base_url() ?>">Dashboard</a>
        / <?= $title_page ?>
      </div>
    </div>
  </div>
  <!-- END: dashhead -->
</header>
<!-- END: .main-heading -->

<!-- begin .main-content -->
<div class="main-content bg-clouds">

  <!-- begin .container-fluid -->
  <div class="container-fluid p-t-15">

    <?php if($this->session->flashdata('info')){ ?>

    <div class="alert alert-success alert-dismissible show" role="alert">
        <?= $this->session->flashdata('info'); ?>
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
    </div>

    <?php } ?>

    <div class="row">
      <div class="col-sm-12">
        <div class="box">
          <header>
            <h4><i class="fa fa-table"></i> Data <?= $title_page ?></h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>

          <div class="box-body">
            
            <?php if($this->mevaluation_teacher->count()->total > 0){ ?>
            
            <div class="form-group">
              <a href="<?= base_url('evaluation_teacher/export') ?>" title="Details" class="btn btn-rect btn-success"><i class="fa fa-download"></i> Export to Excel</a>
              <a href="<?= base_url('evaluation_teacher/delete_all') ?>" onclick="return confirm('Are you sure to delete all data?')" class="btn btn-rect btn-danger"><i class="fa fa-trash"></i> Delete All</a>
            </div>

            <?php } ?>
            
            <div class="table-responsive">
              <table data-plugin="datatables" class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Teacher</th>
                    <th>Class</th>
                    <th>Module</th>
                    <th>Quiz</th>
                    <th>Timestamp</th>
                  </tr>
                </thead>
                <tbody>

                  <?php 
                  $no = 0; 
                  foreach($evaluation_teacher as $row){ 
                    $no++;                     
                    $classes = $this->db->get_where('classes', array('id' => $row->id_classes))->row(); 
                    $teacher = $this->db->get_where('teacher', array('id' => $classes->id_teacher))->row(); 
                  ?>

                  <tr>
                    <td><?= $no ?></td>
                    <td><?= $teacher->first_name ." ". $teacher->last_name ?></td>
                    <td><?= $classes->class_name ?></td>
                    <td><?= $row->module ?></td>
                    <td><?= $row->quiz ?></td>
                    <td><?= $row->timestamp ?></td>
                  </tr>

                  <!-- Modal -->
                  <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal<?= $no ?>">
                    <div class="modal-dialog" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h3>Evaluation Teacher ID <?= $row->id ?></h3>
                        </div>
                        <div class="modal-body">
                          <?= $no ?>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        </div>
                      </div>
                    </div>
                  </div>

                  <?php } ?>
                
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- END: .container-fluid -->
</div>
<!-- END: .main-content -->

