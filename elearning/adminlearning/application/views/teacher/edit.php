<!-- begin .app-main -->
<div class="app-main">
<!-- begin .main-heading -->
<header class="main-heading shadow-2dp">
  <!-- begin dashhead -->
  <div class="dashhead bg-white">
    <div class="dashhead-titles">
      <h1 class="dashhead-title"><?= $title_page ?></h1>
    </div>

    <div class="dashhead-toolbar">
      <div class="dashhead-toolbar-item">
        <a href="<?= base_url() ?>">Dashboard</a>
        / <a href="<?= base_url('teacher') ?>">Teacher</a>
        / <?= $title_page ?>
      </div>
    </div>
  </div>
  <!-- END: dashhead -->
</header>
<!-- END: .main-heading -->

<!-- begin .main-content -->
<div class="main-content bg-clouds">

  <!-- begin .container-fluid -->
  <div class="container-fluid p-t-15">

    <?php if($this->session->flashdata('info')){ ?>

    <div class="alert alert-success"><?= $this->session->flashdata('info'); ?></div>

    <?php } if($this->session->flashdata('error')){ ?>

    <div class="alert alert-danger"><?= $this->session->flashdata('error'); ?></div>

    <?php } ?>        

    <div class="row">
      <div class="col-sm-12">
        <div class="box">
          <header>
            <h4><i class="fa fa-address-card-o"></i> <?= $title_page ?></h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>
          <div class="box-body">       

            <?= form_open('teacher/update_data/'.$this->uri->segment(3)) ?>

            <div class="form-horizontal">

              <input type="hidden" name="username" value="<?= $this->uri->segment(3) ?>">

              <div class="form-group">
                <label class="col-sm-2 control-label">NIDN</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nidn" placeholder="NIDN" value="<?= $teacher->nidn ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Full Name</label>
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-md-6">
                      <input type="text" class="form-control" placeholder="First Name" name="first_name" value="<?= $teacher->first_name ?>">
                    </div>
                    <div class="col-md-6">
                      <input type="text" class="form-control" placeholder="Last Name" name="last_name" value="<?= $teacher->last_name ?>">
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Gender</label>
                <div class="col-sm-10">

                  <?php if($teacher->gender == 'm'){ ?>

                  <div class="radio">
                    <label><input type="radio" name="gender" value="m" checked=""> Male</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="gender" value="f"> Female</label>
                  </div>

                  <?php } else { ?>

                  <div class="radio">
                    <label><input type="radio" name="gender" value="m"> Male</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="gender" value="f" checked=""> Female</label>
                  </div>

                  <?php } ?>

                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="address" size="4" placeholder="Enter address..."><?= $teacher->address ?></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="phone" placeholder="exp. +6281234567890" value="<?= $teacher->phone ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" name="email" placeholder="exp. erisdsr@elearning.com"  value="<?= $user->email ?>">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Verify</label>
                <div class="col-sm-10">
                  <select name="verify" class="form-control">
                    
                    <?php if($user->verify == 1){ ?>

                    <option value="1" selected="">Yes</option>
                    <option value="0">No</option>

                    <?php } else { ?>

                    <option value="1">Yes</option>
                    <option value="0" selected="">No</option>

                    <?php } ?>

                  </select>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                  <input type="submit" class="btn btn-rect btn-success" name="save" value="Save Change">
                  <a href="<?= base_url('teacher') ?>" class="btn btn-rect btn-danger">Cancel</a>
                </div>
              </div>

            </div>

            <?= form_close() ?>

          </div>
        </div>


        <div class="box">
          <header>
            <h4><i class="fa fa-lock"></i> Change Password</h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>
          <div class="box-body">

            <?= form_open('teacher/update_password/'.$this->uri->segment(3)) ?>

            <div class="form-horizontal">

              <input type="hidden" name="username" value="<?= $this->uri->segment(3) ?>">

              <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" name="password" placeholder="New password">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Retype Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" name="retype_password" placeholder="Retype a new password">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                  <input type="submit" class="btn btn-rect btn-success" name="save" value="Save Change">
                  <a href="<?= base_url('teacher') ?>" class="btn btn-rect btn-danger">Cancel</a>
                </div>
              </div>

            </div>

            <?= form_close() ?>

          </div>
        </div>

        <div class="box">
          <header>
            <h4><i class="fa fa-image"></i> Change Profile Photo</h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>
          <div class="box-body">  

            <?= form_open_multipart('teacher/update_profile_photo/'.$this->uri->segment(3)) ?>

            <div class="form-horizontal">

              <input type="hidden" name="username" value="<?= $this->uri->segment(3) ?>">

              <div class="row">
                <div class="col-md-2">
                  <img src="<?= base_url('assets/images/teacher/'.$teacher->image) ?>" class="img-responsive thumbnail">
                </div>  
                <div class="col-md-10">
                  <div class="form-group">
                    <label>Profile Photo</label>
                    <input type="file" name="image">
                  </div>
                  <div class="form-group">
                    <div>
                      <input type="submit" class="btn btn-rect btn-success" name="save" value="Save Change">
                      <a href="<?= base_url('teacher') ?>" class="btn btn-rect btn-danger">Cancel</a>
                    </div>
                  </div>
                </div>  
              </div>
            </div>

            <?= form_close() ?>

          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- END: .container-fluid -->
</div>
<!-- END: .main-content -->

