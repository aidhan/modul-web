<!-- begin .app-main -->
<div class="app-main">
<!-- begin .main-heading -->
<header class="main-heading shadow-2dp">
  <!-- begin dashhead -->
  <div class="dashhead bg-white">
    <div class="dashhead-titles">
      <h1 class="dashhead-title"><?= $title_page ?></h1>
    </div>

    <div class="dashhead-toolbar">
      <div class="dashhead-toolbar-item">
        <a href="<?= base_url() ?>">Dashboard</a>
        / <a href="<?= base_url('teacher') ?>">Teacher</a>
        / <?= $title_page ?>
      </div>
    </div>
  </div>
  <!-- END: dashhead -->
</header>
<!-- END: .main-heading -->

<!-- begin .main-content -->
<div class="main-content bg-clouds">

  <!-- begin .container-fluid -->
  <div class="container-fluid p-t-15">

    <?php if($this->session->flashdata('info')){ ?>

    <div class="alert alert-success"><?= $this->session->flashdata('info'); ?></div>

    <?php } ?>          

    <div class="row">
      <div class="col-sm-12">
        <div class="box">
          <header>
            <h4><i class="fa fa-plus"></i> <?= $title_page ?></h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>
          <div class="box-body">  

            <?= form_open_multipart('teacher/insert') ?>

            <div class="form-horizontal">

              <div class="form-group">
                <label class="col-sm-2 control-label">NIDN</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="nidn" placeholder="NIDN">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Full Name</label>
                <div class="col-sm-10">
                  <div class="row">
                    <div class="col-md-6">
                      <input type="text" class="form-control" placeholder="First Name" name="first_name">
                    </div>
                    <div class="col-md-6">
                      <input type="text" class="form-control" placeholder="Last Name" name="last_name">
                    </div>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Address</label>
                <div class="col-sm-10">
                  <textarea class="form-control" name="address" size="4" placeholder="Enter address..."></textarea>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Gender</label>
                <div class="col-sm-10">
                  <div class="radio">
                    <label><input type="radio" name="gender" value="m"> Male</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="gender" value="f"> Female</label>
                  </div>
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Phone</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="phone" placeholder="exp. +6281234567890">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" name="email" placeholder="exp. erisdsr@elearning.com">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="username" placeholder="exp. erisdsr">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Profile Photos</label>
                <div class="col-sm-10">
                  <input type="file" class="form-control" name="image">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Password</label>
                <div class="col-sm-10">
                  <input type="password" class="form-control" name="password" placeholder=" Enter password">
                </div>
              </div>
            
              <input type="hidden" name="level" value="2">
              <input type="hidden" name="verify" value="1">

              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                  <input type="submit" class="btn btn-rect btn-success" name="save" value="Add Data">
                  <a href="<?= base_url('teacher') ?>" class="btn btn-rect btn-danger">Cancel</a>
                </div>
              </div>

            </div>

            <?= form_close() ?>

          </div>
        </div>
      </div>
    </div>

  </div>
  <!-- END: .container-fluid -->
</div>
<!-- END: .main-content -->

