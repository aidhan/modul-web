        <!-- begin .app-main -->
        <div class="app-main">
          <!-- begin .main-heading -->
          <header class="main-heading shadow-2dp">
            <!-- begin dashhead -->
            <div class="dashhead bg-white">
              <div class="dashhead-titles">
                <h1 class="dashhead-title"><?= $title_page ?></h1>
              </div>

              <div class="dashhead-toolbar">
                <div class="dashhead-toolbar-item">
                  <a href="<?= base_url() ?>">Dashboard</a>
                  / <?= $title_page ?>
                </div>
              </div>
            </div>
            <!-- END: dashhead -->
          </header>
          <!-- END: .main-heading -->

          <!-- begin .main-content -->
          <div class="main-content bg-clouds">

            <!-- begin .container-fluid -->
            <div class="container-fluid p-t-15">

              <?php if($this->session->flashdata('info')){ ?>

              <div class="alert alert-success alert-dismissible show" role="alert">
                  <?= $this->session->flashdata('info'); ?>
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                  </button>
              </div>

              <?php } ?>

              <div class="row">
                <div class="col-sm-12">
                  <div class="box">
                    <header>
                      <h4><i class="fa fa-plus"></i> Add Data <?= $title_page ?></h4>
                      <div class="box-tools">
                        <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                        <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
                        <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
                      </div>
                    </header>
                    <div class="box-body">

                      <?= form_open('category/add'); ?>

                      <div class="form-inline">
                        <div class="form-group">
                            <input type="text" name="category_name" placeholder="Write category name..." class="form-control" size="50%">
                            <input type="submit" name="save" class="btn btn-rect btn-success" style="border-radius: 0;margin-top: 2px" value="Add Data">
                        </div>
                      </div>

                      <?= form_close(); ?>

                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-sm-12">
                  <div class="box">
                    <header>
                      <h4><i class="fa fa-table"></i> Data <?= $title_page ?></h4>
                      <div class="box-tools">
                        <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                        <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
                        <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
                      </div>
                    </header>
                    <div class="box-body">
                      <div class="table-responsive">
                        <table data-plugin="datatables" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>Category Name</th>
                              <th>Action</th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php foreach($category as $row){  ?>

                            <tr>
                              <td><?= $row->id; ?></td>
                              <td><?= $row->category_name; ?></td>
                              <td>
                                <a href="<?= base_url("category/delete/$row->id") ?>" class="btn btn-rect btn-sm btn-danger" title="Delete" onclick="return confirm('Are you sure to delete data?')" ><i class="fa fa-trash"></i></a>
                                <a href="<?= base_url("category/edit/$row->id") ?>" class="btn btn-rect btn-sm btn-warning" title="Edit"><i class="fa fa-edit"></i></a>
                              </td>
                            </tr>

                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <!-- END: .container-fluid -->
          </div>
          <!-- END: .main-content -->

