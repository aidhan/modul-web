        <!-- begin .app-main -->
        <div class="app-main">
          <!-- begin .main-heading -->
          <header class="main-heading shadow-2dp">
            <!-- begin dashhead -->
            <div class="dashhead bg-white">
              <div class="dashhead-titles">
                <h1 class="dashhead-title"><?= $title_page ?></h1>
              </div>

              <div class="dashhead-toolbar">
                <div class="dashhead-toolbar-item">
                  <a href="<?= base_url() ?>">Dashboard</a>
                  / <a href="<?= base_url('category') ?>">Category</a>
                  / <?= $title_page ?>
                </div>
              </div>
            </div>
            <!-- END: dashhead -->
          </header>
          <!-- END: .main-heading -->

          <!-- begin .main-content -->
          <div class="main-content bg-clouds">

            <!-- begin .container-fluid -->
            <div class="container-fluid p-t-15">

              <?php if($this->session->flashdata('info')){ ?>

              <div class="alert alert-success">
                  <?= $this->session->flashdata('info'); ?>
              </div>

              <?php } ?>

              <div class="row">
                <div class="col-sm-12">
                  <div class="box">
                    <header>
                      <h4><i class="fa fa-edit"></i> Edit Data Category ID <?= $this->uri->segment(3); ?></h4>
                      <div class="box-tools">
                        <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                        <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
                        <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
                      </div>
                    </header>
                    <div class="box-body">

                      <?= form_open("category/update"); ?>

                      <div class="form-inline">
                        <div class="form-group">
                            <input type="hidden" name="id" value="<?= $category->id ?>">
                            <input type="text" name="category_name" value="<?= $category->category_name ?>" class="form-control" size="50%">
                            <input type="submit" name="save" class="btn btn-rect btn-success" style="border-radius: 0;margin-top: 2px" value="Save Changes">
                            <a href="<?= base_url('Category') ?>" class="btn btn-rect btn-danger" style="border-radius: 0;margin-top: 2px">Cancel</a>
                        </div>
                      </div>

                      <?= form_close(); ?>

                    </div>
                  </div>
                </div>
              </div>

            </div>
            <!-- END: .container-fluid -->
          </div>
          <!-- END: .main-content -->

