<!-- begin .app-main -->
<div class="app-main">
<!-- begin .main-heading -->
<header class="main-heading shadow-2dp">
  <!-- begin dashhead -->
  <div class="dashhead bg-white">
    <div class="dashhead-titles">
      <h1 class="dashhead-title"><?= $title_page ?></h1>
    </div>

    <div class="dashhead-toolbar">
      <div class="dashhead-toolbar-item">
        <a href="<?= base_url() ?>">Dashboard</a>
        / <a href="<?= base_url('user') ?>">Administrator</a>
        / <?= $title_page ?>
      </div>
    </div>
  </div>
  <!-- END: dashhead -->
</header>
<!-- END: .main-heading -->

<!-- begin .main-content -->
<div class="main-content bg-clouds">

  <!-- begin .container-fluid -->
  <div class="container-fluid p-t-15">

    <?php if($this->session->flashdata('info')){ ?>

    <div class="alert alert-success"><?= $this->session->flashdata('info'); ?></div>

    <?php } if($this->session->flashdata('error')){ ?>

    <div class="alert alert-danger"><?= $this->session->flashdata('error'); ?></div>

    <?php } ?>               

    <div class="row">
      <div class="col-sm-12">
        <div class="box">
          <header>
            <h4><i class="fa fa-edit"></i> <?= $title_page ?></h4>
            <div class="box-tools">
              <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
              <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
              <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
            </div>
          </header>
          <div class="box-body">  

            <?= form_open('user/update/'.$this->uri->segment(3)) ?>

            <div class="form-horizontal">

              <div class="form-group">
                <label class="col-sm-2 control-label">Username</label>
                <div class="col-sm-10">
                  <input type="text" class="form-control" name="username" value="<?= $user->username ?>" placeholder="exp. erisdsr">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Email</label>
                <div class="col-sm-10">
                  <input type="email" class="form-control" name="email" value="<?= $user->email ?>" placeholder="exp. erisdsr@elearning.com">
                </div>
              </div>

              <div class="form-group">
                <label class="col-sm-2 control-label">Verify</label>
                <div class="col-sm-10">
                  <select class="form-control" name="verify">
                    
                    <?php if($user->verify == 0){ ?>
                    
                    <option value="0" selected="">No</option>
                    <option value="1">Yes</option>
                    
                    <?php } else { ?>

                    <option value="0">No</option>
                    <option value="1" selected="">Yes</option>
                    
                    <?php } ?>
                  </select>
                </div>
              </div>
            
              <input type="hidden" name="level" value="1">

              <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                  <input type="submit" class="btn btn-rect btn-success" name="save" value="Save Change">
                  <a href="<?= base_url('user') ?>" class="btn btn-rect btn-danger">Cancel</a>
                </div>
              </div>

            </div>

            <?= form_close() ?>

          </div>
        </div>
      </div>
    </div>


    <div class="box">
      <header>
        <h4><i class="fa fa-lock"></i> Change Password</h4>
        <div class="box-tools">
          <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
          <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
          <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
        </div>
      </header>
      <div class="box-body">

        <?= form_open('user/update_password/'.$this->uri->segment(3)) ?>

        <div class="form-horizontal">

          <input type="hidden" name="username" value="<?= $this->uri->segment(3) ?>">

          <div class="form-group">
            <label class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" name="password" placeholder="New password">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label">Retype Password</label>
            <div class="col-sm-10">
              <input type="password" class="form-control" name="retype_password" placeholder="Retype a new password">
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"></label>
            <div class="col-sm-10">
              <input type="submit" class="btn btn-rect btn-success" name="save" value="Save Change">
              <a href="<?= base_url('user') ?>" class="btn btn-rect btn-danger">Cancel</a>
            </div>
          </div>

        </div>

        <?= form_close() ?>

      </div>
    </div>

  </div>
  <!-- END: .container-fluid -->
</div>
<!-- END: .main-content -->

