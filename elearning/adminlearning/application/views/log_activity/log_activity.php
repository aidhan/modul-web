        <!-- begin .app-main -->
        <div class="app-main">
          <!-- begin .main-heading -->
          <header class="main-heading shadow-2dp">
            <!-- begin dashhead -->
            <div class="dashhead bg-white">
              <div class="dashhead-titles">
                <h1 class="dashhead-title"><?= $title_page ?></h1>
              </div>

              <div class="dashhead-toolbar">
                <div class="dashhead-toolbar-item">
                  <a href="<?= base_url() ?>">Dashboard</a>
                  / <?= $title_page ?>
                </div>
              </div>
            </div>
            <!-- END: dashhead -->
          </header>
          <!-- END: .main-heading -->

          <!-- begin .main-content -->
          <div class="main-content bg-clouds">

            <!-- begin .container-fluid -->
            <div class="container-fluid p-t-15">

            <?php if($this->session->flashdata('info')){ ?>

            <div class="alert alert-success"><?= $this->session->flashdata('info'); ?></div>

            <?php } ?>

              <div class="row">
                <div class="col-sm-12">
                  <div class="box">
                    <header>
                      <h4><i class="fa fa-table"></i> Data <?= $title_page ?></h4>
                      <div class="box-tools">
                        <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                        <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
                        <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
                      </div>
                    </header>

                    <div class="box-body">
          
                      <?php if($this->mlog_activity->count()->total > 0){ ?>
                      
                      <div class="form-group">
                        <a href="<?= base_url('log_activity/delete_all') ?>" onclick="return confirm('Are you sure to delete all data?')" class="btn btn-rect btn-danger"><i class="fa fa-trash"></i> Delete All History</a>
                      </div>

                      <?php } ?>
                      
                      <div class="table-responsive">
                        <table data-plugin="datatables" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>No</th>
                              <th>Username</th>
                              <th>Activity</th>
                              <th>Timestamp</th>
                            </tr>
                          </thead>
                          <tbody>

                            <?php $no = 0; foreach($log_activity as $row){ $no++; ?>

                            <tr>
                              <td><?= $no ?></td>
                              <td><?= $row->username ?></td>
                              <td><?= $row->action ?></td>
                              <td><?= $row->timestamp ?></td>
                            </tr>

                            <?php } ?>
                          
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- END: .container-fluid -->
          </div>
          <!-- END: .main-content -->

