        <!-- begin .app-main -->
        <div class="app-main">
          <!-- begin .main-heading -->
          <header class="main-heading shadow-2dp">
            <!-- begin dashhead -->
            <div class="dashhead bg-white">
              <div class="dashhead-titles">
                <h1 class="dashhead-title"><?= $title_page ?></h1>
              </div>

              <div class="dashhead-toolbar">
                <div class="dashhead-toolbar-item">
                  <a href="<?= base_url() ?>">Dashboard</a> 
                  / <a href="<?= base_url('student') ?>">Student</a>
                  / <?= $title_page ?>
                </div>
              </div>
            </div>
            <!-- END: dashhead -->
          </header>
          <!-- END: .main-heading -->

          <!-- begin .main-content -->
          <div class="main-content bg-clouds">

            <!-- begin .container-fluid -->
            <div class="container-fluid p-t-15">

              <div class="row">
                <div class="col-sm-12">
                  <div class="box">
                    <header>
                      <h4><i class="fa fa-address-card-o"></i> Data <?= $title_page ?></h4>
                      <div class="box-tools">
                        <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                        <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a>
                        <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
                      </div>
                    </header>

                    <div class="box-body">

                      <div class="row">
                        <div class="col-md-10">
                          <div class="table-responsive">
                            <table class="table">
                              <tbody>
                                <tr>
                                  <td>NIM</td>
                                  <td width="1%">:</td>
                                  <td><?= $student->nim ?></td>
                                </tr>
                                <tr>
                                  <td>Name</td>
                                  <td>:</td>
                                  <td><?= $student->first_name." ".$student->last_name ?></td>
                                </tr>
                                <tr>
                                  <td>Address</td>
                                  <td>:</td>
                                  <td><?= $student->address ?></td>
                                </tr>
                                <tr>
                                  <td>Phone</td>
                                  <td>:</td>
                                  <td><?= $student->phone ?></td>
                                </tr>
                                <tr>
                                  <td>Mail</td>
                                  <td>:</td>
                                  <td><?= $user->email ?></td>
                                </tr>
                                <tr>
                                  <td>Username</td>
                                  <td>:</td>
                                  <td><?= $student->username ?></td>
                                </tr>
                                <tr>
                                  <td>Member Since</td>
                                  <td>:</td>
                                  <td><?= $student->timestamp ?></td>
                                </tr>
                                <tr>
                                  <td>Verify Account</td>
                                  <td>:</td>
                                  <td><?= $user->verify == 1? 'Yes' : 'No' ?></td>
                                </tr>
                              </tbody>
                            </table>
                          </div>
                        </div>
                        <div class="col-md-2">
                          <img src="<?= base_url('assets/images/student/'.$student->image) ?>" class="img-responsive thumbnail">
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>

            </div>
            <!-- END: .container-fluid -->
          </div>
          <!-- END: .main-content -->

