<!-- begin .app-main -->
<div class="app-main">
  <!-- begin .main-heading -->
  <header class="main-heading shadow-2dp">
    <!-- begin dashhead -->
    <div class="dashhead bg-white">
      <div class="dashhead-titles">
        <h1 class="dashhead-title"><?= $title_page ?></h1>
      </div>

      <div class="dashhead-toolbar">
        <div class="dashhead-toolbar-item">
          <a href="<?= base_url() ?>">Dashboard</a>
          / <a href="<?= base_url('quiz') ?>">Quiz</a>
          / <?= $title_page ?>
        </div>
      </div>
    </div>
    <!-- END: dashhead -->
  </header>
  <!-- END: .main-heading -->

  <!-- begin .main-content -->
  <div class="main-content bg-clouds">

    <!-- begin .container-fluid -->
    <div class="container-fluid p-t-15">

      <div class="row">
        <div class="col-sm-12">
          <div class="box">
            <header>
              <h4><i class="fa fa-edit"></i> <?= $title_page ?></h4>
              <div class="box-tools">
                <a class="fa fa-fw fa-minus" href="#" data-box="collapse"></a>
                <a class="fa fa-fw fa-square-o" href="#" data-fullscreen="box"></a> 
                <a class="fa fa-fw fa-refresh" href="#" data-box="refresh"></a>
              </div>
            </header>

            <div class="box-body">

            <?= form_open('quiz/update') ?>

              <div class="form-horizontal">

                <div class="form-group">
                  <label class="col-sm-2 control-label">Question</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="question"><?= $quiz->question ?></textarea>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label">Classes</label>
                  <div class="col-sm-10">
                    <select class="form-control" name="id_classes">
                      <option value="">- Select a Classes -</option>

                      <?php foreach ($classes as $row) { ?>

                      <?php if($row->id == $quiz->id_classes){ ?>
                      <option value="<?= $row->id ?>" selected><?= $row->class_name ?></option>
                      <?php } else { ?>
                      <option value="<?= $row->id ?>"><?= $row->class_name ?></option>

                      <?php } } ?>
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="col-sm-2 control-label"></label>
                  <div class="col-sm-10">
                    <input type="submit" name="save" class="btn btn-rect btn-success" value="Save Change">
                  </div>
                </div>

              </div>

            <?= form_close(); ?>


            </div>
          </div>
        </div>
      </div>

    </div>
    <!-- END: .container-fluid -->
  </div>
  <!-- END: .main-content -->

