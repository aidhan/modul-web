<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('muser');

		if($this->session->username != null){
			redirect(base_url('home'));
		}
	}

	public function index(){
		$data = array(
			'page' => 'login.php',
			'title_page' => 'Login Kulinera'
		);

		$this->load->view('login', $data);
	} 

	public function validate(){
		if($this->input->post('submit')){
			$username = $this->input->post('username');
			$password = md5($this->input->post('password'));
			$login = $this->muser->login($username, $password);

			if($login){
				if($login->username == $username && $login->password == $password && ($login->level == 0 || $login->level == 1)){
					$dataLogin = array(
						'id' => $login->id,
						'username' => $login->username,
						'level' => $login->level
					);
					$this->muser->setOnline($login->id, 1);
					$this->session->set_userdata($dataLogin);
					redirect(base_url('home'));
				} 
				else {
					$this->session->set_flashdata('info', 'You do not have access to this page!');
					redirect(base_url('login'));					
				}
			} else {
				$this->session->set_flashdata('info', 'Username or Password invalid!');
				redirect(base_url('login'));
			}
		}
	}
}
	