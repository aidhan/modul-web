<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Comment extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mcomment');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$data = array(
			'page' => 'comment/comment.php',
			'title_page' => 'Comment',
			'comment' => $this->mcomment->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function publish(){
		if($this->mcomment->updateData($this->uri->segment(3), 1)){
			$this->session->set_flashdata('info', 'Comment has been Published!');
			$this->mlog_activity->insertData('Publish a Comment');
			redirect(base_url('comment'));
		} 
	}

	public function unpublish(){
		if($this->mcomment->updateData($this->uri->segment(3), 0)){
			$this->session->set_flashdata('info', 'Comment has been Unpublished!');
			$this->mlog_activity->insertData('Unpublish a Comment');
			redirect(base_url('comment'));
		} 
	}
}
	