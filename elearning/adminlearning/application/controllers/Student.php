<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array('mstudent','muser','mlog_activity'));

		if($this->session->username == null){
			redirect(base_url('login'));
		} 
	}

	public function index(){
		$data = array(
			'page' => 'student/student.php',
			'title_page' => 'Student',
			'student' => $this->mstudent->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function add(){
		$data = array(
			'page' => 'student/add.php',
			'title_page' => 'Add Student'
		);

		$this->load->view('includes/template', $data);
	}

	public function edit(){
		$data = array(
			'page' => 'student/edit.php',
			'title_page' => 'Edit Student',
			'student' => $this->mstudent->getId($this->uri->segment(3)),
			'user' => $this->muser->getId($this->uri->segment(3))
		);

		$this->load->view('includes/template', $data);
	}

	public function details(){
		$data = array(
			'page' => 'student/details.php',
			'title_page' => 'Detail Student',
			'student' => $this->mstudent->getId($this->uri->segment(3)),
			'user' => $this->muser->getId($this->uri->segment(3))
		);

		$this->load->view('includes/template', $data);
	}

	public function insert(){
		$config['upload_path'] = 'assets/images/student/';
        $config['allowed_types'] = 'jpg|png';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('image')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->mstudent->insertData($file_data['file_name']);
			$this->muser->insertData();
			$this->session->set_flashdata('info', 'Data inserted!');
			$this->mlog_activity->insertData('Insert Data Student');
			redirect(base_url('student'));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'student/add.php',
				'title_page' => 'Add student'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload photo');
			redirect(base_url('student/add'));
		}
	}

	public function update_data(){
		$username = $this->input->post('username');
		$this->mstudent->updateData($username); 
		$this->muser->updateData($username);
		$this->session->set_flashdata('info','Data has been updated!');
		$this->mlog_activity->insertData('Update Data Student');
		redirect(base_url('student/edit/'.$this->uri->segment(3)));
	}

	public function update_password(){
		$username = $this->input->post('username');
		if($this->input->post('password') == $this->input->post('retype_password')){
			$this->muser->updatePassword($username);
			$this->session->set_flashdata('info','Password has been updated!');
			$this->mlog_activity->insertData('Update Password Student');
			redirect(base_url('student/edit/'.$this->uri->segment(3)));
		} else {
			$this->session->set_flashdata('error', 'Wrong password combination! Please retype password same as new password');
			redirect(base_url('student/edit/'.$this->uri->segment(3)));
		}
	}

	public function update_profile_photo(){
		$username = $this->input->post('username');
		
		$uriSegment = $this->uri->segment(3);
		$row = $this->db->where('username', $username)->get('student')->row();
		$path = 'assets/images/student/'.$row->image;
		
		$this->load->helper("file");
		unlink($path);

		$config['upload_path'] = 'assets/images/student/';
        $config['allowed_types'] = 'jpg|png';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('image')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->mstudent->updateProfilePhoto($username, $file_data['file_name']);
			$this->session->set_flashdata('info','Profile photo has been updated!');
			$this->mlog_activity->insertData('Update Profile Photo Student');
			redirect(base_url('student/edit/'.$this->uri->segment(3)));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'student/edit.php',
				'title_page' => 'Edit Student'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload photo');
			redirect(base_url('student/edit/'.$this->uri->segment(3)));
		}
	}

	public function delete(){		
		$uriSegment = $this->uri->segment(3);
		$row = $this->db->where('username', $uriSegment)->get('student')->row();
		if(unlink('assets/images/student/'.$row->image)){
			$this->mstudent->deleteData($uriSegment);
			$this->muser->deleteData($uriSegment);
			$this->session->set_flashdata('info','Data has been deleted!');
			$this->mlog_activity->insertData('Delete Data Student');
			redirect(base_url('student')); 
		}
	}
}
	