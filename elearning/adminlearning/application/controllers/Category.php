<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mcategory');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$data = array(
			'page' => 'category/category.php',
			'title_page' => 'Category',
			'category' => $this->mcategory->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function add(){
		$this->mcategory->insertData();
		$this->session->set_flashdata('info', 'Data inserted!');
		$this->mlog_activity->insertData('Insert Data Category');
		redirect(base_url('category'));
	}

	public function delete(){
		$uriSegment = $this->uri->segment(3);
		if($this->mcategory->deleteData($uriSegment)){
			$this->session->set_flashdata('info','Data has been deleted!');
			$this->mlog_activity->insertData('Delete Data Category');
		} 
		redirect(base_url('category'));
	}

	public function edit(){
		$uriSegment = $this->uri->segment(3);
		if($this->mcategory->getId($uriSegment)){
			$data = array(
				'page' => 'category/edit.php',
				'title_page' => 'Edit Category',
				'category' => $this->mcategory->getId($uriSegment)
			);
			$this->load->view('includes/template', $data);
		}
	}

	public function update(){
		$id = $this->input->post('id');
		if($this->mcategory->updateData($id)){
			$this->session->set_flashdata('info', 'Data has been updated!');
			$this->mlog_activity->insertData('Update Data Category');
			redirect(base_url('category'));
		} 
	}
}
	