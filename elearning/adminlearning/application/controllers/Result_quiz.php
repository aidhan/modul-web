<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Result_quiz extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mclasses');
		$this->load->model('mresult_quiz');
		$this->load->model('mstudent');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){	
		error_reporting(0);
		$data = array(
			'page' => 'result_quiz/result_quiz.php',
			'title_page' => 'Result Quiz',
			'result_quiz' => $this->mresult_quiz->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function delete_all(){
		if($this->mresult_quiz->deleteAll()){
			$this->session->set_flashdata('info', 'All data deleted!');
			$this->mlog_activity->insertData('Delete All Result Quiz');
			redirect(base_url('result_quiz'));
		}
	}

	public function export(){ 

	    include APPPATH.'third_party/PHPExcel.php';   	
    	$excel = new PHPExcel();
		$excel->getProperties()->setCreator('Kulinera')
		             ->setLastModifiedBy('Kulinera')
		             ->setTitle("Result Quiz")
		             ->setSubject("Result Quiz")
		             ->setDescription("Result Quiz")
		             ->setKeywords("Result Quiz");

		$style_col = array(
		  'font' => array('bold' => true), 
		  'alignment' => array(
		    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
		    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
		  ),
		  'borders' => array(
		    'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
		    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  
		    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
		    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
		  )
		);

		$style_row = array(
		  'alignment' => array(
		    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
		  ),
		  'borders' => array(
		    'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
		    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
		    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
		    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
		  )
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "RESULT QUIZ"); 
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); 
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); 

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); 
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "NIM"); 
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "NAME"); 
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "QUESTION"); 
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "ANSWER"); 
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "SCORE"); 
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "CLASSES"); 
		$excel->setActiveSheetIndex(0)->setCellValue('H3', "TIMESTAMP"); 

		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('H3')->applyFromArray($style_col);

		$excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		$excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
		$excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);

		$query = $this->mresult_quiz->getData()->result();
		$no = 1; 
		$numrow = 4; 
		foreach($query as $row){ 

            $question = $this->db->get_where('quiz', array('id' => $row->id_quiz))->row(); 
            $classes = $this->db->get_where('classes', array('id' => $row->id_classes))->row(); 
            $student = $this->db->get_where('student', array('id' => $row->id_student))->row(); 

		  	$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		  	$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $student->nim);
		  	$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $student->first_name." ".$student->last_name);
		  	$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $question->question);
		  	$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $row->answer);  
		  	$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $row->score);  
		  	$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $classes->class_name);  
		  	$excel->setActiveSheetIndex(0)->setCellValue('H'.$numrow, $row->timestamp);  
		  
		  	$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('H'.$numrow)->applyFromArray($style_row);
		  
		  	$excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
		  
		  	$no++; 
		  	$numrow++; 
		}

		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); 
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(50); 
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(50); 
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(50); 
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(12); 
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(20); 
		$excel->getActiveSheet()->getColumnDimension('H')->setWidth(20); 

		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

		$excel->getActiveSheet(0)->setTitle("Result Quiz");
		$excel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="[Kulinera]_Result-Quiz.xlsx"'); 
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');

  	}

}
