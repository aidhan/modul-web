<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluation_student extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mclasses');
		$this->load->model('mteacher');
		$this->load->model('mstudent');
		$this->load->model('mresult_assesment');
		$this->load->model('mresult_quiz');
		$this->load->model('mevaluation_student');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		error_reporting(0);		
		$data = array(
			'page' => 'evaluation_student/evaluation_student.php',
			'title_page' => 'Evaluation Student',
			'evaluation_student' => $this->mevaluation_student->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function delete_all(){
		if($this->mevaluation_student->deleteAll()){
			$this->session->set_flashdata('info', 'All data deleted!');
			$this->mlog_activity->insertData('Delete All Evaluation Student');
			redirect(base_url('evaluation_student'));
		}
	}

	public function export(){ 

	    include APPPATH.'third_party/PHPExcel.php';   	
    	$excel = new PHPExcel();
		$excel->getProperties()->setCreator('Kulinera')
		             ->setLastModifiedBy('Kulinera')
		             ->setTitle("Evaluation Student")
		             ->setSubject("Evaluation Student")
		             ->setDescription("Evaluation Student")
		             ->setKeywords("Evaluation Student");

		$style_col = array(
		  'font' => array('bold' => true), 
		  'alignment' => array(
		    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
		    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
		  ),
		  'borders' => array(
		    'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
		    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  
		    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
		    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
		  )
		);

		$style_row = array(
		  'alignment' => array(
		    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
		  ),
		  'borders' => array(
		    'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
		    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
		    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
		    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
		  )
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "EVALUATION STUDENT"); 
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); 
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); 

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); 
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "NIM"); 
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "NAME"); 
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "ASSESMENT"); 
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "QUIZ"); 
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "CLASSES"); 
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "TIMESTAMP"); 

		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);

		$excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		$excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
		$excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);

		$query = $this->mevaluation_student->getData()->result();
		$no = 1; 
		$numrow = 4; 
		foreach($query as $row){ 

	        $classes = $this->db->get_where('classes', array('id' => $row->id_classes))->row(); 
	        $student = $this->db->get_where('student', array('id' => $row->id_student))->row(); 
	        $assesment = $this->db->query("SELECT SUM(score) AS a_score FROM result_assesment WHERE id_student='$row->id_student' AND id_assesment='$row->id_assesment'")->row(); 
	        $quiz = $this->db->query("SELECT SUM(score) AS q_score FROM result_quiz WHERE id_student='$row->id_student' AND id_classes='$row->id_classes'")->row(); 

		  	$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		  	$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $student->nim);
		  	$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $student->first_name." ".$student->last_name);
		  	$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $assesment->a_score);
		  	$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $quiz->q_score);  
		  	$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $classes->class_name);  
		  	$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $row->timestamp);  
		  
		  	$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		  
		  	$excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
		  
		  	$no++; 
		  	$numrow++; 
		}

		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); 
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(50); 
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(15); 
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(12); 
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(12); 
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(20); 

		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

		$excel->getActiveSheet(0)->setTitle("Evaluation Student");
		$excel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="[Kulinera]_Evaluation-Student.xlsx"'); 
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');

  	}

}