<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Classes extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('mclasses');
		$this->load->model('mteacher');
		$this->load->model('mstudent');
		$this->load->model('mprovince');
		$this->load->model('mcategory');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		error_reporting(0);
		$data = array(
			'page' => 'classes/classes.php',
			'title_page' => 'Classes',
			'classes' => $this->mclasses->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function details(){
		$data = array(
			'page' => 'classes/details.php',
			'title_page' => 'Detail Classes',
			'student_list' => $this->mstudent->getIdClasses($this->uri->segment(3))->result(),
			'classes' => $this->mclasses->getId($this->uri->segment(3))
		);

		$this->load->view('includes/template', $data);
	}

	public function add(){
		$data = array(
			'page' => 'classes/add.php',
			'title_page' => 'Add Classes',
			'max_class' => $this->mclasses->maxID(),
			'generate_code' => $this->mclasses->generateCode(),
			'teacher' => $this->mteacher->getData()->result(),
			'student' => $this->mstudent->getData()->result(),
			'province' => $this->mprovince->getData()->result(),
			'category' => $this->mcategory->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function insert(){		
		$config['upload_path'] = 'assets/images/classes/';
        $config['allowed_types'] = 'jpg|png';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('image')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->mclasses->insertData($file_data['file_name']);
			$this->session->set_flashdata('info', 'Data inserted!');
			$this->mlog_activity->insertData('Insert Data Classes');
			redirect(base_url('classes'));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'classes/add.php',
				'title_page' => 'Add Teacher'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload photo');
			redirect(base_url('classes/add'));
		}
	}

	public function edit(){		
		$uriSegment = $this->uri->segment(3);
		$data = array(
			'page' => 'classes/edit.php',
			'title_page' => 'Edit Classes',
			'classes' => $this->mclasses->getId($uriSegment),
			'teacher' => $this->mteacher->getData()->result(),
			'student' => $this->mstudent->getData()->result(),
			'province' => $this->mprovince->getData()->result(),
			'category' => $this->mcategory->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function update_data(){		
		$id = $this->uri->segment(3);
		$this->mclasses->updateData($id);
		$this->session->set_flashdata('info','Data has been updated!');
		$this->mlog_activity->insertData('Update Data Classes');
		redirect(base_url('classes/edit/'.$this->uri->segment(3)));
	}

	public function update_image(){			
		$uriSegment = $this->uri->segment(3);
		$row = $this->db->where('id', $uriSegment)->get('classes')->row();
		$path = 'assets/images/classes/'.$row->image;
		
		$this->load->helper('file');
		unlink($path);

		$config['upload_path'] = 'assets/images/classes/';
        $config['allowed_types'] = 'jpg|png';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('image')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->mclasses->updateImage($uriSegment, $file_data['file_name']);
			$this->session->set_flashdata('info','Profile photo has been updated!');
			$this->mlog_activity->insertData('Update Image Classes');
			redirect(base_url('classes/edit/'.$this->uri->segment(3)));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'classes/edit.php',
				'title_page' => 'Edit Classes'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload photo');
			redirect(base_url('classes/edit/'.$this->uri->segment(3)));
		}
	}

	public function delete(){		
		$uriSegment = $this->uri->segment(3);
		$row = $this->db->where('id', $uriSegment)->get('classes')->row();
		if(unlink('assets/images/classes/'.$row->image)){
			$this->mclasses->deleteData($uriSegment);
			$this->session->set_flashdata('info','Data has been deleted!');
			$this->mlog_activity->insertData('Delete Data Classes');
			redirect(base_url('classes')); 
		}
	}
}
	