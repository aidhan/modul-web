<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct() {
		parent::__construct();

		$this->load->model('mdashboard');
		$this->load->model('muser');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$data = array(
			'page' => 'home/home.php',
			'title_page' => 'Dashboard',
			'count_student' => $this->mdashboard->countStudent()->row(),
			'count_teacher' => $this->mdashboard->countTeacher()->row(),
			'count_category' => $this->mdashboard->countCategory()->row(),
			'count_classes' => $this->mdashboard->countClasses()->row(),
			'user_online' => $this->muser->getUserOnline(),
			'log_activity' => $this->mlog_activity->getDataLimit()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function logout() {
		$this->muser->setOnline($_SESSION['id'], 0);
		unset(
			$_SESSION['id'],
			$_SESSION['username'],
			$_SESSION['level']
		);

		redirect(base_url('login'));
	}
}
	