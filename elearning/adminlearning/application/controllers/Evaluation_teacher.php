<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Evaluation_teacher extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mclasses');
		$this->load->model('mteacher');
		$this->load->model('mstudent');
		$this->load->model('mevaluation_teacher');
		$this->load->model('mlog_activity');
 
		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){	
		error_reporting(0);
		$data = array(
			'page' => 'evaluation_teacher/evaluation_teacher.php',
			'title_page' => 'Evaluation Teacher',
			'evaluation_teacher' => $this->mevaluation_teacher->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function delete_all(){
		if($this->mevaluation_teacher->deleteAll()){
			$this->session->set_flashdata('info', 'All data deleted!');
			$this->mlog_activity->insertData('Delete All Evaluation Teacher');
			redirect(base_url('evaluation_teacher'));
		}
	}

	public function export(){ 

	    include APPPATH.'third_party/PHPExcel.php';   	
    	$excel = new PHPExcel();
		$excel->getProperties()->setCreator('Kulinera')
		             ->setLastModifiedBy('Kulinera')
		             ->setTitle("Evaluation Teacher")
		             ->setSubject("Evaluation Teacher")
		             ->setDescription("Evaluation Teacher")
		             ->setKeywords("Evaluation Teacher");

		$style_col = array(
		  'font' => array('bold' => true), 
		  'alignment' => array(
		    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER, 
		    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER 
		  ),
		  'borders' => array(
		    'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
		    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),  
		    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
		    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
		  )
		);

		$style_row = array(
		  'alignment' => array(
		    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER
		  ),
		  'borders' => array(
		    'top' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
		    'right' => array('style'  => PHPExcel_Style_Border::BORDER_THIN), 
		    'bottom' => array('style'  => PHPExcel_Style_Border::BORDER_THIN),
		    'left' => array('style'  => PHPExcel_Style_Border::BORDER_THIN) 
		  )
		);
		$excel->setActiveSheetIndex(0)->setCellValue('A1', "EVALUATION TEACHER"); 
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setBold(TRUE); 
		$excel->getActiveSheet()->getStyle('A1')->getFont()->setSize(15); 

		$excel->setActiveSheetIndex(0)->setCellValue('A3', "NO"); 
		$excel->setActiveSheetIndex(0)->setCellValue('B3', "NIDN"); 
		$excel->setActiveSheetIndex(0)->setCellValue('C3', "NAME"); 
		$excel->setActiveSheetIndex(0)->setCellValue('D3', "ASSESMENT"); 
		$excel->setActiveSheetIndex(0)->setCellValue('E3', "QUIZ"); 
		$excel->setActiveSheetIndex(0)->setCellValue('F3', "CLASSES"); 
		$excel->setActiveSheetIndex(0)->setCellValue('G3', "TIMESTAMP"); 

		$excel->getActiveSheet()->getStyle('A3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('B3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('C3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('D3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('E3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('F3')->applyFromArray($style_col);
		$excel->getActiveSheet()->getStyle('G3')->applyFromArray($style_col);

		$excel->getActiveSheet()->getRowDimension('1')->setRowHeight(20);
		$excel->getActiveSheet()->getRowDimension('2')->setRowHeight(20);
		$excel->getActiveSheet()->getRowDimension('3')->setRowHeight(20);

		$query = $this->mevaluation_teacher->getData()->result();
		$no = 1; 
		$numrow = 4; 
		foreach($query as $row){ 

            $classes = $this->db->get_where('classes', array('id' => $row->id_classes))->row(); 
            $teacher = $this->db->get_where('teacher', array('id' => $classes->id_teacher))->row(); 

		  	$excel->setActiveSheetIndex(0)->setCellValue('A'.$numrow, $no);
		  	$excel->setActiveSheetIndex(0)->setCellValue('B'.$numrow, $teacher->nidn);
		  	$excel->setActiveSheetIndex(0)->setCellValue('C'.$numrow, $teacher->first_name." ".$teacher->last_name);
		  	$excel->setActiveSheetIndex(0)->setCellValue('D'.$numrow, $row->rate_module);
		  	$excel->setActiveSheetIndex(0)->setCellValue('E'.$numrow, $row->rate_quiz);  
		  	$excel->setActiveSheetIndex(0)->setCellValue('F'.$numrow, $classes->class_name);  
		  	$excel->setActiveSheetIndex(0)->setCellValue('G'.$numrow, $row->timestamp);  
		  
		  	$excel->getActiveSheet()->getStyle('A'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('B'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('C'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('D'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('E'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('F'.$numrow)->applyFromArray($style_row);
		  	$excel->getActiveSheet()->getStyle('G'.$numrow)->applyFromArray($style_row);
		  
		  	$excel->getActiveSheet()->getRowDimension($numrow)->setRowHeight(20);
		  
		  	$no++; 
		  	$numrow++; 
		}

		$excel->getActiveSheet()->getColumnDimension('A')->setWidth(5); 
		$excel->getActiveSheet()->getColumnDimension('B')->setWidth(15); 
		$excel->getActiveSheet()->getColumnDimension('C')->setWidth(50); 
		$excel->getActiveSheet()->getColumnDimension('D')->setWidth(15); 
		$excel->getActiveSheet()->getColumnDimension('E')->setWidth(12); 
		$excel->getActiveSheet()->getColumnDimension('F')->setWidth(12); 
		$excel->getActiveSheet()->getColumnDimension('G')->setWidth(20); 

		$excel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

		$excel->getActiveSheet(0)->setTitle("Evaluation Teacher");
		$excel->setActiveSheetIndex(0);

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment; filename="[Kulinera]_Evaluation-Teacher.xlsx"'); 
		header('Cache-Control: max-age=0');
		$write = PHPExcel_IOFactory::createWriter($excel, 'Excel2007');
		$write->save('php://output');

  	}

}
