<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Message extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mmessage');
		$this->load->model('muser');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$data = array(
			'page' => 'message/message.php',
			'title_page' => 'Message',
			'message' => $this->mmessage->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	//CHATTING

	public function chat(){
		$data = array(
			'title_page' => 'Message',
			'chatting' => $this->mmessage->chat($this->session->userdata('username'), $this->uri->segment(3))->result(),
			'users' => $this->mmessage->showMessage($this->session->userdata('username'))->result()
		);

		$this->load->view('message/chatting', $data);
	}

	public function sent(){
		$this->mmessage->insertData($this->session->userdata('username'), $this->uri->segment(3));
		redirect(base_url('message/chat/'.$this->uri->segment(3)));
	}

	//AKHIR DARI CHATTING
}
	