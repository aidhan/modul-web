<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('muser');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		if($this->session->userdata('level') != 0){
			redirect(base_url('home'));
		}

		$data = array(
			'page' => 'user/user.php',
			'title_page' => 'Administrator',
			'user' => $this->muser->getDataAdmin()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function add(){
		if($this->session->userdata('level') != 0){
			redirect(base_url('home'));
		}

		$data = array(
			'page' => 'user/add.php',
			'title_page' => 'Add Administrator'
		);

		$this->load->view('includes/template', $data);
	}

	public function edit(){
		if($this->session->userdata('level') != 0){
			redirect(base_url('home'));
		}

		$data = array(
			'page' => 'user/edit.php',
			'title_page' => 'Edit Administrator',
			'user' => $this->muser->getId($this->uri->segment(3))
		);

		$this->load->view('includes/template', $data);
	}

	public function insert(){
		$this->muser->insertData();
		$this->session->set_flashdata('info', 'Data inserted!');
		$this->mlog_activity->insertData('Insert Data Administrator');
		redirect(base_url('user'));
	}

	public function update(){
		$this->muser->updateUser($this->uri->segment(3));
		$this->session->set_flashdata('info','Data has been updated!');
		$this->mlog_activity->insertData('Update Data Administrator');
		redirect(base_url('user/edit/'.$this->input->post('username')));
	}

	public function update_password(){
		$username = $this->input->post('username');
		if($this->input->post('password') == $this->input->post('retype_password')){
			$this->muser->updatePassword($username);
			$this->session->set_flashdata('info','Password has been updated!');
			$this->mlog_activity->insertData('Update Password Administrator');
			redirect(base_url('user/edit/'.$username));
		} else {
			$this->session->set_flashdata('error', 'Wrong password combination! Please retype password same as new password');
			redirect(base_url('user/edit/'.$this->uri->segment(3)));
		}
	}

	public function delete(){
		$this->muser->deleteData($this->uri->segment(3));
		$this->session->set_flashdata('info','Data has been deleted!');
		$this->mlog_activity->insertData('Delete Data Administrator');
		redirect(base_url('user')); 
	}
}
	