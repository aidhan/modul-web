<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Post extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mpost');
		$this->load->model('muser');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$data = array(
			'page' => 'post/post.php',
			'title_page' => 'Post',
			'post' => $this->mpost->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function publish(){
		if($this->mpost->updateData($this->uri->segment(3), 1)){
			$this->session->set_flashdata('info', 'Post has been Published!');
			$this->mlog_activity->insertData('Publish a Post');
			redirect(base_url('post'));
		} 
	}

	public function unpublish(){
		if($this->mpost->updateData($this->uri->segment(3), 0)){
			$this->session->set_flashdata('info', 'Post has been Unpublished!');
			$this->mlog_activity->insertData('Unpublish a Post');
			redirect(base_url('post'));
		} 
	}
}
	