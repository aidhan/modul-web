<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Module extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mmodule');
		$this->load->model('mclasses'); 
		$this->load->model('mlog_activity'); 

		if($this->session->username == null){
			redirect(base_url('login'));
		} 
	}

	public function index(){
		error_reporting(0);
		$data = array(
			'page' => 'module/module.php',
			'title_page' => 'Module',
			'module' => $this->mmodule->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function add(){
		$data = array(
			'page' => 'module/add.php',
			'title_page' => 'Add Module',
			'classes' => $this->mclasses->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function edit(){
		$data = array(
			'page' => 'module/edit.php',
			'title_page' => 'Edit Module',
			'classes' => $this->mclasses->getData()->result(),
			'module' => $this->mmodule->getId($this->uri->segment(3))
		);

		$this->load->view('includes/template', $data);
	}

	public function details(){
		$data = array(
			'page' => 'module/details.php',
			'title_page' => 'Detail Module',
			'module' => $this->mmodule->getId($this->uri->segment(3))
		);

		$this->load->view('includes/template', $data);
	}

	public function insert(){
		$config['upload_path'] = 'assets/docs/';
        $config['allowed_types'] = 'pdf|mp4';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('docs')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->mmodule->insertData($file_data['file_name'], $file_data['file_ext']);
			$this->session->set_flashdata('info', 'Data inserted!');
			$this->mlog_activity->insertData('Insert Data Module');
			redirect(base_url('module'));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'module/add.php',
				'title_page' => 'Add Module'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload Document');
			redirect(base_url('module/add'));
		}
	}

	public function update_data(){	
		$id = $this->uri->segment(3);
		$this->mmodule->updateData($this->uri->segment(3));
		$this->session->set_flashdata('info','Data has been updated!');
		$this->mlog_activity->insertData('Update Data Module');
		redirect(base_url('module/edit/'.$this->uri->segment(3)));
	}

	public function update_docs(){		
		$id = $this->uri->segment(3);
		$row = $this->db->where('id', $this->uri->segment(3))->get('module')->row();
		$path = 'assets/docs/'.$row->file_name;
		
		$this->load->helper('file');
		unlink($path);

		$config['upload_path'] = 'assets/docs/';
        $config['allowed_types'] = 'doc|docx|pdf|ppt|pptx';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('docs')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->mmodule->updateDocs($this->uri->segment(3), $file_data['file_name'], $file_data['file_ext']);
			$this->session->set_flashdata('info','Document has been updated!');
			$this->mlog_activity->insertData('Update Data Module');
			redirect(base_url('module/edit/'.$this->uri->segment(3)));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'module/edit.php',
				'title_page' => 'Edit Module'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload Document');
			redirect(base_url('module/edit/'.$this->uri->segment(3)));
		}
	}

	public function delete(){		
		$id = $this->uri->segment(3);
		$row = $this->db->where('id', $id)->get('module')->row();
		if(unlink('assets/docs/'.$row->file_name)){
			$this->mmodule->deleteData($id);
			$this->session->set_flashdata('info','Data has been deleted!');
			$this->mlog_activity->insertData('Delete Data Module');
			redirect(base_url('module')); 
		}
	}
}
	