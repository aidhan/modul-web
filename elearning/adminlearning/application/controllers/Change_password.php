<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Change_password extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('muser');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$data = array(
			'page' => 'change_password.php',
			'title_page' => 'Change Password'
		);

		$this->load->view('includes/template', $data);
	}

	public function update(){
		if($this->input->post('password') == $this->input->post('retype_password')){
			$this->muser->updatePassword($this->session->userdata('username'));
			$this->session->set_flashdata('info','Password has been updated!');
			$this->mlog_activity->insertData('Update Password Account');
			redirect(base_url('change_password'));
		} else {
			$this->session->set_flashdata('error', 'Wrong password combination! Please retype password same as new password');
			redirect(base_url('change_password'));
		}
	}
}
	