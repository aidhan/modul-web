<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Province extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mprovince');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		$data = array(
			'page' => 'province/province.php',
			'title_page' => 'Province',
			'province' => $this->mprovince->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function add(){
		if($this->input->post('save')){
			$this->mprovince->insertData();
			$this->session->set_flashdata('info', 'Data inserted!');
			$this->mlog_activity->insertData('Insert Data Province');
			redirect(base_url('province'));
		} else {
			$this->session->set_flashdata('info', 'Failed to insert data!');
			redirect(base_url('province/add'));
		}
	}

	public function delete(){
		$uriSegment = $this->uri->segment(3);
		if($this->mprovince->deleteData($uriSegment)){
			$this->session->set_flashdata('info','Data has been deleted!');
			$this->mlog_activity->insertData('Delete Data Province');
		} 
		redirect(base_url('province'));
	}

	public function edit(){
		$uriSegment = $this->uri->segment(3);
		if($this->mprovince->getId($uriSegment)){
			$data = array(
				'page' => 'province/edit.php',
				'title_page' => 'Edit Province',
				'province' => $this->mprovince->getId($uriSegment)
			);
			$this->load->view('includes/template', $data);
		}
	}

	public function update(){
		$id = $this->input->post('id');
		if($this->mprovince->updateData($id)){
			$this->session->set_flashdata('info', 'Data has been updated!');
			$this->mlog_activity->insertData('Update Data Province');
			redirect(base_url('province'));
		} 
	}
}
	