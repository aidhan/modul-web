<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Quiz extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mquiz');
		$this->load->model('mclasses');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
	}

	public function index(){
		error_reporting(0);
		$data = array(
			'page' => 'quiz/quiz.php',
			'title_page' => 'Quiz',
			'quiz' => $this->mquiz->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function add(){		
		$data = array(
			'page' => 'quiz/add.php',
			'title_page' => 'Add Quiz',
			'classes' => $this->mclasses->getDataClass()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function insert(){
		if($this->input->post('save')){
			$this->mquiz->insertData();
			$this->session->set_flashdata('info', 'Data inserted!');
			$this->mlog_activity->insertData('Insert Data Quiz');
			redirect(base_url('quiz'));
		} else {
			$this->session->set_flashdata('info', 'Failed to insert data!');
			redirect(base_url('quiz/add'));
		}
	}

	public function delete(){
		$uriSegment = $this->uri->segment(3);
		if($this->mquiz->deleteData($uriSegment)){
			$this->session->set_flashdata('info','Data has been deleted!');
			$this->mlog_activity->insertData('Delete Data Quiz');
		} 
		redirect(base_url('quiz'));
	}

	public function edit(){
		$uriSegment = $this->uri->segment(3);
		$data = array(
			'page' => 'quiz/edit.php',
			'title_page' => 'Edit Quiz',
			'quiz' => $this->mquiz->getId($uriSegment),			
			'classes' => $this->mclasses->getDataClass()->result()
		);
		$this->load->view('includes/template', $data);
	}

	public function update(){
		$id = $this->input->post('id');
		if($this->mquiz->updateData($id)){
			$this->session->set_flashdata('info', 'Data has been updated!');
			$this->mlog_activity->insertData('Update Data Quiz');
			redirect(base_url('quiz'));
		} 
	}
}
	