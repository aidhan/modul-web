<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Log_activity extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		}
		
		if($this->session->userdata('level') != 0){
			redirect(base_url('home'));
		}

	}

	public function index(){
		$data = array(
			'page' => 'log_activity/log_activity.php',
			'title_page' => 'Log Activity',
			'log_activity' => $this->mlog_activity->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function delete_all(){
		if($this->mlog_activity->deleteAll()){
			$this->session->set_flashdata('info', 'All data deleted!');
			redirect(base_url('log_activity'));
		}
	}
}
	