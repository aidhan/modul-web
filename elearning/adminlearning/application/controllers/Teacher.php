<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Teacher extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model(array('mteacher','muser','mlog_activity'));

		if($this->session->username == null){
			redirect(base_url('login'));
		} 
	}

	public function index(){
		$data = array(
			'page' => 'teacher/teacher.php',
			'title_page' => 'Teacher',
			'teacher' => $this->mteacher->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function add(){
		$data = array(
			'page' => 'teacher/add.php',
			'title_page' => 'Add Teacher'
		);

		$this->load->view('includes/template', $data);
	}

	public function edit(){
		$data = array(
			'page' => 'teacher/edit.php',
			'title_page' => 'Edit Teacher',
			'teacher' => $this->mteacher->getId($this->uri->segment(3)),
			'user' => $this->muser->getId($this->uri->segment(3))
		);

		$this->load->view('includes/template', $data);
	}

	public function details(){
		$data = array(
			'page' => 'teacher/details.php',
			'title_page' => 'Detail Teacher',
			'teacher' => $this->mteacher->getId($this->uri->segment(3)),
			'user' => $this->muser->getId($this->uri->segment(3))
		);

		$this->load->view('includes/template', $data);
	}

	public function insert(){
		$config['upload_path'] = 'assets/images/teacher/';
        $config['allowed_types'] = 'jpg|png';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('image')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->mteacher->insertData($file_data['file_name']);
			$this->muser->insertData();
			$this->session->set_flashdata('info', 'Data inserted!');
			$this->mlog_activity->insertData('Insert Data Teacher');
			redirect(base_url('teacher'));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'teacher/add.php',
				'title_page' => 'Add Teacher'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload photo');
			redirect(base_url('teacher'));
		}
	}

	public function update_data(){
		$username = $this->input->post('username');
		$this->mteacher->updateData($username);
		$this->muser->updateData($username);
		$this->session->set_flashdata('info','Data has been updated!');
		$this->mlog_activity->insertData('Update Data Teacher');
		redirect(base_url('teacher/edit/'.$this->uri->segment(3)));
	}

	public function update_password(){
		$username = $this->input->post('username');
		if($this->input->post('password') == $this->input->post('retype_password')){
			$this->muser->updatePassword($username);
			$this->session->set_flashdata('info','Password has been updated!');
			$this->mlog_activity->insertData('Update Password Teacher');
			redirect(base_url('teacher/edit/'.$this->uri->segment(3)));
		} else {
			$this->session->set_flashdata('error', 'Wrong password combination! Please retype password same as new password');
			redirect(base_url('teacher/edit/'.$this->uri->segment(3)));
		}
	}

	public function update_profile_photo(){
		$username = $this->input->post('username');
		
		$uriSegment = $this->uri->segment(3);
		$row = $this->db->where('username', $username)->get('teacher')->row();
		$path = 'assets/images/teacher/'.$row->image;
		
		$this->load->helper("file");
		unlink($path);

		$config['upload_path'] = 'assets/images/teacher/';
        $config['allowed_types'] = 'jpg|png';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('image')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->mteacher->updateProfilePhoto($username, $file_data['file_name']);
			$this->session->set_flashdata('info','Profile photo has been updated!');
			$this->mlog_activity->insertData('Update Profile Photo Teacher');
			redirect(base_url('teacher/edit/'.$this->uri->segment(3)));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'teacher/edit.php',
				'title_page' => 'Edit Teacher'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload photo');
			redirect(base_url('teacher/edit/'.$this->uri->segment(3)));
		}
	}

	public function delete(){		
		$uriSegment = $this->uri->segment(3);
		$row = $this->db->where('username', $uriSegment)->get('teacher')->row();
		if(unlink('assets/images/teacher/'.$row->image)){
			$this->mteacher->deleteData($uriSegment);
			$this->muser->deleteData($uriSegment);
			$this->session->set_flashdata('info','Data has been deleted!');
			$this->mlog_activity->insertData('Delete Data Teacher');
			redirect(base_url('teacher')); 
		}
	}
}
	