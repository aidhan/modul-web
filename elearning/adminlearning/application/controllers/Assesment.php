<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Assesment extends CI_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('massesment');
		$this->load->model('mclasses');
		$this->load->model('mlog_activity');

		if($this->session->username == null){
			redirect(base_url('login'));
		} 
	}

	public function index(){
		error_reporting(0);
		$data = array(
			'page' => 'assesment/assesment.php',
			'title_page' => 'Assesment',
			'assesment' => $this->massesment->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function add(){
		$data = array(
			'page' => 'assesment/add.php',
			'title_page' => 'Add Assesment',
			'classes' => $this->mclasses->getData()->result()
		);

		$this->load->view('includes/template', $data);
	}

	public function edit(){
		$data = array(
			'page' => 'assesment/edit.php',
			'title_page' => 'Edit Assesment',
			'classes' => $this->mclasses->getData()->result(),
			'assesment' => $this->massesment->getId($this->uri->segment(3))
		);

		$this->load->view('includes/template', $data);
	}

	public function insert(){
		$config['upload_path'] = 'assets/images/assesment/assesment/';
        $config['allowed_types'] = 'jpg|png';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('image')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->massesment->insertData($file_data['file_name']);
			$this->session->set_flashdata('info', 'Data inserted!');
			$this->mlog_activity->insertData('Insert Data Assesment');
			redirect(base_url('assesment'));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'assesment/add.php',
				'title_page' => 'Add Assesment'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload Image');
			redirect(base_url('assesment/add'));
		}
	}

	public function update_data(){	
		$id = $this->uri->segment(3);
		$this->massesment->updateData($this->uri->segment(3));
		$this->session->set_flashdata('info','Data has been updated!');
		$this->mlog_activity->insertData('Update Data Assesment');
		redirect(base_url('assesment/edit/'.$this->uri->segment(3)));
	}

	public function update_image(){		
		$id = $this->uri->segment(3);
		$row = $this->db->where('id', $this->uri->segment(3))->get('assesment')->row();
		$path = 'assets/images/assesment/assesment/'.$row->image;
		
		$this->load->helper('file');
		unlink($path);

		$config['upload_path'] = 'assets/images/assesment/assesment/';
        $config['allowed_types'] = 'jpg|png';

        $this->load->library('upload', $config);

		if($this->upload->do_upload('image')){
            $data = array('upload_data' => $this->upload->data());
			$file_data = $this->upload->data();
			$this->massesment->updateImage($this->uri->segment(3), $file_data['file_name']);
			$this->session->set_flashdata('info','Image has been updated!');
			$this->mlog_activity->insertData('Update Image Assesment');
			redirect(base_url('assesment/edit/'.$this->uri->segment(3)));
		} else {
			$data = array(
				'error' => $this->upload->display_errors(),
				'page' => 'assesment/edit.php',
				'title_page' => 'Edit Assesment'
			);
			$this->load->view('includes/template', $data);
			$this->session->flashdata('info', 'Error Upload Image');
			redirect(base_url('assesment/edit/'.$this->uri->segment(3)));
		}
	}

	public function delete(){		
		$id = $this->uri->segment(3);
		$row = $this->db->where('id', $id)->get('assesment')->row();
		if(unlink('assets/images/assesment/assesment/'.$row->image)){
			$this->massesment->deleteData($id);
			$this->session->set_flashdata('info','Data has been deleted!');
			$this->mlog_activity->insertData('Delete Data Assesment');
			redirect(base_url('assesment')); 
		}
	}
}
	