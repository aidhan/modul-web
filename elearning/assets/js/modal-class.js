var modal = '';

function getModal(number, region) {
    $.getJSON(defURL + "starter/get_class_province/" + number, function(result){
        modal += '<div class="modal fade bd-example-modal-lg-'+ number +'" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">' +
          '<div class="modal-dialog modal-lg">' +
            '<div class="modal-content">' +
              '<div class="modal-header">' +
                '<h5 class="modal-title">Provinsi ' + region + '</h5>' +
                '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
                  '<span aria-hidden="true">&times;</span>' +
                '</button>' +
              '</div>' +
              '<div class="modal-body">' +
                '<div class="row">';
                  if(result.data.length != 0) {
                    $.each(result.data, function (i, val) {
                      modal += '<div class="col-md-4">' +
                          '<div class="card">' +
                          '<img class="card-img-top" src="'+ defURL +'assets/image/pecel.jpg" height="150px">' +
                          '<div class="card-body text-center">' +
                              '<h5 class="card-title">' + val[1] + '</h5>' + val[5] +
                              // '<a href="' + defURL + 'room/detail?class_code=' + val[2] +'" class="btn btn-primary">Detail</a>' +
                          '</div>' +
                          '</div>' +
                      '</div>';
                    });
                  } else if(result.data.length == 0) {
                    modal += '<div class="col-md-12"><center><h3>Maaf belum ada kelas!</h3></center></div>';
                  } 
                modal += '</div>' +
              '</div>' +
              '<div class="modal-footer">' +
                '<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>' +
              '</div>' +
            '</div>' +
          '</div>' +
        '</div>';
        if(result.data.length != 0) {
          $("#color-status-" + number).css("fill", "#04cc4f");
          $("#color-status-" + number).hover(function(){
            $(this).css("fill", "#1d6a3a");
          }, function() {
            $(this).css("fill", "#04cc4f");
          });
        }

        $("#modal-province").append(modal);
    });
}

if (myLocation == "/elearning/starter/dashboard") {
  for (var index = 0; index < region.length; index++) {
    getModal(index + 1, region[index]);  
  }
}



