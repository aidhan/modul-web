var defURL = 'http://localhost/elearning/';
var myLocation = document.location.pathname;
var region = [
	"DI Aceh",
	"Kalimantan Timur",
	"Jawa Barat",
	"Jawa Tengah",
	"Bengkulu",
	"Banten",
	"DKI Jakarta",
	"Kalimantan Barat",
	"Lampung",
	"Sumatera Selatan",
	"Bali",
	"Jawa Timur",
	"Kalimntan Selatan",
	"Nusa Tenggara Timur",
	"Sulawesi Selatan",
	"Sulawesi Barat",
	"Kepulauan Riau",
	"Gorontalo",
	"Jambi",
	"Kalimantan Tengah",
	"Papua Barat",
	"Sumatera Utara",
	"Riau",
	"Sulawesi Utara",
	"Maluku Utara",
	"Sumatera Barat",
	"DI Yogyakarta",
	"Maluku Selatan",
	"Nusa Tenggara Barat",
	"Sulawesi Tenggara",
	"Sulawesi Tengah",
	"Papua"
];

$(document).ready(function() {

	var count = 0;

	function GetURLParameter(sParam) {
		var sPageURL = window.location.search.substring(1);
		var sURLVariables = sPageURL.split('&');
		
		for (var i = 0; i < sURLVariables.length; i++) {
			var sParameterName = sURLVariables[i].split('=');
			if (sParameterName[0] == sParam) {
				return sParameterName[1];
			}
		}
	}
	/**
	 * Ajax POST and GET function
	 * send and get data from and to databases
	 * 
	 */

	// POST login data
	function loginForm() {
		$('#btn-login').click(function (event) {
			event.preventDefault();
			var formData = {
				'username-login' : $('input[name="username-login"]').val(),
				'password-login' : $.md5($('input[name="password-login"]').val())
			}

			formData = $(this).serialize() + "&" + $.param(formData);

			var request = $.ajax({
				url			: defURL + "Starter/post_login",
				type		: "POST",
				data		: formData,
				async		: true,
				dataTyoe	: "json",
				cache		: false,
				processData	: false
			});

			request.done(function (data, textStatus) { 
				if (data == '2') {
					window.location.replace(defURL + 'starter/dashboard');
				} 
				else if (data == '3') {
					window.location.replace(defURL + 'Teacher');
				}
				else if (data == 'failed') {
					alert('password atau username salah');
				}
			});
			return false;
		});
	}

	// POST data for register
	function registerStd() {
		// student register
		$('#btn-reg-std').click(function (event) {
			event.preventDefault();
			var formData = {
				'first-name-std' : $('input[name="first-name-std"]').val(),
				'last-name-std' : $('input[name="last-name-std"]').val(),
				'username-std' : $('input[name="username-std"]').val(),
				'email-std' : $('input[name="email-std"]').val(),
				'nim-std' : $('input[name="nim-std"]').val(),
				'phone-number-std' : $('input[name="phone-number-std"]').val(),
				'address-std' : $('input[name="address-std"]').val(),
				'user-password-std' : $.md5($('input[name="user-password-std"]').val())
			}
			formData = $(this).serialize() + "&" + $.param(formData);
			var request = $.ajax({
				url			: defURL + "Starter/post_register_std",
				type		: "POST",
				data		: formData,
				async		: true,
				dataType	: "json",
				cache		: false,
				processData	: false
			});
			request.done(function (data, textStatus) { 
				console.log(data);
			});

			return false;
		});
	}

	function registerTch() {
		// teacher register
		$('#btn-reg-tch').click(function (event) {
			event.preventDefault();
			var formData = {
				'first-name-tch' : $('input[name="first-name-tch"]').val(),
				'last-name-tch' : $('input[name="last-name-tch"]').val(),
				'username-tch' : $('input[name="username-tch"]').val(),
				'email-tch' : $('input[name="email-tch"]').val(),
				'nidn-tch' : $('input[name="nidn-tch"]').val(),
				'phone-number-tch' : $('input[name="phone-number-tch"]').val(),
				'address-tch' : $('input[name="address-tch"]').val(),
				'user-password-tch' : $.md5($('input[name="user-password-tch"]').val())
			}
			formData = $(this).serialize() + "&" + $.param(formData);
			var request = $.ajax({
				url			: defURL + "Starter/post_register_tch",
				type		: "POST",
				data		: formData,
				async		: true,
				dataTyoe	: "json",
				cache		: false,
				processData	: false
			});

			request.done(function (data, textStatus) { 
				console.log(data);
			});
			return false;
		});
	}

	// POST new data
	function postData(id, postURL, afterURL) {
		$(id).submit(function(event){
			event.preventDefault();
			var formData = new FormData($(this)[0]);

			var request = $.ajax({
				url 		: defURL + postURL,
				type		: "POST",
				data    	: formData,
				async		: true,
				cache		: false,
				contentType	: false,
				processData	: false
			});

			request.done(function (data, textStatus){
				// Log a message to the console
				swal(data, {
					icon : "success",
				}).then((value) => {
					if(afterURL == "reload") {
						location.reload();
						// console.log(data);
					} else {
						window.location.replace(defURL + afterURL);
					}
				});
				
			});
			return false;
		});
	}

	// GET Data for user
	
	// GET data for profil
	function getProfil() {
		$.getJSON(defURL + "profil/get_data", function(result){
			$('.my-fullname').html(result.data[0][2] + " " + result.data[0][3]);
			$('.my-username').html("@" + result.data[0][4]);
			$('.first-name').html(result.data[0][2]);
			$('.last-name').html(result.data[0][3]);
			$('.username').html(result.data[0][4]);
			$('.address').html(result.data[0][5]);
			$('.nim').html(result.data[0][1]);
			$('.phone').html(result.data[0][6]);
		});
	}

	// Get sidebar data
	function getSideData() {
		$.getJSON(defURL + "profil/get_data", function(result){
			$('.side-fullname').html(result.data[0][2] + " " + result.data[0][3]);
			$('.side-username').html("@" + result.data[0][4]);
		});
	}

	// Get province
	function getProvince() {
		$.getJSON(defURL + "Teacher/getProvince", function(result){
			var item;
			$.each(result.data, function(index, val){
				item += "<option value='" + val[0] + "'>" + val[1] + "</option>";
			})
			$('.province-name').html(item);
		});
	}

	function getCategory() {
		$.getJSON(defURL + "Teacher/getCategory", function(result){
			var item;
			$.each(result.data, function (index, val) {
				item += "<option value='" + val[0] + "'>" + val[1] + "</option>";
			});
			$('.category-name').html(item);
		});
	}

	function getDataTable(id, getURL) {
        $(id).DataTable( {
            "processing": true,
            "ajax": defURL + getURL,
            responsive: true
        });
	}
	
	function getDetailClass(id) {
		$.getJSON(defURL + "room/get_class_detail/" + id, function(result){
			
			$('.class-title-first').html('Kelas ' + result.data[0][1]);
			$('.tutor-name').html('Tutor ' + result.data[0][6]);
			$('.province-name').html(result.data[0][4]);
			$('.category-name').html(result.data[0][5]);
			$('.std-number').html(result.data[0][7]);
			$('.progress-num-class').html('<div class="progress-bar" role="progressbar" style="width: ' + result.data[0][7] + '%;" aria-valuenow="'+result.data[0][7]+'" aria-valuemin="0" aria-valuemax="100"></div>')			
			$('.desc-text').html(result.data[0][2]);
			$('.get-class').html(result.data[0][8]);
		});
	}

	function getClassTeacher() {
		$.getJSON(defURL + "Teacher/getDataClass", function(result){
			var item = "";
			$.each(result.data, function (index, val) {
				item = '<div class="col-md-4">' +
				'<div class="card">' +
						'<img src="'+ defURL + 'assets/image/lontong.jpg" class="card-img-top">' +
						'<div class="card-body">' +
							'<h5 class="card-title">' + val[0] + '</h5>' +
							'<a href="' + defURL + 'Teacher/detailClass?class='+ val[5] + '" class="card-link">Detail</a>' +
							'<a href="' + defURL + '/Teacher/addLesson?code_class=' + val[5] + '&module_name=' + val[0] + '" class="card-link btn btn-sm btn-primary"><i class="fa fa-plus"></i> Materi</a>' +
							'<a href="#" class="card-link btn btn-sm btn-warning"><i class="fa fa-pencil"></i> Materi</a>' +
						'</div>' +
					'</div>' +
				'</div>';
				$('#my-list-class').append(item);
			});
		});
	}

	function getAssesmentResult() {
		var id = GetURLParameter('class_code');
		var user_id = GetURLParameter('user_id');
		$.getJSON(defURL + "Teacher/get_assesment_result/" + id +"/" + user_id, function(result){
			$('#start').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/m3pRqiGBfEU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
			$('#process').append('<iframe width="100%" height="315" src="https://www.youtube.com/embed/H4BB9eGUEaE" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
			$('#finish').append('<iframe width="100%" height="315" src="http://localhost/elearning/adminlearning/assets/images/assesment/task/CL-0005/3/' + result.data[0][2] + '" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>');
		});
	}

	// Lesson componen 
	function getQuiz() {
		var id = GetURLParameter('class_code');
		var item = '';

		$('#info-room').html('<h6>' + GetURLParameter('class_name') + '</h6><i>' + GetURLParameter('class_tutor') + '</i>')
		
		$.getJSON(defURL + "room/get_quiz/" + id, function(result){
			$.each(result.data, function (index, val) {
				item += '<div class="form-group">' +
						'<div class="form-group">' +
						'<label for="uassestment">' + val[1] + '</label>' +
						'<textarea class="form-control" id="quiz-answer" name="answer-'+(index+1)+'" rows="4" required></textarea>'
					'</div>'+
				'</div>';
			});

			$('#quiz-generate').html(item);
		});

		$.getJSON(defURL + "room/get_module/" + id, function(result){
			if(result.data.length > 1) {
				$('#material-data').html('<iframe src="'+defURL+'uploads/'+result.data[0][2]+'" width="100%" height="720px"></iframe>');
				$('#tutorial-video').html('<video width="100%" controls><source src="' + defURL + 'uploads/' + result.data[1][2] + '" type="video/mp4">Your browser does not support HTML5 video.</video>');

			} else if(result.data.length == 1) {
				$('#material-data').html('<iframe src="'+defURL+'uploads/'+result.data[0][2]+'" width="100%" height="720px"></iframe>');
				$('#tutorial-video').html('<center><h3>Maaf tidak ada tutorial!</h3></center>');
			}
		});

		$.getJSON(defURL + "room/get_assesment/" + id, function(result){
			console.log(result.data);
			$('#assesment-content').html(result.data[0][1]);			
		});
	}

	/**
	 * General function
	 * place the function for general using here
	 */

	// Zooming the map

	for (var index = 0; index < region.length; index++) {

		$("#popover-" + (index + 1)).popover({ 
			trigger: "hover",
			html: true,
			content: region[index] 
		});
		
	}

	function zoomMap() {	
		svgPanZoom('.map-container', {
			zoomEnabled: true,
			center: true,
			fit: true,
			controlIconsEnabled: true
		});
	}

	// Do step register form
	function stepRegisterForm() {
		var role;
		$(".teacher-form").hide();
		$(".student-form").hide();
		$('.choose-role').change(function(event){
			role = $(this).val();
			$('.choose-role').hide();
			$('.role-label').hide();

			if(role == 'Teacher') {
				$(".student-form").hide("fast");
				$(".teacher-form").show("slow");
			} else {
				$(".teacher-form").hide("fast");
				$(".student-form").show("slow");
			}
		});
	}

	// Add new question form for quiz lesson
	function addNewKuiz() {
		count++;
		var numItems = $('.txt-area-quiz').length; 
		var htmlElement = 	'<div class="form-group">' +
								'<label for="quiz-' + count + '">Pertanyaan ke-' + count + '</label>' +
								'<textarea name="input-quiz-' + count + '" class="txt-area-quiz form-control" id="quiz-' + count + '" rows="4">' +
							'</textarea></div>';
		console.log(numItems);
		$(".quiz-input").append(htmlElement);
	}

	/** 
	* Do step lesson form
	*/ 
	function stepFormLesson() {
		$("#sf2").hide();
		$("#sf3").hide();
		// Binding next button on first step
		$(".open1").click(function() {
			$(".frm").hide("fast");
			$("#sf2").show("slow");
		});

		// Binding next button on second step
		$(".open2").click(function() {
			$(".frm").hide("fast");
			$("#sf3").show("slow");
		});

		// Binding back button on second step
		$(".back2").click(function() {
			$(".frm").hide("fast");
			$("#sf1").show("slow");
		});

		// Binding back button on third step
		$(".back3").click(function() {
			$(".frm").hide("fast");
			$("#sf2").show("slow");
		});

		$(".open3").click(function() {
			
		});
	}

	function validateFileUpload(id, err1, err2, file_ext) {
		var a = 0;

		$(id).bind('change', function() {
			var ext = $(id).val().split('.').pop().toLowerCase();
			if($.inArray(ext, [file_ext]) == -1){
				$(err1).slideDown('slow');
				$(err2).slideUp('slow');

				a = 0;	
			} else {
				var filesize = (this.files[0].size);
				if (filesize > 1000000) {
					$(err2).slideDown('slow');
					a = 0;
				} else {
					a = 1;
					$(err2).slideUp('slow');
				}
				$(err1).slideUp("slow");
			}
		});
	}

	/**
	 * Function Execution
	 * place to execute function above
	 * 
	 */

	if (myLocation == "/elearning/starter/dashboard") {
		zoomMap();

	} else if (myLocation == "/elearning/starter/login") {
		loginForm();
		registerStd();
		registerTch();
		stepRegisterForm();
		
	} else if(myLocation == "/elearning/profil") {
		getProfil();
	} else if(myLocation == "/elearning/Teacher") {
		getClassTeacher();
	} else if(myLocation == "/elearning/Teacher/addClass") {
		getProvince();
		getCategory();
		getDataTable('#class-table', 'Teacher/getDataClass');
		postData('#form-add-class', 'Teacher/postNewClass', 'Teacher/addClass');
	} else {
		getDetailClass(GetURLParameter('class_code'));
		addNewKuiz();
		$(".add-form-quiz").click(function () {
			addNewKuiz();
		});
		stepFormLesson();
		getQuiz();
		getAssesmentResult();
	}
	
	getDataTable('#class-std-table', 'Teacher/getDataStd/' + GetURLParameter('class'));
	getSideData();
	validateFileUpload('#ufile', '#error1', '#error2', 'pdf');
	validateFileUpload('#uvideo', '#error3', '#error4', 'mp4');
	postData('#lesson-form', 'Teacher/postLesson/' + GetURLParameter('code_class') + '/' + GetURLParameter('module_name'), 'Teacher');
	postData('#quiz-form', 'room/post_result_quiz/' + GetURLParameter('class_code'), 'reload');
	postData('#assesment-form', 'room/post_result_assesment/' + GetURLParameter('class_code'), 'reload');
});

// out function
/**
 * 
 * Update, update, class
 */
function delete_class(id) {
	swal({
		title: "Apakah kamu yakin?",
		text: "Kelas tidak akan dapat dikembalikan setelah dihapus!",
		icon: "warning",
		buttons: true,
		dangerMode: true,
	}).then((willDelete) => {
		if (willDelete) {
			var formData = {'delete' : 'delete ' + id};
			var request = $.ajax({
				url 		: defURL + "Teacher/postDeleteClasses/" + id,
				type		: "POST",
				data    	: formData,
			});
			request.done(function (data, textStatus){
				swal(data, {
					icon: "success",
				}).then((value) => {
					location.reload();
				});
			});
			return false;
		} else {
			swal("Kelas batal dihapus!");
		}
	});
}

function visible_class(id, visibility) {
	swal({
		title: "Apakah kamu yakin?",
		buttons: true,
	}).then((willVisible) => {
		if(willVisible) {
			var request = $.ajax({
				url 		: defURL + "Teacher/postVisibility/" + id + "/" + visibility,
				type		: "POST",
			});
			request.done(function (data, textStatus){
				swal(data, {
					icon : "success",
				}).then((value) => {
					location.reload();
				});
			});
		} else {
			if(visibility == 2) {
				swal("Kelas tidak diaktifkan");
			} else {
				swal("Kelas tidak dinonaktifkan");
			}	
		}
	});
}

function update_class(id) {
	localStorage.idClass = id;
	$.getJSON(defURL + "room/get_class_detail/" + id, function(result){
        $.each(result.data, function (i, val) {
			$("input[name='class_name']").val(val[1]);
			$("textarea[name='description']").val(val[2]);
		});
	})
}

function empty_form_update() {
	$("input[name='class_name']").val("");
	$("textarea[name='description']").val("");
}

$('#form-update-class').submit(function(event){

	var formData = new FormData($(this)[0]);

	var request = $.ajax({
		url 		: defURL + 'Teacher/postUpdateClass/' + localStorage.idClass,
		type		: "POST",
		data    	: formData,
		async		: true,
		cache		: false,
		contentType	: false,
		processData	: false
	});
	request.done(function (data, textStatus){
		swal(data, {
			icon : "success",
		}).then((value) => {
			location.reload();
		});
	});
	return false;
});

/**
 * 
 * Room for student management
 */
function add_student(id, id_user) {
	var formData = {
		'id_student' : id_user,
		'id_classes' : id
	}
	formData = $(this).serialize() + "&" + $.param(formData);
	var request = $.ajax({
		url			: defURL + "room/add_student_class",
		type		: "POST",
		data		: formData,
		async		: true,
		cache		: false,
		processData	: false
	});
	request.done(function (data, textStatus) { 
		console.log(data);
	});
}


