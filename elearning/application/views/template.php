
<?php 
    // Header page includes
    $this->load->view('includes/Header');
    // Navbar page includes
    $this->load->view('includes/Navbar'); 
?>
<!-- Content page includes-->
    <main role="main">
        <div class="container-fluid">
            <div class="row">
                <?php 
                    $this->load->view('includes/Sidebar');
                    $this->load->view($page);
                ?>
            </div>
        </div>
    </main>
<!-- Footer includes-->
    <?php $this->load->view('includes/Footer'); ?>

