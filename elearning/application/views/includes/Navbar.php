<?php 
  if($this->session->level == 2):
?>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Kulinera</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('/starter/dashboard') ?>"><i class="fa fa-home"></i> Beranda <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('/profil') ?>"><i class="fa fa-user"></i> Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('/starter/message') ?>"><i class="fa fa-envelope"></i> Message</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('/starter/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<?php 
  endif;
  if($this->session->level == 3):
?>
<nav class="navbar navbar-expand-md navbar-dark fixed-top bg-primary">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Makindo</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarCollapse">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('/Teacher') ?>"><i class="fa fa-home"></i> Beranda <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('/profil') ?>"><i class="fa fa-user"></i> Profil</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?php echo base_url('/starter/logout') ?>"><i class="fa fa-sign-out"></i> Logout</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<?php 
  endif;
?>