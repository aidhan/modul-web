<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="">

    <title><?= $title_page; ?></title>

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url('assets/css/bootstrap.css') ?>" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= base_url('assets/css/album.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/navbar-top-fixed.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/sticky-footer-navbar.css') ?>" rel="stylesheet">
    <link href="<?= base_url('assets/css/my-style.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/DataTables/datatables.min.css')?>" rel="stylesheet">
    <link href="<?= base_url('assets/DataTables/media/css/dataTables.bootstrap4.min.css')?>" rel="stylesheet">
    
    <!--
    <link href="<?//= base_url('assets/css/datatables.min.css'); ?>" rel="stylesheet">
	<link href="<?//= base_url('assets/css/responsive.dataTables.min.css'); ?>" rel="stylesheet">
	-->

    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  </head>

  <body>

