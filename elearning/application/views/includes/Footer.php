    <footer class="footer">
      <div class="container">
        <p>&copy; 2018. E-Learning</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="<?= base_url('assets/js/jquery-3.3.1.js'); ?>"></script>
    <script src="<?= base_url('assets/js/vendor/popper.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/bootstrap.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/vendor/holder.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/svg-pan-zoom.min.js'); ?>"></script>
    <script src="<?= base_url('assets/js/jquery.md5.js')?>"></script>
    <script src="<?= base_url('assets/DataTables/datatables.min.js')?>"></script>
    <script src="<?= base_url('assets/DataTables/media/js/dataTables.bootstrap4.min.js')?>"></script>
    
    <!--
    <script src="<?//= base_url('assets/js/datatables.min.js'); ?>"></script>
    <script src="<?//= base_url('assets/js/dataTables.responsive.min.js'); ?>"></script>
    -->

    <script src="<?= base_url('assets/js/my-app.js'); ?>"></script>
    <script src="<?= base_url('assets/js/modal-class.js'); ?>"></script>
    <script src="<?= base_url('assets/js/sweetalert.min.js'); ?>"></script>
  </body>
</html>