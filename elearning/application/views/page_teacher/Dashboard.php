<div class="col-md-10 ml-sm-auto my-distance">
    <!-- welcome message -->
    <div class="card text-center border-info" id="my-welcome">
        <div class="card-header">
            <h5>Selamat Datang Tutor</h5>
        </div>
        <div class="card-body text-info">
            <a class="btn btn-primary" href="<?php echo base_url('/Teacher/addClass') ?>"><i class="fa fa-plus"></i> Tambah Kelas</a>
        </div>
    </div>
        <br>
    <div class="card border-info">
        <div class="card-header text-center">
            <h5>Kelas yang dimiliki</h5>
        </div> 
        <div class="card-body">
            <div id="my-list-class" class="row">
                
            </div>
        </div>

        
    </div>
</div>