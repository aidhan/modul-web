<div class="col-md-10 ml-sm-auto my-distance">
    <div class="card border-info">
        <div class="card-header" style="background: #007bff; color: #fff;">
            <i class="fa fa-plus fa-2x"></i><span style="font-size: 25px;"> Tambahkan Materi, Penugasan, dan Kuis</span>
        </div>
        <div class="card-body">
            <form id="lesson-form">
                <!-- ID sf1 -->
                <div id="sf1" class="frm">
                    <fieldset>
                        <legend class="text-center">Step 1 of 3</legend>
                        <div class="form-group">
                            <label class="col-md-6 control-label" for="ufile">Upload file materi</label>
                            <div class="col-md-6">
                                <input type="file" placeholder="Your Name" id="ufile" name="ufile" class="form-control" required>
                            </div>
                        </div>
                        <!-- error code -->
                        <div class="alert alert-warning" role="alert" id="error1" style="display:none;">
                            Format salah! format harus pdf
                        </div>
                        <div class="alert alert-warning" role="alert" id="error2" style="display:none;">
                            Maksimal file berukuran 1 MB
                        </div>
                        
                        <div class="form-group">
                            <label class="col-md-6 control-label" for="uvideo">Upload file tutorial (video)</label>
                            <div class="col-md-6">
                                <input type="file" placeholder="Your Name" id="uvideo" name="uvideo" class="form-control">
                            </div>
                        </div>
                        <!-- error code -->
                        <div class="alert alert-warning" role="alert" id="error3" style="display:none;">
                            Format salah! format harus mp4
                        </div>
                        <div class="alert alert-warning" role="alert" id="error4" style="display:none;">
                            Maksimal file berukuran 20 MB
                        </div>
                        <!-- Navigation -->
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <!-- open1 is given in the class that is binded with the click event -->
                                <button class="btn btn-primary open1" type="button">Next <span class="fa fa-arrow-right"></span></button> 
                            </div>
                        </div>
                    </fieldset>
                </div>
                <!-- ID sf2 -->
                <div id="sf2" class="frm">
                    <fieldset>
                        <legend class="text-center">Step 2 of 3</legend>
                        <div class="form-group">
                            <label for="uassestment">Input Soal Assestment</label>
                            <textarea class="form-control" id="uassestment" name="uassesment" rows="4" required></textarea>
                        </div>
                        <!-- Navigation -->
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <!-- open1 is given in the class that is binded with the click event -->
                                <button class="btn btn-warning back2" type="button"><span class="fa fa-arrow-left"></span> Back</button> 
                                <button class="btn btn-primary open2" type="button">Next <span class="fa fa-arrow-right"></span></button> 
                            </div>
                        </div>
                    </fieldset>
                </div>
                <!-- ID sf3 -->
                <div id="sf3" class="frm">
                    <fieldset>
                        <legend class="text-center">Step 3 of 3</legend>
                        <div class="btn btn-sm btn-primary add-form-quiz"><i class="fa fa-plus"></i>&nbsp;Tambah Soal</div>   
                            <div class="quiz-input"></div>
                        <!-- Navigation -->
                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <!-- open1 is given in the class that is binded with the click event -->
                                <button class="btn btn-warning back3" type="button"><span class="fa fa-arrow-left"></span> Back</button> 
                                <input type="submit" class="btn btn-primary open3" href="#"><span class="fa fa-submit"></span> 
                            </div>
                        </div>
                    </fieldset>
                </div>
            </form>
        </div>
    </div>
</div>