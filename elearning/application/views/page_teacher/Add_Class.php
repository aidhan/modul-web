<div class="col-md-10 ml-sm-auto my-distance">
    <div class="card border-info">
        <div class="card-header">
            <i class="fa fa-plus fa-2x"></i><span style="font-size: 25px;">Kelola Kelas</span>
        </div>
        <div class="card-body">
            <!-- List Table -->
            <div class="card border-info">
                <div class="card-header">
                    <i class="fa fa-list-alt fa-2x"></i><span style="font-size: 25px;">Daftar Kelas</span>
                </div>
                <div class="card-body">
                    <div class="table-responsive-md">
                        <table id="class-table" cellspacing="0" class="display table table-striped">
                            <thead style="background: #007bff; color: #fff;">
                                <tr>
                                    <th>Nama Kelas</th>
                                    <th>Provinsi</th>
                                    <th>Kategori</th>
                                    <th>Aksi</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                </div>
            </div>
                <br>
            <form id="form-add-class">
                <div class="row">
                    <div class="col-md-6">                    
                        <div class="form-group">
                            <label for="class-name">Nama Kelas</label>
                            <input type="text" class="form-control" name="class_name" id="class-name" aria-describedby="nameId" required>
                        </div>
                        <div class="form-group">
                            <label for="province-name">Provinsi</label>
                            <select class="form-control province-name" name="id_province" aria-describedby="provinceId" required>

                            </select>
                        </div>
                        <div class="form-group">
                            <label for="category-name">Kategori</label>
                            <select class="form-control category-name" name="id_category" aria-describedby="categoryId" required>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="class-name">Deskripsi Kelas</label>
                            <textarea type="text" class="form-control" name="description" id="class-description" required></textarea>
                        </div>
                        <div class="form-group">
                            <label for="picture-name">Gambar Kelas</label>
                            <input type="file" class="form-control" name="picture-name" id="picture-name">
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-primary" name="submit-class" id="submit-class" value="Buat Kelas">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
        <br>
    
</div>

<!-- Modal Update -->
<div id="modal-class-update" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <form id="form-update-class"> <!-- Form Start -->
                    <div class="form-group">
                        <label for="class-name">Nama Kelas</label>
                        <input type="text" class="form-control" name="class_name" id="class-name" aria-describedby="nameId" required>
                    </div>
                    <div class="form-group">
                        <label for="province-name">Provinsi</label>
                        <select class="form-control province-name" name="id_province" aria-describedby="provinceId" required>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="category-name">Kategori</label>
                        <select class="form-control category-name" name="id_category" aria-describedby="categoryId" required>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="class-name">Deskripsi Kelas</label>
                        <textarea type="text" class="form-control" name="description" id="class-description" required></textarea>
                    </div>
                
            </div>
            <div class="modal-footer">
                <input type="submit" class="btn btn-primary" value="submit">
                <button type="button" class="btn btn-default" onclick="empty_form_update();" data-dismiss="modal">Close</button>
            </div>
                </form>  <!-- Form End -->
        </div>

    </div>
</div>
