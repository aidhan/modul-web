<div class="col-md-10 ml-sm-auto my-distance">
    <div class="card border-info">
        <div class="card-header">
            <i class="fa fa-list-alt fa-2x"></i><span style="font-size: 25px;">Hasil Assesmen</span>
        </div>
        <div class="card-body">
            <div class="row">
                <div id="start" class="col-md-6">

                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="uassestment">Berikan Komentar</label>
                        <textarea class="form-control" id="uassestment" name="uassesment" rows="4" required></textarea>
                    </div>
                    <!-- Rating Star -->
                    <fieldset class="rating">
                        <input type="radio" id="1-star5" name="rating" value="5" /><label class = "full" for="1-star5" title="Awesome - 5 stars"></label>
                        <input type="radio" id="1-star4half" name="rating" value="4 and a half" /><label class="half" for="1-star4half" title="Pretty good - 4.5 stars"></label>
                        <input type="radio" id="1-star4" name="rating" value="4" /><label class = "full" for="1-star4" title="Pretty good - 4 stars"></label>
                        <input type="radio" id="1-star3half" name="rating" value="3 and a half" /><label class="half" for="1-star3half" title="Meh - 3.5 stars"></label>
                        <input type="radio" id="1-star3" name="rating" value="3" /><label class = "full" for="1-star3" title="Meh - 3 stars"></label>
                        <input type="radio" id="1-star2half" name="rating" value="2 and a half" /><label class="half" for="1-star2half" title="Kinda bad - 2.5 stars"></label>
                        <input type="radio" id="1-star2" name="rating" value="2" /><label class = "full" for="1-star2" title="Kinda bad - 2 stars"></label>
                        <input type="radio" id="1-star1half" name="rating" value="1 and a half" /><label class="half" for="1-star1half" title="Meh - 1.5 stars"></label>
                        <input type="radio" id="1-star1" name="rating" value="1" /><label class = "full" for="1-star1" title="Sucks big time - 1 star"></label>
                        <input type="radio" id="1-starhalf" name="rating" value="half" /><label class="half" for="1-starhalf" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div id="process" class="col-md-6">
                
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="uassestment">Berikan Komentar</label>
                        <textarea class="form-control" id="uassestment" name="uassesment" rows="4" required></textarea>
                    </div>
                    <!-- Rating Star -->
                    <fieldset class="rating">
                        <input type="radio" id="2-star5" name="video_rating_2" value="5" /><label class = "full" for="2-star5" title="Awesome - 5 stars"></label>
                        <input type="radio" id="2-star4half" name="video_rating_2" value="4 and a half" /><label class="half" for="2-star4half" title="Pretty good - 4.5 stars"></label>
                        <input type="radio" id="2-star4" name="video_rating_2" value="4" /><label class = "full" for="2-star4" title="Pretty good - 4 stars"></label>
                        <input type="radio" id="2-star3half" name="video_rating_2" value="3 and a half" /><label class="half" for="2-star3half" title="Meh - 3.5 stars"></label>
                        <input type="radio" id="2-star3" name="video_rating_2" value="3" /><label class = "full" for="2-star3" title="Meh - 3 stars"></label>
                        <input type="radio" id="2-star2half" name="video_rating_2" value="2 and a half" /><label class="half" for="2-star2half" title="Kinda bad - 2.5 stars"></label>
                        <input type="radio" id="2-star2" name="video_rating_2" value="2" /><label class = "full" for="2-star2" title="Kinda bad - 2 stars"></label>
                        <input type="radio" id="2-star1half" name="video_rating_2" value="1 and a half" /><label class="half" for="2-star1half" title="Meh - 1.5 stars"></label>
                        <input type="radio" id="2-star1" name="video_rating_2" value="1" /><label class = "full" for="2-star1" title="Sucks big time - 1 star"></label>
                        <input type="radio" id="2-starhalf" name="video_rating_2" value="half" /><label class="half" for="2-starhalf" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>
                </div>
            </div>
            <div class="row">
                <div id="finish" class="col-md-6">
                
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="uassestment">Berikan Komentar</label>
                        <textarea class="form-control" id="uassestment" name="uassesment" rows="4" required></textarea>
                    </div>
                    <!-- Rating Star -->
                    <fieldset class="rating">
                        <input type="radio" id="3-star5" name="video_rating_3" value="5" /><label class = "full" for="3-star5" title="Awesome - 5 stars"></label>
                        <input type="radio" id="3-star4half" name="video_rating_3" value="4 and a half" /><label class="half" for="3-star4half" title="Pretty good - 4.5 stars"></label>
                        <input type="radio" id="3-star4" name="video_rating_3" value="4" /><label class = "full" for="3-star4" title="Pretty good - 4 stars"></label>
                        <input type="radio" id="3-star3half" name="video_rating_3" value="3 and a half" /><label class="half" for="3-star3half" title="Meh - 3.5 stars"></label>
                        <input type="radio" id="3-star3" name="video_rating_3" value="3" /><label class = "full" for="3-star3" title="Meh - 3 stars"></label>
                        <input type="radio" id="3-star2half" name="video_rating_3" value="2 and a half" /><label class="half" for="3-star2half" title="Kinda bad - 2.5 stars"></label>
                        <input type="radio" id="3-star2" name="video_rating_3" value="2" /><label class = "full" for="3-star2" title="Kinda bad - 2 stars"></label>
                        <input type="radio" id="3-star1half" name="video_rating_3" value="1 and a half" /><label class="half" for="3-star1half" title="Meh - 1.5 stars"></label>
                        <input type="radio" id="3-star1" name="video_rating_3" value="1" /><label class = "full" for="3-star1" title="Sucks big time - 1 star"></label>
                        <input type="radio" id="3-starhalf" name="video_rating_3" value="half" /><label class="half" for="3-starhalf" title="Sucks big time - 0.5 stars"></label>
                    </fieldset>
                </div>
            </div>
        </div>
    </div>

    <div class="card border-info">
        <div class="card-header">
            <i class="fa fa-list-alt fa-2x"></i><span style="font-size: 25px;">Hasil Quiz</span>
        </div>
        <div class="card-body">
            
        </div>
    </div>
</div>