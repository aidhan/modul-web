<?php 
    $this->load->view('includes/Header');
?>
    <div class="container">
        <div class="row my-distance">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="card border-info">
                    <div class="card-body">
                        <div class="text-center">
                            <h3>Makindo</h3>
                        </div>
                        <ul class="nav nav-tabs mb-3" id="pills-tab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="pills-login-tab" data-toggle="pill" href="#pills-login" role="tab" aria-controls="pills-login" aria-selected="true">Login</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="pills-register-tab" data-toggle="pill" href="#pills-register" role="tab" aria-controls="pills-register" aria-selected="false">Daftar</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="pills-tabContent">
                            <div class="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="pills-login-tab">
                                <!-- Login Form -->
                                    <div class="form-group">
                                        <label for="username-login">Username</label>
                                        <input type="text" class="form-control" id="username-login" name="username-login" placeholder="Username kamu">
                                    </div>
                                    <div class="form-group">
                                        <label for="password-login">Password</label>
                                        <input type="password" class="form-control" id="password-login" name="password-login" placeholder="Password">
                                    </div>
                                    <!-- submit button -->
                                    <input type="submit" id="btn-login" class="btn btn-outline-primary" Value="Login">
                                    <a href="<?= base_url(); ?>" class="btn btn-outline-danger">Batal</a>
                            </div>
                            <div class="tab-pane fade" id="pills-register" role="tabpanel" aria-labelledby="pills-register-tab">
                                <!-- Register Form -->
                                <div class="form-group">
                                    <label class="role-label" for="exampleFormControlSelect1">Daftar Sebagai</label>
                                    <select class="form-control choose-role" id="exampleFormControlSelect1">
                                        <option>--Pilih--</option>
                                        <option>Student</option>
                                        <option>Teacher</option>                                            
                                    </select>
                                    <!-- Student Form -->
                                    <div class="student-form">
                                        <div class="form-group">
                                            <label for="first-name-std">Nama Depan</label>
                                            <input type="text" class="form-control" id="first-name-std" name="first-name-std" placeholder="Nama Depan">
                                        </div>
                                        <div class="form-group">
                                            <label for="last-name-std">Nama Belakang</label>
                                            <input type="text" class="form-control" id="last-name-std" name="last-name-std" placeholder="Nama Belakang">
                                        </div>
                                        <div class="form-group">
                                            <label for="username-std">Username</label>
                                            <input type="text" class="form-control" id="username-std" name="username-std" placeholder="Username kamu">
                                        </div>
                                        <div class="form-group">
                                            <label for="email-std">Email</label>
                                            <input type="email" class="form-control" id="email-std" name="email-std" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="nim-std">NIM</label>
                                            <input type="text" class="form-control" id="nim-std" name="nim-std" placeholder="NIM kamu">
                                        </div>
                                        <div class="form-group">
                                            <label for="phone-number-std">Nomor Ponsel</label>
                                            <input type="text" class="form-control" id="phone-number-std" name="phone-number-std" placeholder="Nomor Telepon">
                                        </div>
                                        <div class="form-group">
                                            <label for="address-std">Alamat</label>
                                            <input type="text" class="form-control" id="address-std" name="address-std" placeholder="Alamat kamu">
                                        </div>
                                        <div class="form-group">
                                            <label for="user-password-std">Password</label>
                                            <input type="password" class="form-control" id="user-password-std" name="user-password-std" placeholder="Password">
                                        </div>

                                        <!-- submit button -->
                                        <input type="submit" id="btn-reg-std" class="btn btn-outline-primary" name="submit-std" value="Daftar">
                                        <a href="<?= base_url(); ?>" class="btn btn-outline-danger">Batal</a>
                                    </div>
                                    <!-- Teacher Form -->
                                    <div class="teacher-form">
                                        <div class="form-group">
                                            <label for="first-name-tch">Nama Depan</label>
                                            <input type="text" class="form-control" id="first-name-tch" name="first-name-tch" placeholder="Nama Depan">
                                        </div>
                                        <div class="form-group">
                                            <label for="last-name-tch">Nama Belakang</label>
                                            <input type="text" class="form-control" id="last-name-tch" name="last-name-tch" placeholder="Nama Belakang">
                                        </div>
                                        <div class="form-group">
                                            <label for="username-tch">Username</label>
                                            <input type="text" class="form-control" id="username-tch" name="username-tch" placeholder="Username kamu">
                                        </div>
                                        <div class="form-group">
                                            <label for="email-tch">Email</label>
                                            <input type="email" class="form-control" id="email-tch" name="email-tch" placeholder="Email">
                                        </div>
                                        <div class="form-group">
                                            <label for="nidn-tch">NIDN</label>
                                            <input type="text" class="form-control" id="nidn-tch" name="nidn-tch" placeholder="NIM kamu">
                                        </div>
                                        <div class="form-group">
                                            <label for="phone-number-tch">Nomor Ponsel</label>
                                            <input type="text" class="form-control" id="phone-number-tch" name="phone-number-tch" placeholder="Nomor Telepon">
                                        </div>
                                        <div class="form-group">
                                            <label for="address-tch">Alamat</label>
                                            <input type="text" class="form-control" id="address-tch" name="address-tch" placeholder="Alamat kamu">
                                        </div>
                                        <div class="form-group">
                                            <label for="user-password-tch">Password</label>
                                            <input type="password" class="form-control" id="user-password-tch" name="user-password-tch" placeholder="Password">
                                        </div>

                                        <!-- submit button -->
                                        <input type="submit" id="btn-reg-tch" class="btn btn-outline-primary" name="submit-tch" value="Daftar">
                                        <a href="<?= base_url(); ?>" class="btn btn-outline-danger">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </div>
<?php 
    $this->load->view('includes/Footer')
?>