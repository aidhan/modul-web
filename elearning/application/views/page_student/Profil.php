<div class="col-md-10 ml-sm-auto my-distance">
    <div class="jumbotron" id="my-profil">
        <h1 class="display-2 my-fullname">Nama</h1>
        <p class="lead my-username">Username</p>
    </div>
      <br>
     <!-- Tabs -->
    <ul class="nav nav-pills mb-3" id="myTab" role="tablist">
      <li class="nav-item">
        <a class="nav-link active" id="pdf-tab" data-toggle="tab" href="#pdf" role="tab" aria-controls="pdf" aria-selected="true">Information</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" id="tutorial-tab" data-toggle="tab" href="#tutorial" role="tab" aria-controls="tutorial" aria-selected="false">Photo Profil</a>
      </li>
    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="pdf" role="tabpanel" aria-labelledby="pdf-tab">
          <div class="card bg-light border-info">
            <div class="card-body">
              <h2>My Information</h2>
                <div class="row">
                  <div class="col-md-6">
                    <table class="table">
                      <tr>
                        <th>First Name</th>
                        <td class="first-name">MyFirst Name</td>
                      </tr>
                      <tr>
                        <th>Last Name</th>
                        <td class="last-name">MyLast Name</td>
                      </tr>
                      <tr>
                        <th>Username</th>
                        <td class="username">MyUsername Name</td>
                      </tr>
                      
                    </table>
                    
                  </div>
                  <div class="col-md-6">
                    <table class="table">
                      <tr>
                        <tr>
                          <th>Address</th>
                          <td class="address">street</td>
                        </tr>
                        <tr>
                          <th>NIM</th>
                          <td class="nim">140533605655</td>
                        </tr>
                        <tr>
                          <th>Phone</th>
                          <td class="phone">phone</td>
                        </tr>
                      </tr>
                    </table>
                  </div>
                </div>
                <br>
                  <br>
                    <br>
                      <br>
                        <br>
                          <br>
              </div>
              <div class="card-footer text-center">
                <a name="" id="" class="btn btn-primary" href="#" role="button">Edit Information</a>
              </div>
           </div>
        </div>
        <div class="tab-pane fade" id="tutorial" role="tabpanel" aria-labelledby="tutorial-tab">
          <div class="card bg-light border-info">
            <div class="card-body">
              <h2>My Photo</h2>
                <center>
                  <img src="<?= base_url('assets/image/user.jpg'); ?>" alt="">                  
                </center>
                <br>
                  <br>
                    <br>
              </div>
              <div class="card-footer text-center">
                <a name="" id="" class="btn btn-primary" href="#" role="button">Change Photo</a>
              </div>
           </div>
          </div>
        </div>
    </div>
</div>
