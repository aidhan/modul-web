<div class="col-md-10 ml-sm-auto my-distance">
    <div class="card text-center border-info">
        <h5 class="card-header class-title-first"></h5>
        <div class="card-body">
            <div class="row">
                <div class="col-md-4">
                    <div class="card">
                        <img src="<?= base_url('assets/image/pecel.jpg');?>" alt="Card image cap" width="100%">
                        <div class="card-body">
                            <p class="card-text desc-text"></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h2 class="tutor-name"></h2>
                            <i>Provinsi: <span class="province-name"></span> <br>
                            <i>Kategori: <span class="category-name"></span></i>
                            </div>
                            <div class="card-body">
                                <i class="fa fa-users fa-4x"><span class="std-number"></span>/20</i>
                                    <br>
                                        <br>
                                <div class="progress progress-num-class">
                                    
                                </div>
                            </div>
                            <div class="card-footer">
                                <div class="alert alert-success" role="alert">
                                    kamu sudah bergabung di kelas ini
                                </div>
                                <div class="row">
                                    <div class="col-md-6 get-class">
                                    
                                    </div>
                                    <div class="col-md-6">
                                        <a class="btn btn-danger btn-lg btn-block" href="<?= base_url();?>">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>