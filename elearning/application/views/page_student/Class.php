<div class="col-md-10 ml-sm-auto my-distance">
    <div class="card border-info">
        <div class="card-body">
            <div class="row">
                <div class="col-md-1">
                    <img src="<?= base_url('assets/image/Chef.ico'); ?>" width="95">
                </div>
                <div class="col-md-11">
                    <h3>Selamat Belajar !</h3>
                    <div id="info-room">

                    </div>
                </div>
            </div>
        </div>
    </div>
        <br>
    <!-- Room Menu Tabs -->
    <ul class="nav nav-pills" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="reading-tab" data-toggle="tab" href="#reading" role="tab" aria-controls="reading" aria-selected="true">Materi</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="tutorial-tab" data-toggle="tab" href="#tutorial" role="tab" aria-controls="tutorial" aria-selected="false">Tutorial</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="assesment-tab" data-toggle="tab" href="#assesment" role="tab" aria-controls="assesment" aria-selected="false">Assesmen</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="quiz-tab" data-toggle="tab" href="#quiz" role="tab" aria-controls="quiz" aria-selected="false">Kuis</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="result-tab" data-toggle="tab" href="#result" role="tab" aria-controls="result" aria-selected="false">Hasil</a>
        </li>
    </ul>

    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="reading" role="tabpanel" aria-labelledby="reading-tab">
            <div class="card border-info">
                <div class="card-header">
                    <h5>Membaca membuka wawasan!</h5>
                </div>
                <div id="material-data" class="card-body">
                    
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="tutorial" role="tabpanel" aria-labelledby="tutorial-tab">
            <div class="card border-info">
                <div class="card-header">
                    <h5>Perhatikan Tutorial di bawah ini!</h5>
                </div>
                <div id="tutorial-video" class="card-body text-center">

                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="assesment" role="tabpanel" aria-labelledby="assesment-tab">
            <div class="card border-info">
                <div class="card-header">
                    <h5>Kerjakan assetment dengan mengupload video hasil pekerjaanmu!</h5>
                </div>
                <div class="card-body">
                    <div class="alert alert-success" role="alert">
                        <div class="alert alert-warning" role="alert" id="assesment-content">
                        </div>
                        Upload proses persiapan, pembuatan, dan penyajian dalam video. Masing-masing video berukuran maksimal 20 MB.
                    </div>
                    <form id="assesment-form">
                        <fieldset>
                            <div class="form-group">
                                <label class="col-md-6 control-label" for="ufile">Upload Hasil Persiapan!</label>
                                <div class="col-md-6">
                                    <input type="file" placeholder="Your Name" id="ufile" name="ufile-1" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-6 control-label" for="ufile">Upload Hasil Proses Pembuatan!</label>
                                <div class="col-md-6">
                                    <input type="file" placeholder="Your Name" id="ufile" name="ufile-2" class="form-control" required>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-6 control-label" for="ufile">Upload Hasil Penyajian!</label>
                                <div class="col-md-6">
                                    <input type="file" placeholder="Your Name" id="ufile" name="ufile-3" class="form-control" required>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-10 col-md-offset-2">
                                    <!-- open1 is given in the class that is binded with the click event -->
                                    <input type="submit" class="btn btn-primary open3" href="#"><span class="fa fa-submit"></span> 
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="tab-pane fade" id="quiz" role="tabpanel" aria-labelledby="quiz-tab">
            <div class="card border-info">
                <div class="card-header">
                    <h5>Jawablah quiz dibawah ini!</h5>
                </div>
                <div class="card-body">
                    <form id="quiz-form">
                        <div id="quiz-generate">

                        </div>

                        <div class="form-group">
                            <div class="col-md-10 col-md-offset-2">
                                <!-- open1 is given in the class that is binded with the click event -->
                                <input type="submit" class="btn btn-primary open3" href="#"><span class="fa fa-submit"></span> 
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="result" role="tabpanel" aria-labelledby="result-tab">
            <div class="card border-info">
                <div class="card-header">
                    <h5>Hasil belajar</h5>
                </div>
                <div class="card-body">
                </div>
            </div>
        </div>

    </div>
</div>
