<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class Starter extends CI_Controller {

        public $data;
        public $response;
        public $fetch_data;

        /**
         * Routing Zone
         * 
         */
    
        public function __construct() {
            parent::__construct();
            error_reporting(0);
        }
    
        public function index() {
            $this->mlogin->page_direct_dash();
			$this->data = array(
				'title_page' => 'Selamat Datang'
			);

			$this->load->view('home', $this->data);
        }

        public function dashboard() {
            $this->mlogin->page_direct_log($this->session->level, 'starter/login');
            $this->data = array(
				'page'       => 'page_student/Home',
				'title_page' => 'Selamat Datang'
			);

			$this->load->view('template', $this->data);
        }

        public function login() {
            $this->mlogin->page_direct_dash();
            $this->data = array(
                'title_page' => 'Login'
            );

            $this->load->view('reg_log', $this->data);
        }

        public function message() {
            $this->data = array(
                'page'       => 'message'
            );

            $this->load->view('message', $this->data);
        }

        public function logout() {
            $this->mlogin->logout();
        }

        /**
         * API GET AND POST Zone
         * 
         */

        public function post_login() {
            $this->data = array(
                'username' => $this->input->post('username-login'),
                'password' => $this->input->post('password-login')
            );

            $this->response = $this->mlogin->login($this->data);

            if (!empty($this->response['username'])) {
                $this->mlogin->set_session($this->response);
                echo $this->session->level;
            } else {
                echo "failed";
            }      
        }

        public function post_register_std() {
            $this->data = array(
                'nim'        => $this->input->post('nim-std'),
                'first_name' => $this->input->post('first-name-std'),
                'last_name'  => $this->input->post('last-name-std'),
                'address'    => $this->input->post('address-std'),
                'phone'      => $this->input->post('phone-number-std'),
                'username'   => $this->input->post('username-std'),
                'password'   => $this->input->post('user-password-std'),
                'email'      => $this->input->post('email-std')
            );

            if ($this->mregister->register_std($this->data)) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }

        public function post_register_tch() {
            $this->data = array(
                'nidn'       => $this->input->post('nidn-tch'),
                'first_name' => $this->input->post('first-name-tch'),
                'last_name'  => $this->input->post('last-name-tch'),
                'address'    => $this->input->post('address-tch'),
                'phone'      => $this->input->post('phone-number-tch'),
                'username'   => $this->input->post('username-tch'),
                'password'   => $this->input->post('user-password-tch'),
                'email'      => $this->input->post('email-tch')
            );

            if ($this->mregister->register_tch($this->data)) {
                echo 'success';
            } else {
                echo 'failed';
            }

        }

        public function get_class_province($id) {
            $this->fetch_data = $this->mclass->get_class_province($id);
            $data = array();
            foreach ($this->fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->id;
                $sub_array[] = $row->class_name;
                $sub_array[] = $row->class_code;
                $sub_array[] = $row->image;
                $sub_array[] = $row->province_name;
                $sub_array[] = '<a href="'.base_url('room/detail?class_code='.$row->class_code.'&user_id='.$this->session->user_id).'" class="btn btn-primary">Detail</a>';

                $data[] = $sub_array;
            }

            $result = array('data' => $data);
            echo json_encode($result);  
        }

    }