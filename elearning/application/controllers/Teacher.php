<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

	class Teacher extends CI_Controller {
        public $data = array();
        public $fetch_data;
        
        public function __construct() {
            parent::__construct();
            error_reporting(0);
        }

        // Routing
        public function index() {
            $this->mlogin->page_direct_log($this->session->level, 'starter/login');
            $this->data = array(
                'page'       => 'page_teacher/Dashboard',
                'title_page' => 'Dashboard'
            );

            $this->load->view('template', $this->data);
        }

        public function addClass() {
            $this->mlogin->page_direct_log($this->session->level, 'starter/login');
            $this->data = array(
                'page'       => 'page_teacher/Add_Class',
                'title_page' => 'Tambah kelas'
            );

            $this->load->view('template', $this->data);
        }

        public function detailClass() {
            $this->mlogin->page_direct_log($this->session->level, 'starter/login');
            $this->data = array(
                'page'       => 'page_teacher/Detail_Class',
                'title_page' => 'Detail Peserta'
            );

            $this->load->view('template', $this->data);
        }

        public function detailResult() {
            $this->mlogin->page_direct_log($this->session->level, 'starter/login');
            $this->data = array(
                'page'       => 'page_teacher/Detail_Result',
                'title_page' => 'Hasil Pengerjaan'
            );

            $this->load->view('template', $this->data);
        }

        public function addLesson() {
            $this->mlogin->page_direct_log($this->session->level, 'starter/login');
            $this->data = array(
                'page'       => 'page_teacher/Add_Lesson',
                'title_page' => 'Tambah materi'
            );

            $this->load->view('template', $this->data);
        }

        // Post
        public function postLesson($id, $name) {
            $question    = array();
            $module_name =  str_replace("%20", " ", $name);
            $question_asst = $_POST['uassesment'];

            // upload quiz
            for ($i = 0; $i < count($_POST)-1; $i++) { 
                $this->data['question']   = $_POST['input-quiz-'.($i+1)];
                $this->data['id_classes'] = $id;

                $question[] = $this->data;
            }

            $this->mlesson->add_quiz($question);

            // upload assesment
            $assesment = array(
                'question'   => $question_asst,
                'id_classes' => $id
            );

            $this->mlesson->add_assesment($assesment);

            // upload module
            $config['upload_path']   = './uploads/';
            $config['allowed_types'] = 'pdf|mp4';
            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('ufile')) {
                $error = $this->upload->display_errors();

            } else {
                $result = $this->upload->data();
                $module = array(
                    'module_name' => 'Materi tentang '.$module_name,
                    'file_name'   => $result['file_name'],
                    'file_type'   => $result['file_ext'],
                    'id_classes'  => $id
                );
                $this->mlesson->add_module($module);                
            }

            if (!$this->upload->do_upload('uvideo')) {
                $error = $this->upload->display_errors();
                print_r($error);
            } else {
                $result = $this->upload->data();
                if($result['file_name'] != null) {
                    $module = array(
                        'module_name' => 'Tutorial tentang '.$module_name,
                        'file_name'   => $result['file_name'],
                        'file_type'   => $result['file_ext'],
                        'id_classes'  => $id
                    );
                    $this->mlesson->add_module($module);
                }
            }

        }

        public function postNewClass() {
            $this->mclass->post_new_class($_POST);
        }

        public function postUpdateClass($id) {
            $this->mclass->update_class($id, $_POST);
        }

        public function postDeleteClasses($id) {
            $this->fetch_data = $this->mlesson->get_module($id);
            $count_data       = count($this->fetch_data);

            if($count_data > 1) {
                foreach ($this->fetch_data as $row) {
                    unlink('./uploads/'.$row->file_name);
                }
                
                $this->mclass->delete_class($id);
                echo $this->mlesson->delete_all($id);

            } else {
                $file_name_1 = $this->fetch_data[0]->file_name;
                unlink('./uploads/'.$file_name_1);

                $this->mclass->delete_class($id);
                echo $this->mlesson->delete_all($id);
            }
            
        }

        public function postVisibility($id, $visibility) {
            $this->mclass->update_visibility($id, $visibility);
        }

        /**
         * 
         * --------------------------------------------------------------------
         * This is get function
         */
        public function getProvince() {
            $this->fetch_data = $this->mclass->get_province();
            $data = array();

            foreach ($this->fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->id;
                $sub_array[] = $row->province_name;
                $data[] = $sub_array;
            }

            $result = array('data' => $data);
            echo json_encode($result);
        }

        public function getCategory() {
            $this->fetch_data = $this->mclass->get_category();
            $data = array();

            foreach ($this->fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->id;
                $sub_array[] = $row->category_name;
                $data[] = $sub_array;
            }

            $result = array('data' => $data);
            echo json_encode($result);
        }

        public function getDataClass() {
            $this->fetch_data = $this->mclass->get_class_data();
            $data = array();

            foreach ($this->fetch_data as $row) {
                if($row->status == 1) {
                    $btn_visible = '<a href="#" class="btn btn-sm btn-dark" title="Lihat Kelas"><i class="fa fa-eye" onclick=visible_class("'.$row->class_code.'",2);></i></a>';
                } else if($row->status == 2) {
                    $btn_visible = '<a href="#" class="btn btn-sm btn-success" title="Lihat Kelas"><i class="fa fa-eye" onclick=visible_class("'.$row->class_code.'",1);></i></a>';
                }
                $sub_array = array();
                $sub_array[] = $row->class_name;
                $sub_array[] = $row->province_name;
                $sub_array[] = $row->category_name;
                $sub_array[] = '<a href="#" class="btn btn-sm btn-danger" title="Hapus" onclick=delete_class("'.$row->class_code.'");><i class="fa fa-trash"></i></a>
                                <a href="#" class="btn btn-sm btn-warning" title="Edit" data-toggle="modal" data-target="#modal-class-update" onclick=update_class("'.$row->class_code.'");><i class="fa fa-pencil"></i></a> '.$btn_visible;
                                
                if($row->status == 1) {
                    $sub_array[] = '<span class="badge badge-dark">Tidak Aktif</span>'; 
                } else if ($row->status == 2) {
                    $sub_array[] = '<span class="badge badge-success">Aktif</span>'; 
                }
                $sub_array[] = $row->class_code;
                
                $data[] = $sub_array;
            }

            $result = array('data' => $data);
            echo json_encode($result);           
        }

        public function getDataStd($id) {
            $this->fetch_data = $this->mclass->get_class_std($id);
            $data = array();

            foreach ($this->fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->nim;
                $sub_array[] = $row->first_name." ".$row->last_name;
                $sub_array[] = 'Belum dikerjakan'; 
                $sub_array[] = 'Belum dikerjakan';
                $sub_array[] = '<a href="'.base_url('Teacher/detailResult?class_code='.$id.'&user_id='.$row->id).'" class="btn btn-success btn-primary" title="Detail"><i class="fa fa-list"></i> Detail</a>';

                $data[] = $sub_array;

            }

            $result = array('data' => $data);
            echo json_encode($result); 
        }

        public function get_assesment_result($id, $user_id) {
            $this->fetch_data = $this->mlesson->get_result_video($id, $user_id);
            
            $data = array();
            foreach ($this->fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->id;
                $sub_array[] = $row->id_student;
                $sub_array[] = $row->task_start;
                $sub_array[] = $row->task_process;
                $sub_array[] = $row->task_final;
                $sub_array[] = $row->id_classes;

                $data[] = $sub_array;
            }

            $result = array('data' => $data);
            echo json_encode($result);
        }
    }