<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

	class Room extends CI_Controller {
        var $data = array();
        
        public function __construct() {
            parent::__construct();
        }

        /**
         * 
         * Routing
         */

        public function detail() { 
            $this->mlogin->page_direct_log($this->session->level, 'starter/login');
            if(!empty($_GET['class_code']) AND !empty($_GET['user_id'])) {
                $status         = $this->mclass->get_exist_student($_GET['class_code'], $_GET['user_id']);
                $student_number = $this->mclass->get_std_number($_GET['class_code']); 
            }

            if ($status != 0) {
                $page = 'page_student/Preclass_Finish';               
            } else if ($status == 0) {
                if ($student_number < 20) {
                    $page = 'page_student/Preclass_Available';
                } else {
                    $page = 'page_student/Preclass_Full';
                }
            }

            $this->data = array(
                'page'       => $page,
                'title_page' => 'Room Info'
            );

            $this->load->view('template', $this->data);
        }

        public function rooms() {
            $this->mlogin->page_direct_log($this->session->level, 'starter/login');
            $this->data = array(
                'page'       => 'page_student/Class',
                'title_page' => 'Kelas Pecel'
            );

            $this->load->view('template', $this->data);
        }

        /**
         * Post
         */

        public function add_student_class() {
            echo $this->mclass->post_new_student($_POST);
        } 
        
        public function post_result_assesment($id) {
            // upload module

            if (!is_dir('./adminlearning/assets/images/assesment/task/'.$id.'/'.$this->session->user_id)) {
                mkdir('./adminlearning/assets/images/assesment/task/'.$id.'/'.$this->session->user_id, 0777, TRUE);
                $config['upload_path']   = './adminlearning/assets/images/assesment/task/'.$id.'/'.$this->session->user_id;
            } else {
                $config['upload_path']   = './adminlearning/assets/images/assesment/task/'.$id.'/'.$this->session->user_id;
            }
            $config['allowed_types'] = 'mp4';
            $this->load->library('upload', $config);
            
            $data = array(
                'id_student'    => $this->session->user_id,
                'task_start'    => $_FILES['ufile-1']['name'],
                'task_process'  => $_FILES['ufile-2']['name'],
                'task_final'    => $_FILES['ufile-3']['name'],
                'id_classes'    => $id
            );    
            
            for ($i = 0; $i < count($_FILES); $i++) {
                if (!$this->upload->do_upload('ufile-'.($i + 1))) {
                    $error = $this->upload->display_errors();
                    print_r($error);
                } else {
                    $result = $this->upload->data();
                }
            }

            $this->mlesson->add_assesment_answer($data);
        }

        public function post_result_quiz($id) {
            $data = array();
            for ($i = 0; $i < count($_POST); $i++) { 
                $sub_array['id_student'] = $this->session->user_id;
                $sub_array['answer']     = $_POST['answer-'.($i + 1)];
                $sub_array['id_classes'] = $id;

                $data[] = $sub_array;     
            }

            $this->mlesson->add_quiz_answer($data);
        }

        /**
         * 
         * Get
         */
        public function get_class_detail($id) {
            $this->fetch_data = $this->mclass->get_class_detail($id);
            $std_number       = $this->mclass->get_std_number($id);
            $status           = $this->mclass->get_exist_student($this->fetch_data[0]->class_code, $this->session->user_id);

            $data = array();
            foreach ($this->fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->class_code;
                $sub_array[] = $row->class_name;
                $sub_array[] = $row->description;
                $sub_array[] = $row->image;
                $sub_array[] = $row->province_name;
                $sub_array[] = $row->category_name;
                $sub_array[] = $row->first_name.' '.$row->last_name;
                $sub_array[] = $std_number;
                if($status == 0) {                         
                    $sub_array[] = '<a class="btn btn-primary btn-lg btn-block" onclick=add_student("'.$row->class_code.'","'.$this->session->user_id.'"); href="'.base_url().'room/rooms?class_code='.$row->class_code.'&class_name='.$row->class_name.'&class_tutor='.$row->first_name.' '.$row->last_name.'">Ambil Kelas</a>';       
                } else if($status != 0) {
                    $sub_array[] = '<a class="btn btn-success btn-lg btn-block" href="'.base_url().'room/rooms?class_code='.$row->class_code.'&class_name='.$row->class_name.'&class_tutor='.$row->first_name.' '.$row->last_name.'">Masuk</a>';                           
                }
                $data[] = $sub_array;
            }

            $result = array('data' => $data);
            echo json_encode($result);  
        }

        public function get_quiz($id) {
            $this->fetch_data = $this->mlesson->get_quiz($id);
            
            $data = array();
            foreach ($this->fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->id;
                $sub_array[] = $row->question;
                $sub_array[] = $row->id_classes;

                $data[] = $sub_array;
            }

            $result = array('data' => $data);
            echo json_encode($result);  
        }

        public function get_assesment($id) {
            $this->fetch_data = $this->mlesson->get_assesment($id);

            $data = array();
            foreach ($this->fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->id;
                $sub_array[] = $row->question;
                $sub_array[] = $row->image;
                $sub_array[] = $row->id_classes;

                $data[] = $sub_array;
            }

            $result = array('data' => $data);
            echo json_encode($result);  
        }

        public function get_module($id) {
            $this->fetch_data = $this->mlesson->get_module($id);
            
            $data = array();
            foreach ($this->fetch_data as $row) {
                $sub_array = array();
                $sub_array[] = $row->id;
                $sub_array[] = $row->module_name;
                $sub_array[] = $row->file_name;
                $sub_array[] = $row->id_classes;

                $data[] = $sub_array;
            }

            $result = array('data' => $data);
            echo json_encode($result);  
        }
    }