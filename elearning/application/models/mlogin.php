<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class  mlogin extends CI_Model {
    public $data;
    public $condition;
    public $my_query;
    public $result;

    public function login($data = array()) {

        $this->db->where($data);
        $this->my_query  = ($this->db->get('user'))->row();

        $this->result = array(
            'id'       => $this->my_query->id,
            'username' => $this->my_query->username,
            'verify'   => $this->my_query->verify,
            'level'    => $this->my_query->level
        );

        return $this->result;
    }

    public function logout() {
        $this->data = array('username', 'verify', 'level');
        $this->session->unset_userdata($this->data);
        redirect('starter/login');
    }

    public function set_session($data = array()) {
        $this->session->set_userdata($data);
    }

    public function page_direct_dash() {
        $session = $this->session->level;
        if (!empty($session)) {
            if($session == '2') {
                redirect('starter/dashboard');
            } else if($session == '3') {
                redirect('Teacher/addClass');
            }
        }
    }

    public function page_direct_log($session, $page) {
        if (empty($session)) {
            redirect($page);
        }
    }

}