<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class mclass extends CI_Model {
        public $table;
        public $status;

        /**
         * ---------------------------
         * model room for teacher
         * ---------------------------
         */

        
        public function maxID(){		
            $this->db->select_max('class_code', 'max_class');	
            $result = $this->db->get('classes')->row();
            return $result;
        }

        public function generateCode(){
            $maxID = $this->maxID()->max_class;
            $split = explode("-", $maxID);
            $prefix = $split['0'];
            $code = $split['1'];			
            $increment_code = $code+1;
            $length = strlen($increment_code);	

            if(empty($prefix)){
                $prefix = 'CL';
            }

            if($length == 1){
                $generate_code = $prefix."-000".$increment_code;
            } else if($length == 2){
                $generate_code = $prefix."-00".$increment_code;
            } else if($length == 3){
                $generate_code = $prefix."-0".$increment_code;
            } else if($length == 4){
                $generate_code = $prefix."-".$increment_code;
            }

            return $generate_code;
        }

        /**
         * 
         *  Post model
         */

        public function post_new_class($data = array()){
            $this->table = 'classes';
            $this->db->set('id_teacher', $this->session->user_id, FALSE);
            $this->db->set('class_code', $this->generateCode());

            if($this->db->insert($this->table, $data) !== FALSE) {
                $this->status = "success-insert";
            } else {
                $this->status = "failed-insert";
            }

            return $this->status;
        }

        /**
         * 
         * Get model
         */

        public function get_province() {
            $this->table = 'province';
            $this->db->select('*');

            return ($this->db->get($this->table))->result();
        }

        public function get_category() {
            $this->table = 'category';
            $this->db->select('*');

            return ($this->db->get($this->table))->result();
        }

        public function get_class_data() {
            $query = "SELECT `classes`.`class_code`, `classes`.`class_name`, `classes`.`description`, `classes`.`status`, `province`.`province_name`, `category`.`category_name` FROM `classes`, `province`, `category` WHERE `classes`.`id_province` = `province`.`id` AND `classes`.`id_category` = `category`.`id` AND `classes`.`id_teacher` = ".$this->session->user_id;
            
            return ($this->db->query($query))->result();
        }

        

        public function get_class_std($id) {
            $query = "SELECT * FROM `student`, `student_classes` WHERE `student`.id = `student_classes`.`id_student` AND `student_classes`.`id_classes` = '".$id."'";

            return ($this->db->query($query))->result();
        }

        public function get_std_number($id) {
            $this->table = 'student_classes';
            $this->db->select('*');
            $this->db->where('id_classes', $id);

            return ($this->db->get($this->table))->num_rows();
        }

        /**
         * 
         * Delete model
         */

        public function delete_class($id) {
            $this->db->where('class_code', $id);
            
            if($this->db->delete('classes') !== FALSE) {
                echo "Kelas berhasil dihapus";
            } else {
                echo "Kelas gagal dihapus";
            }
        }

        /**
         * 
         * Update model
         */

        public function update_visibility($id, $visibility) {
            $this->db->set('status', $visibility);
            $this->db->where('class_code', $id);

            if($this->db->update('classes') !== FALSE) {
                if($visibility == 2) {
                    echo "Kelas berhasil diaktifkan";
                } else {
                    echo "Kelas berhasil dinonaktifkan";
                }
            } else {
                echo "Update gagal";
            }
        }

        public function update_class($id, $data) {
            $this->db->where('class_code', $id);

            if($this->db->update('classes', $data) !== FALSE) {
                echo "Update Berhasil";
            } else {
                echo "Gagal";
            }
        }

        /**
         * ---------------------------
         * model room for student
         * ---------------------------
         */
        
         /**
          * 
          * Post Model
          */
        public function post_new_student($data = array()) {
            $this->table = 'student_classes';

            if($this->db->insert($this->table, $data) !== FALSE) {
                $this->status = "success-insert";
            } else {
                $this->status = "failed-insert";
            }

            return $this->status;
        }

        /**
         * 
         * Get Model
         */
        public function get_class_province($id) {
            $query = "SELECT classes.id, classes.class_name, classes.class_code, classes.image, province.province_name FROM `classes`, `province` WHERE `classes`.`id_province` = `province`.`id` AND `classes`.`id_province` = ".$id." AND `classes`.`status` = '2'";

            return ($this->db->query($query))->result();
        }

        public function get_class_detail($id) {
            $query = "SELECT `classes`.`class_code`, `classes`.`class_name`, `classes`.`description`, `classes`.`image`, `province`.`province_name`, `category`.`category_name`, `teacher`.`first_name`, `teacher`.`last_name` FROM `province`, `category`, `teacher`, `classes` WHERE classes.id_teacher = teacher.id AND classes.id_province = province.id AND classes.id_category = category.id AND `classes`.`class_code` = '".$id."'";

            return ($this->db->query($query))->result();
        }

        public function get_exist_student($id, $user_id) {
            $this->table = 'student_classes';
            $this->db->select('*');
            $this->db->where(array('id_classes' => $id, 'id_student' => $user_id));

            return ($this->db->get($this->table))->num_rows();
        }

        
    }