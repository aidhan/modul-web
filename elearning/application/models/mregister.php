<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class  mregister extends CI_Model {
        public $data;
        public $my_query;

        public function register_user($data = array()) {
            return $this->db->insert('user', $data);
        }

        public function register_tch($data = array()) {
            $this->data = array(
                'nidn'         => $data['nidn'],
                'first_name'  => $data['first_name'],
                'last_name'   => $data['last_name'],
                'address'     => $data['address'],
                'phone'       => $data['phone'],
                'username'    => $data['username']
            );

            $user_data = array(
                'username'    => $data['username'],
                'password'    => $data['password'],
                'email'       => $data['email'],
                'code_verify' => 'awut',
                'verify'      => '1',
                'level'       => '3'
            );

            $my_query = $this->db->insert('teacher', $this->data);
            $this->register_user($user_data);
            return $my_query;
        }

        public function register_std($data = array()) {
            $this->data = array(
                'nim'         => $data['nim'],
                'first_name'  => $data['first_name'],
                'last_name'   => $data['last_name'],
                'address'     => $data['address'],
                'phone'       => $data['phone'],
                'username'    => $data['username']
            );

            $user_data = array(
                'username'    => $data['username'],
                'password'    => $data['password'],
                'email'       => $data['email'],
                'code_verify' => 'awut',
                'verify'      => '1',
                'level'       => '2'
            );
            
            $my_query = $this->db->insert('student', $this->data);
            $this->register_user($user_data);
            return $my_query;
        }
    }