<?php
defined('BASEPATH') OR exit('No direct script access allowed');

    class mprofil extends CI_Model {
        public $table1 = 'student, user';
        public $table2 = 'teacher, user';
        public $select_column1  = array('student.id', 'student.nim', 'student.first_name', 'student.last_name', 'student.address', 'student.address', 'student.phone', 'student.username', 'student.image');
        public $select_column2  = array('teacher.id', 'teacher.nidn', 'teacher.first_name', 'teacher.last_name', 'teacher.address', 'teacher.address', 'teacher.phone', 'teacher.username', 'teacher.image');
        public $status;

        public function get() {
            if($this->session->level == 2) {
                $this->db->select($this->select_column1);
                $this->db->where('student.username', $this->session->username);
                $this->db->where('user.id', $this->session->id);     
            } else if($this->session->level == 3) {
                $this->db->select($this->select_column2);
                $this->db->where('teacher.username', $this->session->username);
                $this->db->where('user.id', $this->session->id); 
            }
        
        }

        public function result() {
            $this->get();
            if($this->session->level == 2) {
                $this->status = ($this->db->get($this->table1))->result();
                $this->session->set_userdata(array('user_id' => $this->status[0]->id));

                return $this->status;

            } else if($this->session->level == 3) {
                $this->status = ($this->db->get($this->table2))->result();
                $this->session->set_userdata(array('user_id' => $this->status[0]->id));

                return $this->status;
            }           
        }
    }