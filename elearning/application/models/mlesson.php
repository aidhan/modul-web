<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class  mlesson extends CI_Model {
        /**
         * 
         * Post function model
         */

        public function add_quiz($data = array()) {
            if ($this->db->insert_batch('quiz', $data)) {
                echo "success";
            } else {
                echo "failed";
            }

        }

        public function add_assesment($data = array()) {
            if ($this->db->insert('assesment', $data)) {
                echo "success";
            } else {
                echo "failed";
            }

        }

        public function add_module($data = array()) {
            if ($this->db->insert('module', $data)) {
                echo "success";
            } else {
                echo "failed";
            }
        }

        public function add_quiz_answer($data) {
            if($this->db->insert_batch('result_quiz', $data) !== FALSE) {
                echo "Submit Berhasil";
            } else {
                echo "Submit Gagal";
            }
        }

        public function add_assesment_answer($data) {
            if ($this->db->insert('result_assesment', $data) !== FALSE) {
                echo "Submit Berhasil";
            } else {
                echo "Submit Gagal";
            }
        }

        /**
         * 
         * Get function model
         */

        public function get_assesment($id) {
            $this->db->select('*');
            $this->db->where('id_classes', $id);

            return ($this->db->get('assesment'))->result();            
        }

        public function get_quiz($id) {
            $this->db->select('*');
            $this->db->where('id_classes', $id);

            return ($this->db->get('quiz'))->result();
        }

        public function get_module($id) {
            $this->db->select('*');
            $this->db->where('id_classes', $id);

            return ($this->db->get('module'))->result();
        }

        public function get_result_video($id, $user_id) {
            $this->db->select('*');
            $this->db->where(array(
                'id_student' => $user_id,
                'id_classes' => $id
            ));

            return ($this->db->get('result_assesment'))->result();
        }

        /**
         * 
         * Delete Fucntion
         */

        public function delete_all($id) {
            $table = array('assesment', 'quiz', 'module');

            $this->db->where('id_classes', $id);
            
            if($this->db->delete($table) !== FALSE) {
                echo "Hapus kelas berhasil";
            } else {
                echo "Hapus kelas gagal ".$this->db->error();
            }
        }

    }